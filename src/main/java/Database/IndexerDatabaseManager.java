package Database;


import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

import java.io.File;
import java.io.IOException;

public class IndexerDatabaseManager {
    public  static Environment myDbEnvironment = null;
    private static File envHome;
    private static EnvironmentConfig envConfig;
    public static EntityStore indexStore;

    public static void initStore() {
        StoreConfig sc = new StoreConfig();
        sc.setAllowCreate(true);
        indexStore = new EntityStore(myDbEnvironment, "indexStore", sc);

    }

    //Opens the database
    public static void initDatabase(String dir) throws IOException {
        envHome = new File(dir);
        try {
            if (!envHome.exists()) {
                Runtime.getRuntime().exec("mkdir " +  envHome);
            }

            envConfig = new EnvironmentConfig();
            envConfig.setAllowCreate(true);
            myDbEnvironment = new Environment(envHome, envConfig);

        } catch (DatabaseException dbe) {
            dbe.printStackTrace();
        }
    }

}
