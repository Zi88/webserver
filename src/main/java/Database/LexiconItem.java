package Database;


import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.util.Set;

@Entity
public class LexiconItem {

    public int ni;
    public int mi;

    @PrimaryKey
    public String word;

    public LexiconItem(){};

    public LexiconItem(String word, int ni, int mi) {
        this.word = word;
        this.mi = mi;
        this.ni = ni;
    }



}


