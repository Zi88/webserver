package Database;

public class TFIDFScoreObject implements Comparable<TFIDFScoreObject> {
    public String word;
    public String url;
    public double score;

    public TFIDFScoreObject(String word, String url, double score) {
        this.score = score;
        this.url = url;
        this.word = word;
    }

    @Override
    public int compareTo(TFIDFScoreObject other) {
        if (this.score >= other.score) return 1;
        else return -1;
    }
}
