package edu.upenn.cis455.xpathengine;

import org.w3c.dom.Node;

/**
 * Created by Alex on 3/25/18
 */
public class TextTest implements StepTest {

    String text;

    @Override
    public boolean passesTest(Node n) {
        return n.getTextContent() != null && n.getTextContent().equals(text);
    }
}
