package edu.upenn.cis455.xpathengine;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.helpers.DefaultHandler;

import java.io.InputStream;
import java.util.List;

public class XPathEngineImpl implements XPathEngine {

    private String[] xpaths;
    private XPath[] compiledXPaths;

    public XPathEngineImpl() {
        // Do NOT add arguments to the constructor!!
    }


    public void setXPaths(String[] s) {
        xpaths = s;
        if (xpaths == null) {
            return;
        }

        // Compile all the xpath strings to get useable XPath objects for matching on documents
        RecursiveDescentParser p = new RecursiveDescentParser();
        compiledXPaths = new XPath[xpaths.length];
        for (int i = 0; i < xpaths.length; i++) {
            compiledXPaths[i] = p.compile(xpaths[i]);
        }
    }

    public boolean isValid(int i) {
        return !(xpaths == null || i >= xpaths.length || i < 0) && validXPath(compiledXPaths[i]);
    }

    public boolean[] evaluate(Document d) {
        if (xpaths == null) {
            return null;
        }
        boolean[] matches = new boolean[xpaths.length];
        for (int i = 0; i < compiledXPaths.length; i++) {
            if (matchFound(d, compiledXPaths[i])) {
                matches[i] = true;
            }
        }
        return matches;
    }

    private boolean matchFound(Document d, XPath xpath) {
        if (xpath == null) {
            return false;
        }
        Step first = xpath.first;
        Element startingElt = d.getDocumentElement();
        return matchOnElt(startingElt, first);
        // TODO the alternate version, though not sure why it gives us the root in a NodeList correctly
        //return matchOnAny(d.getChildNodes(), first);
    }

    private boolean matchOnElt(Node node, Step step) {
        if (node == null) {
            return false;
        }
        if (!step.nodename.equals(node.getNodeName())) {
            return false;
        }
        if (!passStepTests(node, step.tests)) {
            return false;
        }
        if (!(step.next == null)) {
            NodeList childNodes = node.getChildNodes();
            for (Step s: step.next) {
                boolean childPasses = false;
                for (int i = 0; i < childNodes.getLength(); i++) {
                    if (matchOnElt(childNodes.item(i), s)) {
                        childPasses = true;
                        break;
                    }
                }
                if (!childPasses) {
                    return false;
                }
            }
        }
        return true;
    }

    // TODO: This one might be gentler on the stack, since it presumably gets called less often
    private boolean matchOnAny(NodeList nodes, Step step) {
        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            // Check that the nodename is correct
            if (!step.nodename.equals(n.getNodeName())) {
                continue;
            }
            // Check that all this node's tests are passed
            if (!passStepTests(n, step.tests)) {
                continue;
            }
            // Check that all child steps are satisfied by the children of this node
            if (step.next != null) {
                boolean childrenPass = true;
                for (Step s : step.next) {
                    if (!matchOnAny(n.getChildNodes(), s)) {
                        childrenPass = false;
                        break;
                    }
                }
                if (!childrenPass) {
                    continue;
                }
            }
            // If nodename is right, step tests are right, and children steps all match, we've got a full match
            return true;
        }
        // If none of the nodes match, we're out of luck
        return false;
    }

    // Return true if the given node passes the list of tests
    private boolean passStepTests(Node n, List<StepTest> tests) {
        if (tests == null) {
            return true;
        }
        for (StepTest t : tests) {
            if (!t.passesTest(n)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param xpath an XPath in String form
     * @return true if the XPath is valid according to our simplified grammar
     */
    private boolean validXPath(XPath xpath) {
        return !(xpath == null);
    }

    @Override
    public boolean isSAX() {
        return false;
    }

    @Override
    public boolean[] evaluateSAX(InputStream document, DefaultHandler handler) {
        return null;
    }
}
