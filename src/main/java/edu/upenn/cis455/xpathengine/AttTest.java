package edu.upenn.cis455.xpathengine;

import org.w3c.dom.Node;

/**
 * Created by Alex on 3/25/18
 */
public class AttTest implements StepTest {

    String attname;
    String attval;

    public boolean passesTest(Node n) {
        if (!n.hasAttributes()) {
            return false;
        }
        Node attr = n.getAttributes().getNamedItem(attname);
        if (attr == null) {
            return false;
        }
        return attr.getNodeValue() != null && attr.getNodeValue().equals(attval);
    }
}