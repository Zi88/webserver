package edu.upenn.cis455.xpathengine;

import java.util.List;

/**
 * Created by Alex on 3/23/18
 */
public class Step {

    public String nodename;
    public List<StepTest> tests;
    public List<Step> next;

}
