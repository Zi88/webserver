package edu.upenn.cis455.xpathengine;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Created by Alex on 3/23/18
 */

// A parser for XPath expressions
public class RecursiveDescentParser {

    private class Pair<K, V> {
        private K key;
        private V value;

        public Pair(K k, V v) {
            key = k;
            value = v;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

    public XPath compile(String exp) {
        try {
            // Remove all whitespace from the get go
            exp = removeUnescapedWhitespace(exp);
            return xpath(exp, 0);
        } catch (XPathParseException e) {
            return null;
        }

    }

    private XPath xpath(String exp, int pos) throws XPathParseException {
        expect(exp, pos, '/');
        pos++;
        XPath xpath = new XPath();
        Pair<Integer, Step> p = step(exp, pos, false);
        xpath.first = p.getValue();
        return xpath;
    }

    private Pair<Integer, Step> step(String exp, int pos, boolean calledFromTest) throws XPathParseException {
        // Parse the nodename, which is required for every step
        HashSet<Character> stops = new HashSet<>(Arrays.asList('/', '[', ']'));
        Pair<String, Integer> p = consumeUntil(exp, pos, stops);
        pos = p.getValue();
        String nodename = p.getKey();
        if (nodename.equals("")) {
            throw new XPathParseException("Nonexistent nodename at pos = " + pos);
        }
        Step s = new Step();
        s.nodename = nodename;

        // TODO make sure this loop is appropriate
        // We are done if we've consumed the entire String
        // Otherwise, figure out if we've hit a new (independent) step or a test
        while (pos < exp.length()) {
            // TODO you'll need to something to make sure the s.next ref isn't already occupied (using it in both of these)
            switch (exp.charAt(pos)) {
                case '/':
                    pos++;
                    Pair<Integer, Step> pair = step(exp, pos, calledFromTest);
                    addChildStep(s, pair.getValue());
                    pos = pair.getKey();
                    break;
                case '[':
                    pos++;
                    pos = test(s, exp, pos);
                    expect(exp, pos, ']');
                    pos++;
                    break;
                case ']':
                    // If a test is not open, this (step) test itself must be coming to a close
                    if (calledFromTest) {
                        return new Pair<>(pos, s);
                    }
                default:
                    throw new XPathParseException("Unexpected char");
            }
        }
        return new Pair<>(pos, s);
    }

    private int test(Step s, String exp, int pos) throws XPathParseException {
        // Get the substring
        String substr = exp.substring(pos);

        // See what the test starts with
        if (substr.startsWith("text()=")) {
            // Text test
            int consumed = textTest(s, substr);
            return pos + consumed;
        } else if (substr.startsWith("contains(text(),")) {
            // Contains test
            int consumed = containsTest(s, substr);
            return pos + consumed;
        } else if (substr.startsWith("@")) {
            // Attname test
            int consumed = attnameTest(s, substr);
            return pos + consumed;
        } else {
            // Otherwise the test is itself a step
            Pair<Integer, Step> p = step(exp, pos, true);
            addChildStep(s, p.getValue());
            return p.getKey();
        }
    }

    private void addChildStep(Step s, Step child) {
        if (s.next == null) {
            s.next = new LinkedList<>();
        }
        s.next.add(child);
    }

    private int attnameTest(Step s, String substr) throws XPathParseException {
        // Get the attr name
        int nameStart = substr.indexOf('@') + 1;
        int nameEnd = substr.indexOf('=');
        if (nameStart == 0 || nameStart > nameEnd) {
            throw new XPathParseException("Malformed attribute name");
        }
        String name = substr.substring(nameStart, nameEnd);

        // This is a terminal test, so we need only consume up to the next ']'
        int nextPos = substr.indexOf(']');
        if (nextPos == -1) {
            throw new XPathParseException("Unclosed bracket");
        }
        int textStart = substr.indexOf('"') + 1;
        int textEnd = nextPos - 1;
        if (textStart == 0 || textStart > textEnd) {
            throw new XPathParseException("Mismatched or nonexistent quotation marks");
        }
        String text = substr.substring(textStart, textEnd);

        // Put the test in the set of this step's tests
        if (s.tests == null) {
            s.tests = new LinkedList<StepTest>();
        }
        AttTest t = new AttTest();
        t.attname = name;
        t.attval = text;
        s.tests.add(t);
        return nextPos;
    }

    private int containsTest(Step s, String substr) throws XPathParseException {
        // This is a terminal test, so we need only consume up to the next ']'
        int nextPos = substr.indexOf(']');
        if (nextPos == -1) {
            throw new XPathParseException("Unclosed bracket");
        }
        int textStart = substr.indexOf('"') + 1;
        int textEnd = substr.indexOf('"', textStart);
        if (textStart == 0 || textStart > textEnd) {
            throw new XPathParseException("Mismatched or nonexistent quotation marks");
        }
        String text = substr.substring(textStart, textEnd);

        // Put the test in the set of this step's tests
        if (s.tests == null) {
            s.tests = new LinkedList<StepTest>();
        }
        ContainsTest t = new ContainsTest();
        t.text = text;
        s.tests.add(t);
        return nextPos;
    }

    private int textTest(Step s, String substr) throws XPathParseException {
        // This is a terminal test, so we need only consume up to the next ']'
        int nextPos = substr.indexOf(']');
        if (nextPos == -1) {
            throw new XPathParseException("Unclosed bracket");
        }
        int textStart = substr.indexOf('"') + 1;
        int textEnd = substr.indexOf('"', textStart);
        if (textStart == 0 || textStart > textEnd) {
            throw new XPathParseException("Mismatched or nonexistent quotation marks");
        }
        String text = substr.substring(textStart, textEnd);

        // Put the test in the set of this step's tests
        if (s.tests == null) {
            s.tests = new LinkedList<StepTest>();
        }
        TextTest t = new TextTest();
        t.text = text;
        s.tests.add(t);
        return nextPos;
    }

    private Pair<String, Integer> consumeUntil(String exp, int pos, HashSet<Character> stops) {
        StringBuilder sbuf = new StringBuilder();
        while (pos < exp.length() && !stops.contains(exp.charAt(pos))) {
            sbuf.append(exp.charAt(pos));
            pos++;
        }
        return new Pair<>(sbuf.toString(), pos);
    }

    private void expect(String exp, int pos, char expected) throws XPathParseException {
        if (exp.charAt(pos) == expected) {
            return;
        }
        throw new XPathParseException("Expected: " + expected + " but received: " + exp.charAt(pos));
    }

    private String removeUnescapedWhitespace(String s) {
        StringBuilder sb = new StringBuilder();
        boolean escaped = false;
        int pos = 0;
        while (pos < s.length()) {
            char curr = s.charAt(pos);
            if (curr == '"') {
                escaped = !escaped;
            }
            if (!Character.isWhitespace(curr) || escaped) {
                sb.append(curr);
            }
            pos++;
        }
        return sb.toString();
    }

    private class XPathParseException extends Exception {
        XPathParseException(String message) {
            super(message);
        }
    }
}
