package edu.upenn.cis455.xpathengine;

import org.w3c.dom.Node;

/**
 * Created by Alex on 3/25/18
 */
public class ContainsTest implements StepTest {

    String text;

    public boolean passesTest(Node n) {
        String nText = n.getTextContent();
        return nText != null && nText.contains(text);
    }
}
