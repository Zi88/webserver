package edu.upenn.cis455.xpathengine;

import org.w3c.dom.Node;

/**
 * Created by Alex on 3/25/18
 */
public interface StepTest {
    boolean passesTest(Node n);
}
