package edu.upenn.cis455.barrelServer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BarrelQueryServlet extends HttpServlet {

    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String URI = request.getRequestURI();
		System.out.println("test request: " + URI);
		PrintWriter pw = response.getWriter();
		switch (URI) {
			case "/score":
				String s = "https://colorlib.com:443/wp/best-wordpress-travel-themes/@@24.95921209005478\n" + 
						"https://www.random.org:443/draws/details/?draw=62@@25.123533143059397\n" + 
						"https://marusja.dreamwidth.org:443/read?show=F@@26.815870630563666\n" + 
						"https://www.ageuk.org.uk:443/products/insurance/car-insurance/@@28.263841786695867\n" + 
						"https://garage.vice.com:443/en_us/article/ev5x77/gallery1993-mobile-gallery-los-angeles@@28.415961412272328\n" + 
						"http://www.ibiblio.org:80/hyperwar/USN/ref/BGD/index.html@@30.490952750254852\n" + 
						"https://catvalente.dreamwidth.org:443/?skip=40@@33.33656730037746\n" + 
						"https://insp.ngo:443/streetwise-vendors-review-this-years-oscar-nominated-films/@@51.5365774529683\n" + 
						"https://www.cpj.org:443/imprisoned/2016.php@@63.412023959738704\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30335@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30416@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30293@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=56491@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30325@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30412@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30654@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30430@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30389@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30310@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30423@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30502@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30443@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30404@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30408@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=31440@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30419@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30690@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=31431@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30449@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30323@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30689@@71.85057826112737\n" + 
						"https://asiansecurityblog.wordpress.com:443/about/?replytocom=30422@@71.85057826112737\n" + 
						"https://www.myirelandtour.com:443/about/reviews.php@@75.83813855622499\n" + 
						"https://www.acm.org:443/conferences/best-paper-awards@@103.64816722709102\n" + 
						"https://www.harihareswara.net:443/nb/nb.cgi/category/sumana/Comedy@@118.56792608352053\n" + 
						"http://www.exeter.ac.uk:80/news/rss/international/index.rss@@123.29378105716268\n" + 
						"https://www.thepodcasthost.com:443/equipment/the-best-podcasting-microphones-on-the-market/@@123.32828873614692\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=11219@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=13132@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=6511@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=4872@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=5047@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=3223@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=3289@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=10688@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?share=email@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=7222@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=4931@@132.32574713136674\n" + 
						"https://weeklysift.com:443/2012/09/10/the-distress-of-the-privileged/?replytocom=7231@@132.32574713136674\n" + 
						"https://twitter.com:443/BeckenhamMarket@@1624.4230444853229";
				pw.print(s);
				break;
		}
    }
}
