package edu.upenn.cis455.barrelServer;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

public class BarrelServer  {
    public static void main(String[] args) throws Exception {
    	
		if (args.length != 1) {
			System.out.println("Usage: <port>");
			return;
		}
		
		int port = Integer.parseInt(args[0]);
		System.out.println("port " + port + " is now listening");
        Server server = new Server(port);
        
        WebAppContext context = new WebAppContext();

        context.setDescriptor("conf/web.xml");
        context.setContextPath("/");
        context.setResourceBase(".");
        context.setParentLoaderPriority(false);
        server.setHandler(context);

        server.start();
        server.join();
    }
}