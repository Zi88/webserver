package edu.upenn.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * Created by Alex on 3/7/18
 */
@Entity
public class CacheEntity {

    @PrimaryKey
    private String key;
    private Object value;

    public CacheEntity() {
    }

    public CacheEntity(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
