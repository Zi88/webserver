package edu.upenn.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * Created by Alex on 3/2/18
 */
@Entity
public class ResourceEntity {

    @PrimaryKey
    private String url;       // The URL for this resource, in String form

    private long timestamp;   // Time when the URL was last crawled
    private String body;      // The actual content stored at the given URL
    private String contentType; // The MIME type

    // Default constructor required for Berkley DPL
    public ResourceEntity() {
    }

    public ResourceEntity(String url, long timestamp, String body, String contentType) {
        this.url = url;
        this.timestamp = timestamp;
        this.body = body;
        this.contentType = contentType;
    }

    public String getUrl() {
        return url;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getBody() {
        return body;
    }

    public String getContentType() {
        return contentType;
    }
}
