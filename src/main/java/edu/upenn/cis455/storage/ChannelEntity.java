package edu.upenn.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.util.HashSet;

/**
 * Created by Alex on 3/25/18
 */
@Entity
public class ChannelEntity {

    @PrimaryKey
    private String name;
    private String xpath;
    private String creator;
    private HashSet<String> urls;

    // No args constructor needed by DPL
    public ChannelEntity() {
    }

    public ChannelEntity(String name, String xpath, String creator) {
        this.name = name;
        this.xpath = xpath;
        this.creator = creator;
        this.urls = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public String getXpath() {
        return xpath;
    }

    public String getCreator() {
        return creator;
    }

    public HashSet<String> getUrls() {
        return urls;
    }
}
