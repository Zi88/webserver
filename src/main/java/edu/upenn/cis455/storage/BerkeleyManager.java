package edu.upenn.cis455.storage;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.*;
import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.hostsplitter.SendItemEntity;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import edu.upenn.cis455.crawler.webtools.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 4/7/18
 *
 * This class manages the BerkeleyDB environment(s) for a single worker with potentially many executors
 */
public class BerkeleyManager {

    private static final Logger LOG = LogManager.getLogger(BerkeleyManager.class);
    private static Map<String, EntityStore> tablesByExecutorId = new HashMap<>();
    private static Environment env;
    private static boolean initialized = false;

    public enum Table {
        Resources,
        CacheEntries,
        SendItems
    }

    public static void initManager() {
        if (initialized) {
            return;
        }
        File dir = new File(Config.DATABASE_ENV_DIR);
        if (dir.mkdirs()) {
            LOG.debug("Initializing database environment in " + dir.toString());
        }

        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        env = new Environment(new File(Config.DATABASE_ENV_DIR), envConfig);

        // Initialize all tables
        for (Table tid : Table.values()) {
            initTable(tid);
        }
        initialized = true;
    }

    /**
     * Create the BerkeleyDB table for the given executor
     */
    private static EntityStore initTable(Table tid) {
        StoreConfig conf = StoreConfig.DEFAULT;
        conf.setAllowCreate(true);
        EntityStore e = new EntityStore(env, tid.toString(), StoreConfig.DEFAULT);
        tablesByExecutorId.put(tid.toString(), e);
        return e;
    }

    /**
     * @param url The URL corresponding to the resource
     * @return a ResourceEntity, or null if there is none at the given key
     */
    public static ResourceEntity getResource(URL url) {
        PrimaryIndex<String, ResourceEntity> idx = getPrimaryIndex(Table.Resources, String.class, ResourceEntity.class);
        return idx.get(url.toString());
    }

    /**
     * Given a Response object, build a ResourceEntity, but it in the DB, and return it
     */
    public static ResourceEntity putResponse(URL url, Response resp) {
        // Marshall the Response object into a ResourceEntity, then put it in the DB
        String contentType = resp.getHeader("Content-Type");
        ResourceEntity re = new ResourceEntity(CrawlerUtils.generateUrlKey(url), resp.getTimestamp(), resp.getBodyStr(), contentType);

        PrimaryIndex<String, ResourceEntity> idx = getPrimaryIndex(Table.Resources, String.class, ResourceEntity.class);
        idx.putNoReturn(re);
        return re;
    }

    /**
     * Return a cache entity
     */
    public static CacheEntity getCacheEntity(String key) {
        PrimaryIndex<String, CacheEntity> cacheEntryByKey = getPrimaryIndex(Table.CacheEntries, String.class, CacheEntity.class);
        return cacheEntryByKey.get(key);
    }

    /**
     * Put a Cache entry into the DB.  We wait for the put operation to return so that the cache stays consistent.
     */
    public static CacheEntity putCacheEntity(String key, Object value) {
        PrimaryIndex<String, CacheEntity> cacheEntryByKey = getPrimaryIndex(Table.CacheEntries, String.class, CacheEntity.class);
        CacheEntity ce = new CacheEntity(key, value);
        return cacheEntryByKey.put(ce);
    }

    /**
     * Put a SendItemEntity into the DB.  This is used by the SendDaemon to buffer RPC messages to other nodes
     */
    public static SendItemEntity putSendItemEntity(SendItemEntity sie) {
        PrimaryIndex<Long, SendItemEntity> sendItemsById = getPrimaryIndex(Table.SendItems, Long.class, SendItemEntity.class);
        return sendItemsById.put(sie);
    }

    /**
     * @return a cursor over the RPC messages not yet delivered to the given remote node
     */
    public static EntityCursor<SendItemEntity> getSendItemsByAddr(String addr) {

        // Define a secondary index over the SendItem.addr fields, rather than the internally used Longs
        EntityStore store = tablesByExecutorId.get(Table.SendItems.toString());
        PrimaryIndex<Long, SendItemEntity> primaryIdx = getPrimaryIndex(Table.SendItems, Long.class, SendItemEntity.class);
        SecondaryIndex<String, Long, SendItemEntity> secondaryIdx = store.getSecondaryIndex(primaryIdx, String.class, "addr");

        // Get a subindex of just the values with the given MapReduce key, return a cursor over these
        EntityIndex<Long, SendItemEntity> subIdx = secondaryIdx.subIndex(addr);
        return subIdx.entities();
    }

    public static void sync() {
        env.sync();
    }

    public static void close() {
        for (EntityStore e : tablesByExecutorId.values()) {
            e.close();
        }
        env.sync();
        env.close();
    }

    private static PrimaryIndex getPrimaryIndex(Table tid, Class PK, Class E) {
        EntityStore e = tablesByExecutorId.get(tid.toString());
        return e.getPrimaryIndex(PK, E);
    }

}
