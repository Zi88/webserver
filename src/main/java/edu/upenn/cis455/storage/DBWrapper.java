package edu.upenn.cis455.storage;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;
import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import edu.upenn.cis455.crawler.webtools.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.SortedMap;

public class DBWrapper {

    private static final Logger LOG = LogManager.getLogger(DBWrapper.class);
    private static String envDirectory = Config.DATABASE_ENV_DIR;  // A default filepath
	private static Environment myEnv;
	private static EntityStore store;
    private static PrimaryIndex<String, ResourceEntity> resourceByUrl;
    private static PrimaryIndex<String, UserEntity> userById;
    private static PrimaryIndex<String, CacheEntity> cacheEntryByKey;
    private static PrimaryIndex<String, ChannelEntity> channelByName;
    private static boolean initialized = false;


    public static void initialize(String berkleyHome) {
        if (initialized) {
            return;
        }
        // Initialize the environment
        if (berkleyHome != null) {
            envDirectory = berkleyHome;
        }
        File dbFileObj = Paths.get(envDirectory).toFile();
        if (!dbFileObj.exists()) {
            boolean success = dbFileObj.mkdir();
            if (!success) {
                LOG.error("Failed to create DB directory: " + envDirectory);
                System.exit(-1);
            }
            LOG.info("Created DB directory: " + envDirectory);
        }

        EnvironmentConfig envConf = new EnvironmentConfig();
        envConf.setAllowCreate(true);    // Will create files as needed
        envConf.setTransactional(true);  // Allow transactional semantics
        myEnv = new Environment(dbFileObj, envConf);

        // Create the store
        StoreConfig storeConf = new StoreConfig();
        storeConf.setAllowCreate(true);
        storeConf.setTransactional(true);
        store = new EntityStore(myEnv, "ResourceStore", storeConf);

        // Create the primary index for getting/putting things
        resourceByUrl = store.getPrimaryIndex(String.class, ResourceEntity.class);
        userById = store.getPrimaryIndex(String.class, UserEntity.class);
        cacheEntryByKey = store.getPrimaryIndex(String.class, CacheEntity.class);
        channelByName = store.getPrimaryIndex(String.class, ChannelEntity.class);
        initialized = true;
	}

    /**
     * @param url The URL corresponding to the resource
     * @return a ResourceEntity, or null if there is none at the given key
     */
	public static ResourceEntity getResource(URL url) {
        ensureInitialized();
        return resourceByUrl.get(CrawlerUtils.generateUrlKey(url));
	}

    /**
     * Given a Response object, build a ResourceEntity, but it in the DB, and return it
     */
	public static ResourceEntity putResponse(URL url, Response resp) {
        ensureInitialized();
        // Marshall the Response object into a ResourceEntity, then put it in the DB
        String contentType = resp.getHeader("Content-Type");
        ResourceEntity re = new ResourceEntity(CrawlerUtils.generateUrlKey(url), resp.getTimestamp(), resp.getBodyStr(), contentType);
        resourceByUrl.putNoReturn(re);
        return re;
	}


    public static UserEntity getUser(String user) {
        ensureInitialized();
        return userById.get(user);
    }


    public static UserEntity createUser(String user, String pass) {
        ensureInitialized();
        try {
            String hash = new String(MessageDigest.getInstance("SHA-256").digest(pass.getBytes()));
            UserEntity ue = new UserEntity(user, hash);
            userById.putNoReturn(ue);
            return ue;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * Put a UserEntity in the index (e.g. if you have an existing object that you've updated)
     * @param ue A completed UserEntity
     */
    public static void putUser(UserEntity ue) {
        userById.putNoReturn(ue);
    }

    /**
     * Return a cache entity
     */
    public static CacheEntity getCacheEntity(String key) {
        ensureInitialized();
        return cacheEntryByKey.get(key);
    }

    /**
     * Put a Cache entry into the DB.  We wait for the put operation to return so that the cache stays consistent.
     */
    public static CacheEntity putCacheEntity(String key, Object value) {
        ensureInitialized();
        CacheEntity ce = new CacheEntity(key, value);
        return cacheEntryByKey.put(ce);
    }

    public static ChannelEntity createChannel(String name, String xpath, String creator) {
        ensureInitialized();
        ChannelEntity ce = new ChannelEntity(name, xpath, creator);
        channelByName.putNoReturn(ce);
        return ce;
    }

    public static ChannelEntity getChannel(String name) {
        ensureInitialized();
        return channelByName.get(name);
    }

    public static void putChannel(ChannelEntity ce) {
        channelByName.putNoReturn(ce);
    }

    public static void deleteChannel(String cname) {
        ensureInitialized();
        channelByName.delete(cname);
    }

    private static void ensureInitialized() {
        if (!initialized) {
            initialize(null);
        }
    }

    public static void sync() {
        myEnv.sync();
    }

    public static void close() {
        myEnv.close();
    }

    public static SortedMap<String, ChannelEntity> getAllChannels() {
        return channelByName.sortedMap();
    }
}
