package edu.upenn.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.util.HashSet;

/**
 * Created by Alex on 3/7/18
 */
@Entity
public class UserEntity {

    @PrimaryKey
    private String username;
    private String hashedPassword;
    private HashSet<String> subscriptions;

    // Default constructor needed by DPL
    public UserEntity() {
    }

    public UserEntity(String username, String hashedPassword) {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.subscriptions = new HashSet<>();
    }

    public String getHashedPass() {
        return hashedPassword;
    }

    public String getUsername() {
        return username;
    }

    public void addSubscription(String name) {
        subscriptions.add(name);
    }

    public void removeSubscription(String name) {
        subscriptions.remove(name);
    }

    public HashSet<String> getSubscriptions() {
        return subscriptions;
    }


}
