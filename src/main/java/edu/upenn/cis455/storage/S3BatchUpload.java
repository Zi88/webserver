package edu.upenn.cis455.storage;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import edu.upenn.cis455.crawler.pagetypes.JacksonPageSerializationProvider;
import edu.upenn.cis455.crawler.pagetypes.Page;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Alex on 4/21/18
 */
public class S3BatchUpload {


    private static final String BUCKET_NAME = "cis555-g10-crawl-bucket-1";
    // 750 MB limit per batch file. Can get much larger, since we only roll over on a per-file basis
    private static final int BATCH_SIZE_LIMIT_MB = 750;
    private static final Path BATCH_DIR = Paths.get(Config.WORKING_DIR, "batches");

    // The AWS SDK performs some VERY chatty logging.  Hopefully this disables all of it.
//    static {
//        System.setProperty("org.apache.commons.logging.Log",
//                "org.apache.commons.logging.impl.NoOpLog");
//    }

    public static void main(String[] args) throws IOException {

        try {
            File pageDir = new File(Config.PAGE_DIR);
            SerializationProvider<Page> provider = new JacksonPageSerializationProvider();

            // Some summary statistics
            int totalPages = 0;
            long totalClen = 0;
            int batchPages = 0;
            long batchClen = 0;

            BufferedWriter w = assignWriter();
            for (File pageFile : pageDir.listFiles()) {
                BufferedReader r = new BufferedReader(new FileReader(pageFile));
                String line = null;
                while ((line = r.readLine()) != null) {
                    Page p = provider.readObject(line);
                    // Ignore Page objects that are somehow impossible to deserialize
                    if (p == null) {
                        continue;
                    }
                    // Update stats
                    batchClen += p.getContentLength();
                    totalClen += p.getContentLength();
                    batchPages++;
                    totalPages++;

                    // Write the line back to the batch file
                    w.write(line + "\n");
                }
                r.close();
                // When the file is large enough, roll over to a new one
                if (batchClen > BATCH_SIZE_LIMIT_MB * 1000000) {
                    System.out.println(String.format("Finished batch file: %d pages, %.2f MB", batchPages, (float) batchClen / 1000000));
                    w.close();
                    w = assignWriter();
                    batchClen = 0;
                    batchPages = 0;
                }
            }
            w.close();

            System.out.println(String.format("Finished batching: %d total pages, %.2f MB total content (excluding metadata), %.2f KB avg file size",
                    totalPages, (float) totalClen / 1000000, (float) (totalClen / 1000) / totalPages));

            AmazonS3 s3client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new InstanceProfileCredentialsProvider(false))
                    .build();
            for (File batchedFile : BATCH_DIR.toFile().listFiles()) {
                System.out.println(batchedFile  + ": Uploading");
                s3client.putObject(BUCKET_NAME, buildKey(batchedFile), batchedFile);
            }

        }
        catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
                    "means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
                    "means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    // A key, essentially a file path that S3 uses to imitate a heirarchy among objects
    private static String buildKey(File batchedFile) {
        return "PageObjects/" + batchedFile.getName();
    }

    private static BufferedWriter assignWriter() throws IOException {

        // Assign files with a timestamp, which might be useful for incremental processing
        File batchFile = Paths.get(BATCH_DIR.toString(), "BatchedPages-" + System.currentTimeMillis() + ".txt").toFile();
        // Make parent dir as needed
        batchFile.getParentFile().mkdirs();
        if (batchFile.createNewFile()) {
            System.out.println("Creating new batching file: "  + batchFile);
        }
        // Use a big buffer
        return new BufferedWriter(new FileWriter(batchFile, true), Config.PAGES_FILE_BUFFER_SIZE);
    }
}
