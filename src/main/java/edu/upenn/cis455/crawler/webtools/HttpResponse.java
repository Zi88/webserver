package edu.upenn.cis455.crawler.webtools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alex on 1/24/18.
 */
public class HttpResponse {

    private InputStream body;
    private String statusMsg;
    private int statusCode;
    private String proto;
    private Map<String, String> headers = new HashMap<String, String>();

    public InputStream getBody() {
        return body;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setBody(InputStream body) {
        this.body = body;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getProto() {
        return proto;
    }

    public void setProto(String proto) {
        this.proto = proto;
    }

    public void addHeader(String key, String val) {
        headers.put(key, val);
    }

    public Set<Map.Entry<String, String>> getAllHeaders() {
        return headers.entrySet();
    }

    public String getHeader(String s) {
        return headers.get(s);
    }

    /**
     * A convenience method that reads the entire body InputStream to a byte array.  Should only be used when the
     * body is known to be small enough to fit in memory (like for testing). The InputStream is not reset and can't be reused
     * @return
     */
    public byte[] getBodyAsByteArray() throws IOException {
        byte[] buff = new byte[9182];
        int n;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while((n = body.read(buff, 0, buff.length)) != -1) {
            baos.write(buff, 0, n);
        }
        return baos.toByteArray();
    }
}
