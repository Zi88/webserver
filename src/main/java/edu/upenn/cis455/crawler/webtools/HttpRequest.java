package edu.upenn.cis455.crawler.webtools;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 1/23/18.
 */
public class HttpRequest {

    private String method;
    private URI uri;
    private String proto;
    private Map<String, String> headers = new HashMap<>();
    private String body;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public URI getURI() {
        return uri;
    }

    public String getProto() {
        return proto;
    }

    public void setURI(URI uri) {
        this.uri = uri;
    }

    public void setProto(String proto) {
        this.proto = proto;
    }

    public void addHeader(String key, String val) {
        headers.put(key, val);
    }

    public String getHeader(String key) {
        return headers.get(key);
    }

    public String getPath() {
        return uri.getPath();
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }
}
