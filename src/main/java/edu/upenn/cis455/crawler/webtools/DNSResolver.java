package edu.upenn.cis455.crawler.webtools;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Alex on 3/1/18
 */
public class DNSResolver {

    /**
     * @param host A valid hostname e.g. cis.upenn.edu
     * @return an InetAddress object
     */
    public static InetAddress resolve(String host) throws UnknownHostException {
        return InetAddress.getByName(host);
    }
}
