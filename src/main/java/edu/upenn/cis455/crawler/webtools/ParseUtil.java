package edu.upenn.cis455.crawler.webtools;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by Alex on 2/12/18
 *
 * This class is very similar to HttpUtil, but it does some extra parsing to support more advanced requirements of
 * HttpServletRequests (e.g. headers with multiple values)
 */
public class ParseUtil {

    final static Map<Integer, String> httpMsgs = new HashMap<>();
    static {
        // Relevant 200s
        httpMsgs.put(200, "OK");
        httpMsgs.put(202, "Accepted");
        httpMsgs.put(204, "No Content");

        // Relevant 300s
        httpMsgs.put(301, "Moved Permanently");
        httpMsgs.put(303, "See Other");
        httpMsgs.put(304, "Not Modified");

        // Relevant 400s
        httpMsgs.put(400, "Bad Request");
        httpMsgs.put(401, "Unauthorized");
        httpMsgs.put(403, "Forbidden");
        httpMsgs.put(404, "Not Found");
        httpMsgs.put(405, "Method Not Allowed");
        httpMsgs.put(412, "Precondition Failed");
        httpMsgs.put(413, "Payload Too Large");
        httpMsgs.put(414, "URI Too Long");
        httpMsgs.put(417, "Expectation Failed");
        httpMsgs.put(431, "Request Header Fields Too Large");

        // Relevant 500s
        httpMsgs.put(500, "Internal Server Error");
        httpMsgs.put(501, "Not Implemented");
        httpMsgs.put(500, "Internal Server Error");
        httpMsgs.put(505, "HTTP Version Not Supported");
    }


    /**
     * Add a header, tokenizing comma-separated header values if the given header accepts such lists
     * @param resp A response object
     * @param split an array of key, valueString, where the value string may be comma separated, distinct header values
     */
    public static void addHeader(Response resp, String[] split) {
        // Possible that the header doesn't have a colon
        if (split.length != 2) {
            return;
        }
        // Support as many headers as there are, separated by commas
        String key = split[0].trim();
        String vals = split[1].trim();

        // Some headers have values defined comma separated lists, which need to be added individually
        if (acceptsCommaList(key)) {
            for (String v : vals.split(",")) {
                // Just in case something like "v1,v2,v3," causes weirdness
                if (v.equals("")) {
                    continue;
                }
                resp.addHeader(key, v.trim());
            }
            return;
        }

        // Add normal header
        resp.addHeader(key, vals);
    }

    /**
     * Return true if key is the name of a header whose field value is defined as a comma separated list.
     * List of such headers found here: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
     * @param key
     * @return
     */
    private static boolean acceptsCommaList(String key) {

        switch (key.toLowerCase()) {
            case "keep-alive":
            case "accept":
            case "accept-charset":
            case "accept-encoding":
            case "accept-language":
            case "x-forwarded-for":
            case "content-encoding":
            case "content-language":
            case "allow":
            case "range":
            case "transfer-encoding":
            case "te":
            case "trailer":
            case "expect-ct":
            case "vary":
            case "test-header":
                return true;
            default:
                return false;
        }
    }
}
