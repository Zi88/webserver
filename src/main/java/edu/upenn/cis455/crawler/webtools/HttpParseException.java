package edu.upenn.cis455.crawler.webtools;

/**
 * An exception meant for dealing with common HTTP error codes that don't require anything but a generic
 * response (e.g. 404, 500, etc.)  This is a runtime exception so that I can throw it from inside my default
 * servlets, which originally implemented PathHandler
 */
public class HttpParseException extends Exception {
    private int code;

    public HttpParseException(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}

