package edu.upenn.cis455.crawler.webtools;

import com.amazonaws.services.xray.model.Http;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 1/23/18.
 */
public class HttpUtil {

    final static int MAX_BUFF_LEN = 8192;
    final static int MAX_LINE_LEN = 1024;
    final static Map<Integer, String> httpMsgs = new HashMap<>();
    static {
        // Relevant 200s
        httpMsgs.put(200, "OK");
        httpMsgs.put(202, "Accepted");
        httpMsgs.put(204, "No Content");

        // Relevant 300s
        httpMsgs.put(301, "Moved Permanently");
        httpMsgs.put(302, "Redirect");
        httpMsgs.put(303, "See Other");
        httpMsgs.put(304, "Not Modified");

        // Relevant 400s
        httpMsgs.put(400, "Bad Request");
        httpMsgs.put(401, "Unauthorized");
        httpMsgs.put(403, "Forbidden");
        httpMsgs.put(404, "Not Found");
        httpMsgs.put(405, "Method Not Allowed");
        httpMsgs.put(412, "Precondition Failed");
        httpMsgs.put(413, "Payload Too Large");
        httpMsgs.put(414, "URI Too Long");
        httpMsgs.put(417, "Expectation Failed");
        httpMsgs.put(431, "Request Header Fields Too Large");

        // Relevant 500s
        httpMsgs.put(500, "Internal Server Error");
        httpMsgs.put(501, "Not Implemented");
        httpMsgs.put(500, "Internal Server Error");
        httpMsgs.put(505, "HTTP Version Not Supported");
    }

    /**
     *
     * @param is
     * @return a request object representing all the info sent by the client
     * @throws IOException
     * @throws HttpParseException
     */
    public static HttpRequest decodeRequest(InputStream is) throws IOException, HttpParseException {

        BufferedInputStream bis = new BufferedInputStream(is);
        String line = readLine(bis);
        if (line.equals("")) {
            throw new HttpParseException(400);
        }
        String[] requestLine = line.split("\\s", 3);
        if (requestLine.length != 3) {
            throw new HttpParseException(400);
        }
        HttpRequest req = new HttpRequest();
        req.setMethod(requestLine[0]);
        req.setProto(requestLine[2]);

        // Read any number of headers, until we encounter an empty string (the break between headers and body)
        line = readLine(bis);
        while (!line.matches("")) {
            String[] split = line.split(":", 2);
            if (split.length != 2) {
                line = readLine(bis);
                continue;
            }
            req.addHeader(split[0], split[1].trim());
            line = readLine(bis);
        }

        // Read the body if Content-Length header was given
        int clen = -1;
        if (req.getHeader("Content-Length") != null) {
            try {
                clen = Integer.parseInt(req.getHeader("Content-Length"));
            } catch (NumberFormatException ignored) {}
        }


        byte[] bytes = new byte[MAX_BUFF_LEN];
        int read = 0;
        int n = 0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while (clen > 0 && (read = bis.read(bytes, 0, bytes.length)) != -1) {
            baos.write(bytes, 0, read);
            n += read;
            if (n >= clen) {
                break;
            }
        }
        String body = new String(baos.toByteArray());

        // Unrecognized protocol
        if (!requestLine[2].equals("HTTP/1.1") && !requestLine[2].equals("HTTP/1.0")) {
            throw new HttpParseException(505);
        }

        // Malformed URI/Path
        try {
            req.setURI(new URI(requestLine[1]));
        } catch (URISyntaxException e) {
            throw new HttpParseException(400);
        }

        // HTTP/1.1 must include Host header
        if (req.getProto().equals("HTTP/1.1") && req.getHeader("Host") == null) {
            throw new HttpParseException(400);
        }

        // Message will have body if Content-Length is set
        if (!body.equals("")) {
            req.setBody(body);
        }
        
        return req;
    }

    /**
     *
     * @param status the integer status of an HTTP response (e.g. 404)
     * @return A useable message for the given status code (e.g. "Not Found"), or null if none is found for given code
     */
    public static String getHttpMsg(int status) {
        String msg = httpMsgs.get(status);
        // May be possible that a servlet has some arbitrary code
        if (msg == null) {
            return "";
        }
        return msg;
    }

    private static void addHeader(HttpRequest req, String[] split) throws HttpParseException {
        // Possible that the header doesn't have a colon
        if (split.length != 2) {
            throw new HttpParseException(400);
        }
        String key = split[0].trim();
        String val = split[1].trim();
        req.addHeader(key, val);
    }

    /**
     * Given an output stream, write the full response to the client (no chunking). Response object's body can't be reused,
     * since we read InputStream.
     * @param os
     * @param resp
     */
    public static void encodeResponse(OutputStream os, HttpResponse resp) throws IOException {
        BufferedOutputStream outStream = new BufferedOutputStream(os);

        // Set the reason phrase if not already set
        if (resp.getStatusMsg() == null) {
            resp.setStatusMsg(HttpUtil.getHttpMsg(resp.getStatusCode()));
        }

        // Set Content-Length if not already set.
        // This should have already been set and may be incorrect (depending on underlying InputStream) if set here
        if (resp.getBody() != null && resp.getHeader("Content-Length") == null) {
            resp.addHeader("Content-Length", Integer.toString(resp.getBody().available()));
        }

        // Write initial status line
        String statusLine = String.format("%s %d %s\r\n", resp.getProto(), resp.getStatusCode(), resp.getStatusMsg());
        outStream.write(statusLine.getBytes());

        // Write headers
        for(Map.Entry<String, String> e : resp.getAllHeaders()) {
            String header = String.format("%s: %s\r\n", e.getKey(), e.getValue());
            outStream.write(header.getBytes());
        }

        // Write the body, as long as there is one
        outStream.write("\r\n".getBytes());
        if (resp.getBody() != null) {
            BufferedInputStream bis = new BufferedInputStream(resp.getBody());
            byte[] buff = new byte[MAX_BUFF_LEN];
            int n = 0;
            while ((n = bis.read(buff, 0, buff.length)) != -1) {
                outStream.write(buff, 0, n);
            }
            resp.getBody().close();
        }

        outStream.close();
    }


     /* Read just a single line from an InputStream, until any newline characters are encountered.
     * Ensures that the stream is positioned at the beginning of the next line
     * @param in the InputStream to read from (e.g. a socket)
     * @return a String for the line
     */
    private static String readLine(BufferedInputStream in) throws IOException {
        // Read byte by byte until a linefeed character is encountered
        int readByte;
        StringBuilder sb = new StringBuilder();
        while ((readByte = in.read()) != -1 && readByte != '\n') {
            sb.append((char) readByte);
        }
        return sb.toString().trim();
    }
}
