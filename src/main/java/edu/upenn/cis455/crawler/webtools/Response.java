package edu.upenn.cis455.crawler.webtools;

import edu.upenn.cis455.crawler.utils.CrawlerUtils;

import java.io.Serializable;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by Alex on 3/1/18
 */
public class Response implements Serializable {

    private int statusCode;
    private byte[] body;
    private long timestamp;   // The timestamp for when the crawler issued the HEAD request for this resource
    private URL url;          // The normalized URL that this request corresponds to
    private HashMap<String, List<String>> headers = new HashMap<>();


    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] buf) {
        this.body = buf;
    }

    // For now, store body as a string
    public String getBodyStr() {
        if (body == null) {
            return null;
        }
        return new String(body, pickCharset());
    }

    /**
     * Infer the Charset used by the underling byte array. This is only meaningful for text/XXX MIME types
     * (not for binary data), and it isn't perfect.
     */
    private Charset pickCharset() {

        // UTF-8 seems to be the common default on the web, in defiance of HTTP's specified ISO-8859-1
        String ctype = getHeader("Content-Type");
        if (ctype == null) {
            return StandardCharsets.UTF_8;
        }

        ctype = ctype.toLowerCase();
        if (ctype.contains("utf-16")) {
            return StandardCharsets.UTF_16;
        }
        if (ctype.contains("iso-8859")) {
            return StandardCharsets.ISO_8859_1;
        }
        return StandardCharsets.UTF_8;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public boolean containsHeader(String s) {
        return headers.containsKey(s.toLowerCase());
    }

    public long getDateHeader(String s) {
        String timestamp = getHeader(s.toLowerCase());
        return CrawlerUtils.parseTimestamp(timestamp);
    }

    public long getLongHeader(String s) {
        String val = getHeader(s.toLowerCase());
        try {
            return Long.parseLong(val);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /**
     * Return just the first value for the header with the given name, if the header is present
     */
    public String getHeader(String s) {
        List<String> vals = headers.get(s.toLowerCase());
        if (vals == null || vals.size() == 0) {
            return null;
        }
        return vals.get(0);
    }

    /**
     * Return the list of all values for the header with the given name, if it exists
     */
    public List<String> getHeaders(String s) {
        return headers.get(s.toLowerCase());
    }

    /**
     * Set the header, clearing any preexisting headers with the same name
     */
    public void setHeader(String s, String s1) {
        if (s == null || s1 == null) {
            return;
        }
        List<String> vals = new ArrayList<>();
        vals.add(s1.trim());
        headers.put(s.toLowerCase(), vals);
    }

    /**
     * Add the value to the list of values for this header, keeping any preexisting values intact
     */
    public void addHeader(String k, String v) {
        List<String> vals = headers.get(k.toLowerCase());
        if (vals == null) {
            vals = new ArrayList<>();
            headers.put(k.toLowerCase(), vals);
        }
        vals.add(v.trim());
    }

    public boolean isRedirect() {
        // We are not going to support 300, 304, or 305, and for now, we treat all other redirects the same
        switch (statusCode) {
            case 301:
            case 302:
            case 303:
            case 307:
            case 308:
                return true;
            default:
                return false;
        }
    }

    public HashMap<String, List<String>> getHeaders() {
        return headers;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public URL getUrl() {
        return url;
    }
}
