package edu.upenn.cis455.crawler.webtools;

import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.interfaces.ProtocolClient;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created by Alex on 3/1/18
 * Added by Ziyu: gzip to request compressed body and decompress
 */
public class HTTPClient implements ProtocolClient {

    public enum RequestType {
        PREFETCH,
        DOWNLOAD,
        POST
    }

    /**
     * @param url url for the resource we wish to get (i.e. a GET request)
     * @return a Response object
     */
    @Override
    public Response getResource(URL url) throws IOException, WebException {
        return getStreamsThenTransact(url, RequestType.DOWNLOAD, null);
    }

    /**
     * Prefetch a resource to get a response with metadata (i.e. a HEAD request)
     *
     * @param url url to request
     * @return a Response object
     */
    @Override
    public Response prefetch(URL url) throws IOException, WebException {
        return getStreamsThenTransact(url, RequestType.PREFETCH, null);
    }

    /**
     * Send a POST request, used internally for RPCs
     */
    public Response postResource(URL url, String body) throws IOException, WebException {
        return getStreamsThenTransact(url, RequestType.POST, body);
    }

    private Response getStreamsThenTransact(URL url, RequestType type, String body) throws IOException, WebException {
        InputStream is = null;
        OutputStream os = null;

        // Do the raw socket stuff if it's plain old HTTP
        if (url.getProtocol().toLowerCase().equals("http")) {
            int port = (url.getPort() == -1) ? 80 : url.getPort();
            InetAddress addr = DNSResolver.resolve(url.getHost());
            Socket sock = new Socket(addr, port);
            sock.setSoTimeout(Config.DEFAULT_SO_TIMEOUT);
            is = sock.getInputStream();
            os = sock.getOutputStream();
        }

        // Use URL class's built in Connection for HTTPS
        else if (url.getProtocol().toLowerCase().equals("https")) {
            return doHttpsTransaction(url, type);
        }

        if (is == null || os == null) {
            throw new WebException("Unrecognized protocol for URL: " + url);
        }

        // Either do a HEAD or a GET request
        Response r;
        if (type == RequestType.PREFETCH) {
            r = doHEAD(is, os, url);
        } else if (type == RequestType.DOWNLOAD) {
            r = doGET(is, os, url);
        } else {
            r = doPOST(is, os, url, body);
        }

        // Set the request timestamp, close the underlying streams
        r.setTimestamp(System.currentTimeMillis());
        is.close();
        os.close();
        return r;

    }

    private Response doHttpsTransaction(URL url, RequestType type) throws IOException {
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();;
        conn.setReadTimeout(Config.DEFAULT_SO_TIMEOUT);

        // Set the request method
        if (type == RequestType.PREFETCH) {
            conn.setRequestMethod("HEAD");
        } else {
            conn.setRequestMethod("GET");
        }

        // Set relevant headers. When downloading, advertise that we will accept compressed content
        conn.addRequestProperty("User-Agent", Config.CRAWLER_NAME);
        conn.addRequestProperty("Host", url.getHost());
        if (type == RequestType.DOWNLOAD) {
            conn.addRequestProperty("Accept-Encoding", "gzip");
        }
        
        conn.connect();

        // Build a Response object
        Response r = new Response();
        r.setStatusCode(conn.getResponseCode());
        r.setTimestamp(System.currentTimeMillis());

        // Add headers, though we won't care about most of these
        for (Map.Entry<String, List<String>> e: conn.getHeaderFields().entrySet()) {
            if (e.getKey() != null && e.getValue() != null) {
                for (String v : e.getValue()) {
                    r.addHeader(e.getKey(), v);
                }
            }
        }

        // Return without reading body for HEAD requests
        if (type == RequestType.PREFETCH) {
            return r;
        }

        // Make sure content length doesn't exceed what's allowed
        boolean isChunked = isChunked(r);
        int clen = conn.getContentLength();
        
        if (!isChunked && clen < 0) {
            throw new IOException("Invalid content length in GET request");
        }

        // Already check this in the HEAD filter, but do it again anyway
        if (clen > Config.MAX_CONTENT_LEN_MB * 1000000) {
            throw new IOException(String.format("Content length too long (%d)", clen));
        }

        // Explicitly set content type and length
        r.setHeader("Content-Type", conn.getContentType());
        r.setHeader("Content-Length", Integer.toString(clen));

        // Determine which stream to read from (they're different for some reason)
        InputStream in;
        if (type == RequestType.DOWNLOAD && conn.getResponseCode() <= 299 && conn.getResponseCode() >= 200) {
            in = conn.getInputStream();
        }
        else {
            in = conn.getErrorStream();
        }

        // getErrorStream() can return null (e.g. if connection was never successfully established)
        if (in == null) {
            throw new IOException(url + ": Unable to get valid stream for HTTPS transaction");
        }

        // Read the body either straight, or decompress then read
        byte[] body;
        if (isCompressed(r)) {
            body = readCompressed(in);
            // Always store, the true, uncompressed Content-Length
            r.setHeader("Content-Length", Integer.toString(body.length));
        } else {
            body = readBody(in, clen);
        }
        r.setBody(body);
        in.close();
        return r;
    }

    private Response doHEAD(InputStream is, OutputStream os, URL url) throws IOException {
        String path = (url.getPath().equals("")) ? "/" : url.getPath();
        String requestLine = String.format("HEAD %s HTTP/1.1\r\n", path);
        return writeThenRead(is, os, url, requestLine, null, RequestType.PREFETCH);
    }

    private Response doGET(InputStream is, OutputStream os, URL url) throws IOException {
        String path = (url.getPath().equals("")) ? "/" : url.getPath();
        String requestLine = String.format("GET %s HTTP/1.1\r\n", path);
        return writeThenRead(is, os, url, requestLine, null, RequestType.DOWNLOAD);
    }

    private Response doPOST(InputStream is, OutputStream os, URL url, String bodyJson) throws IOException {
        String path = (url.getPath().equals("")) ? "/" : url.getPath();
        String requestLine = String.format("POST %s HTTP/1.1\r\n", path);
        return writeThenRead(is, os, url, requestLine, bodyJson, RequestType.POST);
    }

    private Response writeThenRead(InputStream is, OutputStream os, URL url, String requestLine, String body, RequestType type) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(os);

        // Write the request
        writeRequest(bos, requestLine, body, url);
        bos.flush();
        
        return readResponse(is, type);
    }


    /**
     * Write an HTTP request to the webserver
     */
    private void writeRequest(OutputStream os, String requestLine, String body, URL url) throws IOException {
        String request =
                requestLine +
                        "User-Agent: " + Config.CRAWLER_NAME + "\r\n" +
                        "Host: " + url.getHost() + "\r\n" +
                        "Accept-Language: en-US, en;q=0.9, *;q=0.5\r\n";
        // TODO keep this out for now, because of messiness from inflating the stream
//        // Advertise that we accept compressed content if making a GET request
//        if (requestLine.startsWith("GET")) {
//            request = request + "Accept-Encoding: gzip\r\n";
//        }
        // Add a Content-Length header if sending a body
        if (body != null) {
            request = request + "Content-Length: " + body.length() + "\r\n";
        }
        os.write(request.getBytes());
        os.write("\r\n".getBytes());
        // Write the actual body if sending it
        if (body != null) {
            os.write(body.getBytes());
        }
        os.flush();
    }

    /**
     * Read the webserver's response and parse it into a useable response.  This may timeout or otherwise throw
     * and IOException if something goes wrong.
     */
    public Response readResponse(InputStream is, RequestType type) throws IOException {
        Response resp = new Response();

        // Read the status line, parse and set the status code
        BufferedInputStream bis = new BufferedInputStream(is);

        String statusLine = readLine(bis);
        if (statusLine.equals("")) {
            throw new IOException("Status line not parsed from socket");
        }
        String[] split = statusLine.split("\\s", 3);
        if (split.length != 3) {
            throw new IOException("Malformed status line");
        }
        try {
            int statusCode = Integer.parseInt(split[1]);
            resp.setStatusCode(statusCode);
        } catch (NumberFormatException ignored) {
            throw new IOException("Status line has malformed code : " + statusLine);
        }

        // Read any number of headers, until we encounter an empty string (the break between headers and body)
        String line = readLine(bis);
        while (!line.matches("")) {
            split = line.split(":", 2);
            if (split.length != 2) {
                line = readLine(bis);
                continue;
            }
            // ParseUtil will figure out if the header accepts a comma-separated list and will tokenize if so
            ParseUtil.addHeader(resp, split);
            line = readLine(bis);
        }


        // Don't read the body if this request doesn't expect one
        if (type == RequestType.PREFETCH) {
            return resp;
        }

        // If not chunked, only read responses that have set a valid Content-Length header
        boolean isChunked = isChunked(resp);

        int clen = (int) resp.getLongHeader("Content-Length");
        if (!isChunked && (clen < 0 || clen > Config.MAX_CONTENT_LEN_MB * 1000000)) {
            System.err.println(resp.getUrl() + ": No valid Content-Length on GET. Proceeding anyway");
            clen = (int) Config.MAX_CONTENT_LEN_MB;
        }

        // Read the body from the raw inputstream, either chunked or unchunked
        byte[] body;
        if (isChunked) {
            body = readBodyChunked(bis);
        } else {
            body = readBody(bis, clen);
        }

        // We've read the body, but it could still be compressed content.  Read it again in its uncompressed form if so.
        if (isCompressed(resp)) {
            ByteArrayInputStream bais = new ByteArrayInputStream(body);
            body = readCompressed(bais);
            // Always store, the true, uncompressed Content-Length
            resp.setHeader("Content-Length", Integer.toString(body.length));
        }

        resp.setBody(body);
        return resp;
    }

    /**
     * @param in an input stream (e.g. from a socket) to read from
     * @param clen the expected content length
     * @return a String containing the bytes read from the socket
     */
    private byte[] readBody(InputStream in, int clen) throws IOException {
        // Read the body as raw bytes
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[8192];
        int n = 0, bytesRead = 0;
        while ((bytesRead = in.read(buf, 0, buf.length)) != -1) {
            baos.write(buf, 0, bytesRead);
            n += bytesRead;
            // Quit reading
            if (n >= clen) {
                break;
            }
        }
        return baos.toByteArray();
    }

    /**
     * Read chunked body data of indefinite size, up to a predefined limit
     * @param in Underlying socket
     * @return a byte array
     */
    private byte[] readBodyChunked(InputStream in) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        long max = Config.MAX_CONTENT_LEN_MB * 1000000;
        long n = 0;
        byte[] buf = new byte[16];
        while (n < max) {
            int csize = CrawlerUtils.asciiToHex((char) in.read());

            // Could be a corrupted chunk, or EOF
            if (csize <= 0) {
                break;
            }

            // Should handle CLRF as well as LF, though CLRF is expected
            char next = (char) in.read();
            while (next != '\n') {
                next = (char) in.read();
            }

            // Stream should now be in position to read csize bytes, though it could always take multiple calls to read d
            int read = in.read(buf, 0, csize);
            // TODO occassional index out of bounds here...are they allowed to send more than 16 bytes
            while (read != csize) {
                read += in.read(buf, read, csize - read);
            }
            n += csize;
            baos.write(buf, 0, csize);

            // Read the line separator again
            next = (char) in.read();
            while (next != '\n') {
                next = (char) in.read();
            }

        }
        return baos.toByteArray();
    }

    /**
     * Wraps readBody, but adds a default length to deal with fact that we don't know uncompressed Content-Length
     */
    private byte[] readCompressed(InputStream in) throws IOException {
        GZIPInputStream zis = new GZIPInputStream(in);
        int max = (int) (Config.MAX_CONTENT_LEN_MB * 1000000);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buf = new byte[8192];
            int n = 0, bytesRead = 0;
            while ((bytesRead = zis.read(buf, 0, buf.length)) != -1) {
                baos.write(buf, 0, bytesRead);
                n += bytesRead;
                // Quit reading
                if (n >= max) {
                    break;
                }
            }
        }
        // This EOFException seems to get thrown needlessly when the stream is empty. Just catch it here,
        // as suggested: https://stackoverflow.com/questions/18837017/eofexception-unexpected-end-of-zlib-input-stream
        catch (EOFException ignored) {
            zis.close();
            ignored.printStackTrace();
        }
        return baos.toByteArray();
    }

    /**
     * Read just a single line from an InputStream, until any newline characters are encountered.
     * Ensures that the stream is positioned at the beginning of the next line
     * @param in the InputStream to read from (e.g. a socket)
     * @return a String for the line
     */
    private String readLine(BufferedInputStream in) throws IOException {
        // Read byte by byte until a linefeed character is encountered
        int readByte;
        StringBuilder sb = new StringBuilder();
        while ((readByte = in.read()) != -1 && readByte != '\n') {
            sb.append((char) readByte);
        }
        return sb.toString().trim();
    }

    private boolean isChunked(Response resp) {
        if (resp.getHeaders("Transfer-Encoding") != null) {
            for (String val : resp.getHeaders("Transfer-Encoding")) {
                if (val.toLowerCase().contains("chunked")) {
                   return true;
                }
            }
        }
        return false;
    }

    private boolean isCompressed(Response resp) {
        if (resp.getHeaders("Content-Encoding") != null) {
            for (String val : resp.getHeaders("Content-Encoding")) {
                if (val.toLowerCase().contains("gzip")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) throws IOException, WebException {
        URL url = new URL("http://www.gutenberg.org/catalog/");
        HTTPClient c = new HTTPClient();
        Response pre = c.prefetch(url);
        System.out.println("Prefetch length (advertised uncompressed length): " + pre.getLongHeader("Content-Length"));
        Response r = c.getResource(url);
        System.out.println("Fetched length after uncompression: " + r.getLongHeader("Content-Length"));

    }
}
