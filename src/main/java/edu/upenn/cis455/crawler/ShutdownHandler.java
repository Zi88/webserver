package edu.upenn.cis455.crawler;

import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import edu.upenn.cis455.storage.BerkeleyManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.awt.print.Book;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Alex on 4/14/18
 */
public class ShutdownHandler extends Bookkeeper {

    private final static Logger LOG = LogManager.getLogger(ShutdownHandler.class);
    private LocalCluster cluster;

    public ShutdownHandler(MercatorFrontier frontier, LocalCluster cluster) {
        super(frontier);
        this.cluster = cluster;
    }

    @Override
    public void run() {
        LOG.error("Shutdown hook invoked");
        LOG.error("Docs Crawled: " + docsCrawled);
        try {
            saveFrontier();
            cluster.shutdown();
            flushLogs();
            BerkeleyManager.close();
        } catch (IOException e) {
            LOG.error("Error when handling shutdown", e);
        }

    }

    private void flushLogs() {
        // // TODO: 4/17/18
    }
}
