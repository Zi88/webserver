package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.crawler.interfaces.SerializationProvider;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Alex on 4/15/18
 */
public class URLSerializationProvider implements SerializationProvider<URL> {

    @Override
    public String writeObject(URL obj) {
       return obj.toString();
    }

    @Override
    public URL readObject(String serialized) {
        try {
            return new URL(serialized);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    @Override
    public String generateKey(URL obj) {
        return obj.getHost();
    }

    @Override
    public String[] tokenize(URL obj) {
        // Want an array that looks like ["edu", "upenn", "cis"...]
        String host = obj.getHost();
        String[] split = host.split("\\.");
        String[] reversed = new String[split.length];
        for (int i = 0; i < split.length; i++) {
            reversed[i] = split[split.length - i - 1];
        }
        return reversed;
    }
}
