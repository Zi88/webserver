package edu.upenn.cis455.crawler.utils;

import java.net.URL;

/**
 * This serialization provider ignores certain subdomains when keying urls.  This is done so that related URLs with
 * technically different hostnames end up in the same frontend queues more often, for greater politeness.
 */
public class ConservativeURLSerializationProvider extends URLSerializationProvider {

    @Override
    public String generateKey(URL obj) {
        // Key all stackexchange things to the main domain
        if (obj.getHost().contains("stackexchange.com")) {
            return "stackexchange.com";
        }
        if (obj.getHost().contains("eater.com")) {
            return "eater.com";
        }
        return super.generateKey(obj);
    }
}
