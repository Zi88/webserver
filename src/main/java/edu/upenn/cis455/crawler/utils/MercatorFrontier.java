package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.interfaces.CleaningCallback;
import edu.upenn.cis455.crawler.interfaces.Prioritizer;
import edu.upenn.cis455.crawler.interfaces.QueueScheduler;
import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import edu.upenn.cis455.crawler.webtools.ParseUtil;
import edu.upenn.cis455.extras.DeficitRoundRobin;
import edu.upenn.cis455.extras.StaticPrioritizer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Alex on 3/7/18
 */
public class MercatorFrontier<T> {

    private SerializationProvider<T> provider;
    private Prioritizer<T> prioritizer;
    private QueueScheduler<T> drr = new DeficitRoundRobin<T>();

    private static final int k = 10;                      // Number of frontend queues
    private static final int n = 5 * Config.NUM_WORKERS;  // Number of backend queues

    private volatile PriorityQueue<HeapEntry<T>> queueByTimestamp;
    private volatile Map<String, BufferedQueue<T>> queueByHostname;
    private volatile List<BufferedQueue<T>> frontendQueues;
    private volatile Cache<String, Long> timestampHints;
    //private volatile Set<Integer> nonemptyFrontends;  // TODO: 3/27/18 could make some optimizations with this

    private volatile List<BufferedQueue<T>> backendQueues;
    private volatile Deque<BufferedQueue<T>> emptyBackends;

    public synchronized boolean isEmpty() {
        return queueByTimestamp.isEmpty();
    }


    public MercatorFrontier(SerializationProvider<T> genericProvider, Prioritizer<T> genericPrioritizer) {

        provider = genericProvider;
        prioritizer = genericPrioritizer;

        // Create the mappings
        queueByTimestamp = new PriorityQueue<>((o1, o2) -> Long.compare(o1.timestamp, o2.timestamp));
        queueByHostname = new HashMap<>();
        timestampHints = new Cache<>();

        // Create the queue holders
        frontendQueues = new ArrayList<>();
        backendQueues = new ArrayList<>();
        emptyBackends = new LinkedList<>();
        //nonemptyFrontends = new HashSet<>();
    }

    /**
     * Enqueue the starting value directly into a backend queue.
     */
    public void initialize(List<T> startingVals, String workingDir) {

        // Create the actual queues, opening the underlying files and rebuilding mappings, etc.
        buildQueues(workingDir);
        prioritizer.initialize(frontendQueues);
        drr.initialize(frontendQueues, null);

        // Enqueue all elements normally. enqueue() will look for empty backends and fill them
        for (int i = 0; i < startingVals.size(); i++) {
            enqueue(startingVals.get(i));
        }

    }

    private void buildQueues(String workingDir) {
        // Create the frontend queues
        for (int i = 0; i < k; i++) {
            // Buffer queue uses default buffer size and flushes on write
            File f = Paths.get(workingDir + "/frontend-" + i + ".txt").toFile();
            frontendQueues.add(new BufferedQueue<>(f, provider, -1));
        }

        // Create the backend queues
        for (int i = 0; i < n; i++) {
            // Default buffer size and flush on write for queue
            File f = Paths.get(workingDir + "/backend-" + i + ".txt").toFile();
            BufferedQueue<T> q = new BufferedQueue<>(f, provider, -1);
            backendQueues.add(q);
            // If newly created queue is empty, schedule it to be refilled
            if (q.isEmpty()) {
                emptyBackends.add(q);
            }
            // Otherwise, rebuild the queue's heap entry
            else {
                initializeNonemptyBackend(q);
            }
        }
    }

    private void initializeNonemptyBackend(BufferedQueue<T> q) {
        // Grab an element from the queue, without removing it
        T elt = q.element();
        String key = provider.generateKey(elt);

        // Build a heap entry associated with this queue
        HeapEntry<T> he = new HeapEntry<>();
        he.queueHandle = q;
        he.hostname = key;
        // Get a timestamp between now and ~3 minutes from now. Meant to cut down on bursty traffic on startup
        he.timestamp = determineTimestamp(he.hostname, Config.COURTESY_DELAY * 18);

        // Add heap entry and hostname mapping
        queueByTimestamp.add(he);
        queueByHostname.put(key, he.queueHandle);
    }

    /**
     * Enqueue: puts the url in one of the frontend queues, based on a priority scheme.
     */
    public synchronized void enqueue(T val) {
        // Enqueue the element based on a prioritization scheme
        prioritizer.prioritize(val);

        // Add the url, and mark this queue as nonempty
        //nonemptyFrontends.add(i);

        // Check if there are any empty, orphan backend queues, attempt to fill one of them
        if (emptyBackends.size() > 0) {
            HeapEntry<T> e = new HeapEntry<>();
            e.queueHandle = emptyBackends.remove();
            refill(e);
        }
    }

    /**
     * Dequeue: hands the caller a backend queue (an entire queue, not just a URL)
     */
    public synchronized HeapEntry<T> dequeue() {
        // Extract min from min-heap.  Each hostname has at most one heap entry, so removing the heap entry prevents other threads from contacting that host
        return queueByTimestamp.remove();
    }

    /** Reinsert a HeapEntry (with updated timestamp) back into the heap, refilling its queue if it's empty **/
    public synchronized void reinsert(String hostname, long timestamp) {

        // Create a fresh HeapEntry for this hostname
        HeapEntry<T> entry = new HeapEntry<>();
        entry.queueHandle = queueByHostname.get(hostname);
        entry.hostname = hostname;
        entry.timestamp = timestamp;

        // TODO: Maybe refills should happen strictly at frontend enqueue?  I.e. only reinsert when the queue is nonempty. Otherwise let it die
        // Refill the backend queue from the frontend queue if necessary
//        if (entry.queueHandle.isEmpty()) {
//            refill(entry);
//        }

        // TODO this isn't great, and somehow throws an NPE on startup in rare cases
        // If the backend queue is now empty, remove the mapping, but add a hint so we remember the timestamp
        if (entry.queueHandle.isEmpty()) {
            timestampHints.put(entry.hostname, entry.timestamp);
            queueByHostname.remove(hostname);
            emptyBackends.add(entry.queueHandle);
        }
        // If backend queue is not empty, insert the rebuilt HeapEntry, so a new URL can be emitted
        else {
            queueByTimestamp.add(entry);
        }
    }

    /** Refill a backend queue by pulling entries out of the frontend queues **/
    private synchronized void refill(HeapEntry<T> e) {

        // Add the backend queue to the end of the list of empty backends. It will remain there unless successfully filled
        emptyBackends.add(e.queueHandle);

        // Don't bother trying to fill a backend queue if there's no frontend queues to pull from
        if (allFrontendsEmpty()) {
            return;
        }

        // Backend queues always have at most one consumer, but maybe many producers (any worker can add to a backend queue)
        while (e.queueHandle.isEmpty() && !allFrontendsEmpty()) {

            // Selected queue is guaranteed to be nonempty
            T elt = getFrontendEntry();
            // Catch deserialization problems (e.g. MalformedURLExceptions)
            if (elt == null) {
                continue;
            }

            // Check if hostname for removed URL has a corresponding backend queue
            String hostname = provider.generateKey(elt);
            Queue<T> backendQ = queueByHostname.get(hostname);
            // If so, add it to that backend queue and keep looking for a fresh hostname
            if (backendQ != null) {
                backendQ.add(elt);
                continue;
            }
            // Otherwise, refill the empty backend queue and remove it from the list of empty backends
            emptyBackends.removeLast();
            e.queueHandle.add(elt);
            // If queue now contains a fresh host, let the worker contact the host after a randomized delay
            e.timestamp = determineTimestamp(hostname, Config.COURTESY_DELAY * 2);
            // Mark new mapping
            queueByHostname.put(hostname, e.queueHandle);
            queueByTimestamp.add(e);
        }
    }

    /**
     * When refilling an empty backend queue, check for a timestamp hint.  Do this because a hostname's heap entry
     * gets removed as soon as its backend queue is empty, which leads to a lot of evictions if the hostname is
     * uncommon or in a low priority frontend queue that doesn't often get pulled from.
     *
     * If we don't find a hint, set a random delay from a large range.
     * This cuts down on bursty requests: we don't hit a.foo.com, b.foo.com, and c.foo.com all at the same time
     */
    private long determineTimestamp(String hostname, int randomRange) {
        Long hinted = timestampHints.get(hostname);

        if (hinted == null) {
            long randomDelay = new Random().nextInt(randomRange);
            return System.currentTimeMillis() + randomDelay;
        }
        return hinted;
    }

    // Return true if all frontend queues are empty, false otherwise.  Could give wrong result if not called from
    // a synchronized context.
    private synchronized boolean allFrontendsEmpty() {
        for (Queue q : frontendQueues) {
            if (!q.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    // Get a frontend element from the set of nonempty frontend queues
    private T getFrontendEntry() {
        // Get a frontend queue based on a prioritization scheme
        while (true) {
            T selected = drr.schedule();
            if (selected != null) {
                return selected;
            }
        }
//        Queue<T> q = selectFrontend();
//        return q.remove();
    }

    /**
     * Save the state of the frontier to disk
     */
    public void save() throws IOException {
        // Save frontend queues
        for (BufferedQueue q : frontendQueues) {
            q.save();
        }

        // Save backend queues
        for (BufferedQueue q : backendQueues) {
            q.save();
        }
    }

    /**
     * Clean the frontend queues, removing garbage URLs by passing them through the prioritizer again.
     */
    public void cleanQueues(CleaningCallback<T> callback) throws IOException {

        // Clean every frontend queue by dequeueing each element, running it through the prioritizer, and dropping bad domains
        for (int i = 0; i < frontendQueues.size(); i++) {
            BufferedQueue<T> q = frontendQueues.get(i);
            T entry = null;
            int novelElts = q.size();
            int seenElts = 0;
            // Don't pull from queue forever. Stop when you've seen everything once
            while (seenElts < novelElts && (entry = q.poll()) != null) {

                // Use the generic callback to alter the entry as we please
                seenElts++;
                T cleaned = callback.clean(entry);
                if (cleaned == null) {
                    continue;
                }

                // Have the prioritizer decide how to reenqueue the elements
                int p = prioritizer.prioritize(cleaned);

                // Things can get dropped
                if (p < 0) {
                    System.out.println("Priotizer selected against: " + entry);
                }

                // Things can get moved
                else if (p != i) {
                    System.out.println("Shuffling " + entry.toString() + " from " + i + " to " + p);
                }
            }
            // Save the queue to remove all the old, read URLs
            q.save();
        }
        // Also clean the backend queues, but don't reshuffle any elements
        for (BufferedQueue<T> q : backendQueues) {
            T entry = null;
            int novelElts = q.size();
            int seenElts = 0;
            // Don't pull from queue forever. Stop when you've seen everything once
            while (seenElts < novelElts && (entry = q.poll()) != null) {
                // Use the generic callback to alter the entry as we please
                seenElts++;
                T cleaned = callback.clean(entry);
                if (cleaned == null) {
                    continue;
                }
                q.add(cleaned);
            }
            q.save();
        }

        // Save everything again because of shuffling
        save();
    }
}
