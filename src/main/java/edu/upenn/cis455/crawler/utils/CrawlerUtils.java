package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.info.RobotsTxtInfo;
import edu.upenn.cis455.crawler.webtools.Response;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.*;

/**
 * Created by Alex on 3/1/18
 */
public class CrawlerUtils {

    private static Map<Character, Integer> asciiToHex;
    static {
        asciiToHex = new HashMap<>();
        asciiToHex.put('0', 0);
        asciiToHex.put('1', 1);
        asciiToHex.put('2', 2);
        asciiToHex.put('3', 3);
        asciiToHex.put('4', 4);
        asciiToHex.put('5', 5);
        asciiToHex.put('5', 5);
        asciiToHex.put('6', 6);
        asciiToHex.put('7', 7);
        asciiToHex.put('8', 8);
        asciiToHex.put('9', 9);
        asciiToHex.put('A', 10);
        asciiToHex.put('B', 11);
        asciiToHex.put('C', 12);
        asciiToHex.put('D', 13);
        asciiToHex.put('E', 14);
        asciiToHex.put('F', 15);
    }


    private final static DateTimeFormatter PREFERRED_DF;
    private final static DateTimeFormatter ACCEPTED_DF1;
    private final static DateTimeFormatter ACCEPTED_DF2;
    private final static DateTimeFormatter PREFERRED_DF_2;

    static {
        ACCEPTED_DF1 = DateTimeFormatter.ofPattern("EEEE, dd MMM yyyy HH:mm:ss z");// e.g.  or Friday, 31 Dec 1999 23:59:59 GMT
        ACCEPTED_DF2 = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss yyyy");  // e.g. Fri Dec 31 23:59:59 1999
        PREFERRED_DF = DateTimeFormatter.RFC_1123_DATE_TIME; // e.g. Fri, 31 Dec 1999 23:59:59 GMT
        PREFERRED_DF_2 = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z");
    }

    /**
     * Read a robots.txt file line by line and build the RobotsTxtInfo object
     * @param r
     * @return
     */
    public static RobotsTxtInfo buildRoboTxtInfo(Response r) {
        RobotsTxtInfo robo = new RobotsTxtInfo();
        String body = r.getBodyStr();
        ArrayList<String> agents = new ArrayList<>();

        // Split at any newline chars, read line by line
        for (String line : body.split("\\R")) {

            line = line.toLowerCase();

            // A blank line (starting a new record)
            if (line.isEmpty()) {
                agents = new ArrayList<>();
                continue;
            }

            // A comment
            if (line.trim().startsWith("#")) {
                continue;
            }

            // A user agent - Put previous user agent/arraylist in the robo
            if (line.startsWith("user-agent:")) {
                String[] split = line.split("user-agent:", 2);
                if (split.length != 2) {
                    continue;
                }
                agents.add(split[1].trim().toLowerCase());
                continue;
            }

            // An allowed link
            if (line.startsWith("allow:")) {
                String[] split = line.split("allow:", 2);
                if (split.length != 2) {
                    continue;
                }

                if (agents.size() == 0) {
                    agents.add("*");
                }
                for (String agent : agents) {
                    robo.addAllowedLink(agent, split[1].trim().toLowerCase());
                }
                continue;
            }

            // A disallowed link
            if (line.startsWith("disallow:")) {
                String[] split = line.split("disallow:", 2);
                if (split.length != 2) {
                    continue;
                }

                if (agents.size() == 0) {
                    agents.add("*");
                }
                for (String agent : agents) {
                    robo.addDisallowedLink(agent, split[1].trim().toLowerCase());
                }
                continue;
            }

            // The crawl delay
            if (line.startsWith("crawl-delay:")) {
                String[] split = line.split("crawl-delay:", 2);
                if (split.length != 2) {
                    continue;
                }

                if (agents.size() == 0) {
                    agents.add("*");
                }
                int delay = Config.COURTESY_DELAY;  // Just in case something goes awry parsing the number
                try {
                    delay = Integer.parseInt(split[1].trim());
                } catch (NumberFormatException ignored) {}
                for (String agent : agents) {
                    robo.addCrawlDelay(agent, delay);
                }
                continue;
            }

        }
        return robo;
    }


    /**
     * Parse a client timestamp (like the ones sent in If-Modified-Since headers) to a long value
     * @param stamp
     * @return the long millisecond of time in current epoch, or -1 if unparseable
     */
    public static long parseTimestamp(String stamp) {
        if (stamp == null) {
            return -1;
        }
        // HTTP Made Really Easy specifies three formats servers should accept, so try all three
        DateTimeFormatter[] dfs = {PREFERRED_DF, PREFERRED_DF_2, ACCEPTED_DF1, ACCEPTED_DF2};
        TemporalAccessor time = null;
        for (DateTimeFormatter df : dfs) {
            try {
                time = df.parse(stamp);
                return Instant.from(time).toEpochMilli();
            } catch (DateTimeException | ArithmeticException ignored) {
            }
        }
        return -1;
    }

    /**
     * @param defaultUrl a URL that contains default values that will be used for filling in missing fields
     * @param dirtyUrl a String that presumably contains a URL
     * @return a clean URL, that has
     * @throws URISyntaxException
     * @throws MalformedURLException
     */
    // TODO: if you could figure out a way to swallow URISyntaxException, that might be good, since
    // MalformedURLException extends IOException, so we don't have to do anything special for it
    public static URL buildURL(URL defaultUrl, String dirtyUrl) throws URISyntaxException, MalformedURLException {

        if (defaultUrl == null || dirtyUrl == null) {
            throw new MalformedURLException("Invalid args: " + defaultUrl + " " + dirtyUrl);
        }

        URI uri = new URI(dirtyUrl);
        uri = uri.normalize();

        // Ignore any links to pages that are using some non HTTP/HTTPS scheme
        String scheme = (uri.getScheme() == null) ? defaultUrl.getProtocol() : uri.getScheme();
        if (!scheme.contains("http")) {
            throw new MalformedURLException("URL does not belong use HTTP/S");
        }

        // If the host is unspecified, the link must be local
        String host = (uri.getHost() == null) ? defaultUrl.getHost() : uri.getHost();

        int port = uri.getPort();
        if (port < 0) {
//            // Use same port of the current request if the link is local and they're using the same protocol
//            if (host.equals(defaultUrl.getHost()) && scheme.equals(defaultUrl.getProtocol()) && isRedirect) {
//                port = defaultUrl.getPort();
//            }
            // Otherwise, default to either 80 or 443
            if (port < 0) {
                if (scheme.matches("https")) {
                    port = 443;
                } else {
                    port = 80;
                }
            }
        }

        // We have no choice but to use whatever path/query we parsed from the link
        String path = uri.getPath();
        String query = uri.getQuery();

        // If URL is absolute, we do nothing to change the path
        if (!uri.isAbsolute()) {
            // URL is path-relative if it does not lead with a slash. Otherwise, it's root relative
            if (path != null && !path.startsWith("/")) {
                // A subcase: if the "parent" is a file, then it's actually a sibling, so trim everything after the last slash
                String parent = (defaultUrl.getPath().equals("")) ? "/" : defaultUrl.getPath();
                parent = parent.substring(0, parent.lastIndexOf('/') + 1);
                path = Paths.get(parent, path).normalize().toString();
            }
        }

        // Ignore user info and fragment when building the clean link
        return new URI(scheme, null, host, port, path, query, null).toURL();
    }

    /**
     * Return the unique key corrresponding to an actual URL object. Used for detecting duplicate URLs, looking up
     * resources in Berkeley
     */
    public static String generateUrlKey(URL url) {
        String host = url.getHost();
        String path = url.getPath();
        String proto = url.getProtocol() + "://";

        // To be consistent, remove trailing slashes
        path = (path.endsWith("/")) ? path.substring(0, path.length() - 1) : path;

        String query = (url.getQuery() != null) ? url.getQuery() : "";

        return proto + host + path + query;
    }

    public static int asciiToHex(char c) {
        Integer i = asciiToHex.get(Character.toUpperCase(c));
        if (i == null) {
            return -1;
        }
        return i;
    }

    public static List<URL> readSeedsFile(String seedsTxt) throws IOException, URISyntaxException {

        // Read one URL per line
        List<String> lines = readStringsFile(seedsTxt);

        // Convert strings to URLs, which must be well formed
        List<URL> seeds = new LinkedList<>();
        for (String line : lines) {
            seeds.add(new URI(line).toURL());
        }
        return seeds;
    }

    // Reader for the basic, one entry per line, strings files we have (e.g. seeds.txt, priorities.txt)
    public static List<String> readStringsFile(String file) throws IOException {
        BufferedReader r = new BufferedReader(new FileReader(file));

        // Read one item per line
        List<String> lines = new LinkedList<>();
        String line;
        while ((line = r.readLine()) != null) {
            // Comment syntax allowed to keep life manageable
            if (line.trim().startsWith("#") || line.trim().equals("")) {
                continue;
            }
            lines.add(line);
        }
        r.close();
        return lines;
    }
}
