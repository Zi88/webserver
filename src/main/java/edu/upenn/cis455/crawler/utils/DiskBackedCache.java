package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.storage.BerkeleyManager;
import edu.upenn.cis455.storage.CacheEntity;

import java.util.ArrayList;
import java.util.Random;

// TODO: to make this better, you could count accesses, evict based on fewest number
/**
 * A Cache that writes to disk when it evicts entries.  This means entries in the cache are always retrievable unless
 * explicitly removed.
 */
public class DiskBackedCache<K, V> extends Cache<K, V> {

    public DiskBackedCache(int size) {
        super(size);
    }

    /**
     * Put a new entry into the cache.  Will evict some entries to make room if the cache is full.
     */
    @Override
    public void put(K key, V val) {
        if (map.size() >= size) {
            batchEvict();
        }
        map.put(key, val);
    }


    /**
     * Get an entry from the cache, or null if no entry exists at that key.  Searches the in memory map as well as
     * disk
     */
    @Override
    public V get(K key) {
        // Check the in-memory map for the object first
        V inMemory = map.get(key);
        if (inMemory != null) {
            return inMemory;
        }

        // Check the cache now
        CacheEntity ce = BerkeleyManager.getCacheEntity(key.toString());
        if (ce != null) {
            return (V) ce.getValue();
        }
        return null;
    }

    /**
     * Write some number of keys to disk
     */
    private synchronized void batchEvict() {

        // Check if executing thread spuriously entered this function after an earlier thread
        if (map.size() < size) {
            return;
        }

        // Randomly evict half the entries in the cache
        Random r = new Random();
        int toEvict = (map.size() / 2) + 1;
        ArrayList<K> keysArr = new ArrayList<K>(map.keySet());
        for (int i = 0; i < toEvict; i++) {
            // Only remove the entry from the in-memory map once it's been added to the DB.  Cache should remain consistent unless DB transaction fails
            int j = r.nextInt(keysArr.size());
            K evictedKey = keysArr.remove(j);
            V evictedVal = map.get(evictedKey);
            BerkeleyManager.putCacheEntity(evictedKey.toString(), evictedVal);
            map.remove(evictedKey);
        }
    }
}
