package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.crawler.Config;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A class that wraps a map and evicts some entries once it grows to some size.  Used to prevent the map from
 * consuming all of main memory
 */
public class Cache<K, V> {
    // Have to use a concurrent hashmap because iterating keys can lead to exceptions when multiple threads access/modify
    protected int size;
    protected ConcurrentHashMap<K, V> map;

    public Cache() {
        this(Config.DEFAULT_CACHE_SIZE);
    }

    public Cache(int size) {
        this.size = size;
        map = new ConcurrentHashMap<K, V>();
    }

    public void put(K key, V val) {
        if (map.size() >= size) {
            batchEvict();
        }
        // Put the new key
        map.put(key, val);
    }

    public V get(K key) {
        return map.get(key);
    }

    private synchronized void batchEvict() {
        // Check if executing thread entered this function spuriously
        if (map.size() < size) {
            return;
        }

        // Randomly evict some multiple of the number of excess keys in the map
        Random r = new Random();
        ArrayList<K> keysArr = new ArrayList<K>(map.keySet());
        int numToEvict = (map.size() - size + 1) * 5;  // Multiple of 5
        for (int i = 0; i < numToEvict; i++) {
            K toEvict = keysArr.remove(r.nextInt(keysArr.size()));
            map.remove(toEvict);
        }
    }
}
