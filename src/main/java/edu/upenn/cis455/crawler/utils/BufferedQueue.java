package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * Created by Alex on 4/14/18
 */
public class BufferedQueue<T> implements Queue<T> {

    private static final Logger LOG = LogManager.getLogger(BufferedQueue.class);
    private File file;
    private SerializationProvider<T> provider;
    private int size = 0;
    private int bufsize = 8192;
    private BufferedWriter writer;
    private BufferedReader reader;

    public BufferedQueue(File file, SerializationProvider<T> provider, int bufsize) {
        this.file = file;
        this.provider = provider;
        if (bufsize > 0) {
            this.bufsize = bufsize;
        }
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                if (file.createNewFile()) {
                    LOG.info("Initializing queue file: " + file);
                }
            }
            reader = new BufferedReader(new FileReader(file), this.bufsize);
            writer = new BufferedWriter(new FileWriter(file, true), this.bufsize);  // True to append
            size = countAvailableLines();
        } catch (IOException e) {
            LOG.fatal("Initialization failed for file " + file, e);
            System.exit(-1);
        }
    }

    // Count the lines available in the underlying file.  Called once on initialization to get an accurate size field
    private int countAvailableLines() throws IOException {
        // Only meant to be called on initialization, but flush all the same
        writer.flush();
        BufferedReader tmpReader = new BufferedReader(new FileReader(file));
        int count = 0;
        while (tmpReader.readLine() != null) {
            count++;
        }
        return count;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Enqueue an item by appending it to the underlying file
     * @param t Item to enqueue
     * @return True on success, false on failure
     */
    @Override
    public synchronized boolean add(T t) {
        // Write one queue item per line, boxed up as a String
        try {
            String toWrite = provider.writeObject(t) + "\n";
            writer.write(toWrite);
            size++;
            return true;
        } catch (IOException e) {
            LOG.error(e);
            return false;
        }
    }

    @Override
    public synchronized T remove() {
        String line;
        if (size == 0 || (line = guardedReadLine()) == null) {
            throw  new NoSuchElementException();
        }
        size--;
        return provider.readObject(line);
    }

    private String guardedReadLine() {
        try {
            String line = reader.readLine();
            // If reader got nothing, we are forced to flush the writer to see if there's something available
            if (line == null) {
                writer.flush();
                line = reader.readLine();
            }
            return line;
        } catch (IOException e) {
            LOG.error(e);
            return null;
        }
    }

    @Override
    public synchronized T poll() {
        String line;
        if ((size == 0) || (line = guardedReadLine()) == null) {
            return null;
        }
        size--;
        return provider.readObject(line);
    }

    /**
     * Save the state of the queue, by writing to a temporary file, then replacing the working file with that
     */
    public synchronized void save() throws IOException {

        writer.flush();

        // Create a temporary file as a sibling to the original file
        String tmpName = file.getName() + "-tmp";
        File tmp = file.toPath().resolveSibling(tmpName).toFile();
        FileWriter tmpWriter = new FileWriter(tmp, false);  // False to overwrite, not append
        if (tmp.createNewFile()) {
            LOG.info("Creating file to save queue: " + tmp.toPath());
        }

        // Read every line from current position, write it to the new file.
        // Need to do this to eliminate old, already consumed lines from the original file
        String line;
        while ((line = guardedReadLine()) != null) {
            tmpWriter.write(line + "\n");
        }
        writer.close();
        tmpWriter.close();

        // Replace the original file with the tmp file
        // This is platform dependent, so use with caution
        Path movedPath = Files.move(tmp.toPath(), file.toPath(), StandardCopyOption.ATOMIC_MOVE);

        // Replace the file, writer, and reader with clean objects, so we can keep using this queue if desired
        file = movedPath.toFile();
        writer = new BufferedWriter(new FileWriter(file, true), bufsize);  // True to append
        reader = new BufferedReader(new FileReader(file), bufsize);

        // Delete tmp file
        tmp.delete();

    }

    /**
     * Flush the underlying Writer without removing duplicates from the existing file.
     * Less resource intensive than saving the frontier, but sufficient for write only queues.
     */
    public void flush() throws IOException {
        writer.flush();
    }

    // Unsupported Queue operations

    // Optional operation, not supported
    @Deprecated
    @Override
    public boolean remove(Object o) {
        return false;
    }

    // Not supported
    @Deprecated
    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    // Not supported
    @Deprecated
    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    // Not supported
    @Deprecated
    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    // Not supported
    @Deprecated
    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    // Not supported
    @Deprecated
    @Override
    public void clear() {}

    // Not supported
    @Deprecated
    @Override
    public boolean equals(Object o) {
        return false;
    }

    // Not supported
    @Deprecated
    @Override
    public int hashCode() {
        return 0;
    }

    // Not supported
    @Deprecated
    @Override
    public boolean offer(T t) {
        return false;
    }

    /**
     * Not an exact implementation of interface.
     * Actually removes the head of the queue and places it at the end of the queue, but who cares?
     */
    @Override
    public T element() {
        T elt = remove();
        add(elt);
        return elt;
    }

    // Not supported
    @Deprecated
    @Override
    public T peek() {
        return null;
    }

    // Unsupported Collection operations

    // Not supported
    @Deprecated
    @Override
    public boolean contains(Object o) {
        return false;
    }

    // Not supported
    @Deprecated
    @Override
    public Iterator<T> iterator() {
        return null;
    }

    // Not supported
    @Deprecated
    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    // Not supported
    @Deprecated
    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

}
