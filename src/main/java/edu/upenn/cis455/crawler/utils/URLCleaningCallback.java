package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.crawler.interfaces.CleaningCallback;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Alex on 4/30/18
 */
public class URLCleaningCallback implements CleaningCallback<URL> {

    @Override
    public URL clean(URL in) {
        // Convert wikipedia entries from http to https (we always get redirected)
        if (in.getProtocol().equals("http") && in.getHost().contains("wikipedia")) {
            try {
                URL asHttps = new URL("https", in.getHost(), 443, in.getFile());
                System.out.println("Cleaned: " + in + " to " + asHttps);
                return asHttps;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }
        }
        return in;
    }
}
