package edu.upenn.cis455.crawler.utils;

import edu.upenn.cis455.crawler.Bookkeeper;

import java.util.*;

/**
 * Created by Alex on 3/1/18
 */
public class Frontier<T> {


    private final Queue<T> list;
    private final static int MAX_LEN = 50;
    private Bookkeeper bookkeeper;
    private Set<Long> waitingThreads; // The set of workers that are waiting to dequeue something
    private int numAccessors;         // The number of worker threads that are accepted to dequeue, used to know when queue will remain empty

    public Frontier(Bookkeeper bookkeeper, int numAccessors) {
        list = new LinkedList<T>();
        waitingThreads = new HashSet<>();
        this.bookkeeper = bookkeeper;
        this.numAccessors = numAccessors;
    }

    public T dequeue() throws InterruptedException {
        synchronized (list) {
            while (list.isEmpty()) {
                // Check if all the workers are waiting to dequeue something.  If so, we've run out of links.
                waitingThreads.add(Thread.currentThread().getId());
                if (waitingThreads.size() == numAccessors) {
                    // The worker who notices interrupts all the other workers, then itself
                    throw new InterruptedException();
                }
                list.wait();
            }
            waitingThreads.remove(Thread.currentThread().getId());
            T removed = list.remove();
            list.notifyAll();
            return removed;
        }
    }

    public void enqueue(T object) throws InterruptedException {
        synchronized (list) {
            while (list.size() >= MAX_LEN) {
                list.wait();
            }
            list.add(object);
            list.notifyAll();
        }
    }

    public void enqueueAll(Collection<T> objects) throws InterruptedException {
        synchronized (list) {
            while (list.size() >= MAX_LEN - objects.size()) {
                list.wait();
            }
            list.addAll(objects);
            list.notifyAll();
        }
    }
}
