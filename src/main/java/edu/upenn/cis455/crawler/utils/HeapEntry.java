package edu.upenn.cis455.crawler.utils;

import java.io.Serializable;
import java.util.Queue;

/**
 * Created by Alex on 3/8/18
 */
public class HeapEntry<T> {
    public BufferedQueue<T> queueHandle;
    public long timestamp;
    public String hostname;
}

