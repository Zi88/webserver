package edu.upenn.cis455.crawler.interfaces;

import java.util.List;
import java.util.Queue;

/**
 * Created by Alex on 3/28/18
 */
public interface Prioritizer<T> {

    void initialize(List<? extends Queue<T>> queues);

    int prioritize(T item);
}
