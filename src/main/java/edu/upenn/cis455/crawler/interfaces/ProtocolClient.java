package edu.upenn.cis455.crawler.interfaces;

import edu.upenn.cis455.crawler.webtools.Response;
import edu.upenn.cis455.crawler.webtools.WebException;

import java.io.IOException;
import java.net.URL;

/**
 * An interface that wraps up HTTP/S, FTP, Gopher, requests etc.
 */
public interface ProtocolClient {

    /**
     * @param url url for the resource we wish to get (i.e. a GET request)
     * @return a Response object
     */
    public Response getResource(URL url) throws IOException, WebException;

    /**
     * Prefetch a resource to get a response with metadata (i.e. a HEAD request)
     * @param url url to request
     * @return a Response object
     */
    public Response prefetch(URL url) throws IOException, WebException;
}
