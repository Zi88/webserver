package edu.upenn.cis455.crawler.interfaces;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alex on 4/15/18
 */
public interface SerializationProvider<T> {
    /**
     * Write an object to a useable String
     * @param obj an object of type T
     */
    String writeObject(T obj);

    /**
     * Read an object from the underlying file
     * @return an object of type T, or null if impossible to deserialize
     */
    T readObject(String serialized);

    String generateKey(T obj);

    String[] tokenize(T obj);
}
