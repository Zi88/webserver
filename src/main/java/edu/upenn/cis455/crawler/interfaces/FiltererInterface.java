package edu.upenn.cis455.crawler.interfaces;

import edu.upenn.cis455.crawler.webtools.Response;
import edu.upenn.cis455.storage.ResourceEntity;

import java.net.URL;

/**
 * Created by Alex on 3/2/18
 */
public interface FiltererInterface {

    /**
     * @param url the URL whose content we wish to fetch
     * @param prefetchResp a webserver response containing metadata (e.g. from a HEAD request)
     * @return true if we should not download this resource because (i.e. wrong content type, not modified, etc.)
     */
    public boolean filterOut(URL url, Response prefetchResp);

    ResourceEntity getUnmodifiedResource(URL url, Response prefetchResp);

    /**
     * Filter out a URL based on its attributes alone, without making a HEAD request for metadata
     * @param url A newly cleaned URL
     * @return true if we should not enqueue or download this resourcee (i.e. crappy TLD, too deep, etc.)
     */
    public boolean prefilter(URL url);
}
