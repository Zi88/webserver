package edu.upenn.cis455.crawler.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Alex on 3/28/18
 */
public interface QueueScheduler<T> {

    public void initialize(List<? extends Queue<T>> queues, int[] params);

    public T schedule();
}
