package edu.upenn.cis455.crawler.interfaces;

import java.net.URL;
import java.util.List;

/**
 * Created by Alex on 3/2/18
 */
public interface ProcessorInterface {

    /**
     * Postprocess a fetched resource: scraping, cleaning, and enqueuing new links.
     * etc.
     */
    public List<URL> process(URL url, String body, String contentType) throws InterruptedException;
}
