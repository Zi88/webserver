package edu.upenn.cis455.crawler.interfaces;

/**
 * Created by Alex on 4/30/18
 *
 * Callback used to generically modify Mercator frontier queue entries when cleaning
 */
public interface CleaningCallback<T> {
    T clean(T in);
}
