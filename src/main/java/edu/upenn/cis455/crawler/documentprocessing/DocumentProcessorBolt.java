package edu.upenn.cis455.crawler.documentprocessing;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.hostsplitter.RcvDaemon;
import edu.upenn.cis455.crawler.pagetypes.JacksonPage;
import edu.upenn.cis455.crawler.pagetypes.JacksonPageSerializationProvider;
import edu.upenn.cis455.crawler.pagetypes.Page;
import edu.upenn.cis455.crawler.utils.BufferedQueue;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import edu.upenn.cis455.crawler.webtools.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Alex on 3/21/18
 */
public class DocumentProcessorBolt implements IRichBolt {

    private static final Logger LOG = LogManager.getLogger(DocumentProcessorBolt.class);
    private Processor processor = new Processor();  // A simple, stateless document parser
    private Fields fields = new Fields("Host", "URL");      // The names of the objects that this bolt emits
    private String executorId = "ProcessorBolt-" + (UUID.randomUUID().toString());  // An ID for debugging
    private BufferedQueue<Page> pageStore;  // An append only queue (just a wrapper around a file)
    private OutputCollector collector;

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }

    @Override
    public void cleanup() {
        try {
            pageStore.flush();
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    @Override
    public void execute(Tuple input) {
        // Get the URL info and Response objects from the tuple
        Response resp = (Response) input.getObjectByField("Response");

        // If the response is a redirect, parse out the URL and emit that
        if (resp.isRedirect()) {
            try {
                URL location = CrawlerUtils.buildURL(resp.getUrl(), resp.getHeader("Location"));
                collector.emit(new Values<>(location.getHost(), location));
                return;
            } catch (MalformedURLException | URISyntaxException e) {
                LOG.error("Error building redirect URL", e);
            }
        }

        // Process the response body to scrape URLs
        List<URL> scrapedUrls = processor.process(resp.getUrl(), resp.getBodyStr(), resp.getHeader("Content-Type"));

        // Emit every scraped url separately
        for (URL scrapedUrl : scrapedUrls) {
            collector.emit(new Values<>(scrapedUrl.getHost(), scrapedUrl));
        }

        // With the scraped URLs and Response object, build a Page object, and append it to this processor's persistent file
        JacksonPage page = buildPage(resp, scrapedUrls);
        pageStore.add(page);
    }

    @Override
    public void prepare(Map<String, Object> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;

        // Page objects get stored as JSON (one object per line) in a file in the persistent pages directory
        File f = Paths.get(Config.PAGE_DIR, executorId + "-pages.txt").toFile();
        pageStore = new BufferedQueue<>(f, new JacksonPageSerializationProvider(), Config.PAGES_FILE_BUFFER_SIZE);

        // Give the RcvDaemon a handle to the collector, so that it can pass sharded URLs directly to the HostSplitterBolt
        RcvDaemon.init(collector);
    }

    @Override
    public void setRouter(IStreamRouter router) {
        this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return fields;
    }

    /**
     * @param resp A given Response object
     * @param scrapedUrls A list of URLs scraped off the body of the response object
     * @return a Page object that marshals together the Response object's fields into a nicely serializable form
     */
    private JacksonPage buildPage(Response resp, List<URL> scrapedUrls) {
        JacksonPage p = new JacksonPage();

        p.setURL(resp.getUrl().toString());
        p.setTimestamp(resp.getTimestamp());
        p.setStatusCode(resp.getStatusCode());
        p.setFingerprint(null);  // TODO, for now, no fingerprint
        p.setHeaders(resp.getHeaders());
        p.setContentLength(resp.getLongHeader("Content-Length"));
        p.setBody(resp.getBody());

        // Make the scraped urls into strings, so Jackson doesn't do anything weird with them
        ArrayList<String> urlStrs = new ArrayList<>();
        for (URL url : scrapedUrls) {
            urlStrs.add(url.toString());
        }
        p.setLinks(urlStrs);

        return p;
    }

}
