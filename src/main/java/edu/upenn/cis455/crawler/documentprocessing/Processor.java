package edu.upenn.cis455.crawler.documentprocessing;

import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.interfaces.ProcessorInterface;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alex on 3/4/18
 */
public class Processor implements ProcessorInterface {


    public Processor() {}

    /**
     * Postprocess a fetched resource, placing its content in the database, scraping, cleaning, and enqueuing new links,
     * etc.
     *  @param url              the URL for the just downloaded resource
     * @param body             a String the content of the downloaded page
     * @param contentType      the MIME type of the body
     */
    @Override
    public List<URL> process(URL url, String body, String contentType) {

        // If the document downloaded is HTML, scrape it for hrefs of interest and return these
        if (contentType != null && contentType.toLowerCase().contains("html")) {

            // Scrape and clean the links, then enqueue them all
            List<String> hrefs = scrapeLinks(body);
            List<URL> urls = cleanLinks(url, hrefs);
            return urls;
        }
        // Otherwise, return an empty collection
        return Collections.emptyList();
    }

    /*
     * @param url the url of the just-scraped document
     * @param hrefs array of hrefs scraped from a document
     * @return a list of clean, filtered URLs that could be enqueued in the Frontier
     */
    private List<URL> cleanLinks(URL url, List<String> hrefs) {
        List<URL> cleanUrls = new LinkedList<>();
        for (String href : hrefs) {

            // Don't accept links past a certain length
            if (href.length() > Config.HREF_LEN_LIMIT) {
                continue;
            }

            // Try to build a URI from the href, and if successful, clean it and make it absolute
            try {

                URL cleaned = CrawlerUtils.buildURL(url, href);
                cleanUrls.add(cleaned);

            } catch (URISyntaxException | MalformedURLException ignored) {
                // Do nothing
            }
        }
        return cleanUrls;
    }

    /**
     * @param body the body of a response (XML or HTML), as a String
     * @return a list of hrefs found in the document
     */
    private List<String> scrapeLinks(String body) {
        Document doc = Jsoup.parse(body);
        Elements links = doc.select("a[href]");
        List<String> hrefs = new LinkedList<>();
        for (Element e : links) {
            String href = e.attr("href");
            if (!href.equals("")) {
                hrefs.add(href);
            }
        }
        return hrefs;
    }
}
