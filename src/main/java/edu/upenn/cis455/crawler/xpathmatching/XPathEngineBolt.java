package edu.upenn.cis455.crawler.xpathmatching;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.crawler.webtools.Response;
import edu.upenn.cis455.storage.ChannelEntity;
import edu.upenn.cis455.storage.DBWrapper;
import edu.upenn.cis455.xpathengine.XPathEngine;
import edu.upenn.cis455.xpathengine.XPathEngineFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.UUID;

/**
 * Created by Alex on 3/26/18
 */
public class XPathEngineBolt implements IRichBolt {

    private static final Logger LOG = LogManager.getLogger(XPathEngineBolt.class);
    private String executorId = "XPathEngineBolt-" + UUID.randomUUID().toString();
    private OutputCollector collector;
    private Map<String, ChannelEntity> channelByXPath;
    private String[] xpaths;
    private XPathEngine engine;
    private Fields fields = new Fields("Response");  // Bolt emits a Response object

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }

    @Override
    public void cleanup() {

    }

    @Override
    public void execute(Tuple input) {
        // This bolt receives a response object and immediately emits it (Does XPath matching off the critical path)
        Response response = (Response) input.getObjectByField("Response");
        collector.emit(new Values<>(response));

        // Don't attempt to match XPaths on something that has no Content-Type header or has no body
        String ctype = response.getHeader("Content-Type");
        if (ctype == null || response.getBodyStr() == null) {
            return;
        }

        Document d = null;

        try {
            if (ctype.toLowerCase().contains("html")) {
                d = buildHTMLDoc(response.getBodyStr());
            } else if (ctype.toLowerCase().contains("xml")) {
                d = buildXMLDoc(response.getBodyStr());
            }
        } catch (SAXException | ParserConfigurationException | IOException e) {
            LOG.error(response.getUrl() + " : Failed to parse document for XPath matching", e);
            return;
        }


        // Invoke the engine
        boolean[] matches = engine.evaluate(d);
        if (matches == null) {
            return;
        }

        // If an XPath was matched on this Doc, record the URL of the Doc in the XPath's corresponding ChannelEntity
        for (int i = 0; i < matches.length; i++) {
            if (matches[i]) {
                String xpath = xpaths[i];
                LOG.info(response.getUrl().toString() + " : Matched XPath " + xpath);
                ChannelEntity ce = channelByXPath.get(xpath);
                ce.getUrls().add(response.getUrl().toString());
                DBWrapper.putChannel(ce);
            }
        }

    }

    // For XML, use javax.xml.parsers library to parse
    private Document buildXMLDoc(String body) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new ByteArrayInputStream(body.getBytes()));
    }

    // For HTML, use JSoup to parse, then convert to W3C
    private Document buildHTMLDoc(String body) {
        org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(body);
        return (new W3CDom()).fromJsoup(jsoupDoc);
    }

    @Override
    public void prepare(Map<String, Object> stormConf, TopologyContext context, OutputCollector collector) {
        // Set fields
        this.collector = collector;
        this.channelByXPath = new HashMap<>();
        engine = XPathEngineFactory.getXPathEngine();

        // Look up all channels, record their xpaths, and remove all the docs crawled for each channel
        SortedMap<String, ChannelEntity> map = DBWrapper.getAllChannels();
        xpaths = new String[map.size()];
        int i = 0;
        for (ChannelEntity ce : map.values()) {
            ce.getUrls().clear();
            DBWrapper.putChannel(ce);
            channelByXPath.put(ce.getXpath(), ce);
            xpaths[i] = ce.getXpath();
            i++;
        }

        // Set the XPaths for the engine to evaluate
        engine.setXPaths(xpaths);
    }

    @Override
    public void setRouter(IStreamRouter router) {
        collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return fields;
    }
}
