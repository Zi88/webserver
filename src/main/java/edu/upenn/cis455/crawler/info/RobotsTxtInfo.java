package edu.upenn.cis455.crawler.info;

import java.util.ArrayList;
import java.util.HashMap;

public class RobotsTxtInfo {
	
	private HashMap<String,ArrayList<String>> disallowedLinks;
	private HashMap<String,ArrayList<String>> allowedLinks;
	
	private HashMap<String,Integer> crawlDelays;
	private ArrayList<String> sitemapLinks;
	private ArrayList<String> userAgents;
	
	public RobotsTxtInfo(){
		disallowedLinks = new HashMap<String,ArrayList<String>>();
		allowedLinks = new HashMap<String,ArrayList<String>>();
		crawlDelays = new HashMap<String,Integer>();
		sitemapLinks = new ArrayList<String>();
		userAgents = new ArrayList<String>();
	}
	
	public void addDisallowedLink(String ua, String d){
        // Clean up the disallowed path a little
        d = (d.startsWith("/")) ? d : "/" + d;
        d = (d.endsWith("/")) ? d.substring(0, d.length() - 1) : d;

        if (!containsUserAgent(ua)) {
            addUserAgent(ua);
        }
		if(!disallowedLinks.containsKey(ua)){
			ArrayList<String> temp = new ArrayList<String>();
			temp.add(d);
			disallowedLinks.put(ua, temp);
		}
		else{
			ArrayList<String> temp = disallowedLinks.get(ua);
			if(temp == null)
				temp = new ArrayList<String>();
			temp.add(d);
			disallowedLinks.put(ua, temp);
		}
	}
	
	public void addAllowedLink(String key, String value){
        if (!containsUserAgent(key)) {
            addUserAgent(key);
        }
		if(!allowedLinks.containsKey(key)){
			ArrayList<String> temp = new ArrayList<String>();
			temp.add(value);
			allowedLinks.put(key, temp);
		}
		else{
			ArrayList<String> temp = allowedLinks.get(key);
			if(temp == null)
				temp = new ArrayList<String>();
			temp.add(value);
			allowedLinks.put(key, temp);
		}
	}
	
	public void addCrawlDelay(String key, Integer value){
		crawlDelays.put(key, value);
	}
	
	public void addSitemapLink(String val){
		sitemapLinks.add(val);
	}
	
	public void addUserAgent(String key){
		userAgents.add(key);
	}
	
	public boolean containsUserAgent(String key){
		return userAgents.contains(key);
	}
	
	private ArrayList<String> getDisallowedLinks(String key){
		return disallowedLinks.get(key);
	}
	
	public ArrayList<String> getAllowedLinks(String key){
		return allowedLinks.get(key);
	}

    /**
     * @return the crawl delay (in seconds) for the specified user agent (or the wildcard if the passed user agent has
	 * no directives)
     */
	public Integer getCrawlDelay(String key){
		if (containsUserAgent(key.toLowerCase())) {
			return crawlDelays.get(key.toLowerCase());
		}
		return crawlDelays.get("*");
	}
	
	public void print(){
		for(String userAgent:userAgents){
			System.out.println("User-Agent: "+userAgent);
			ArrayList<String> dlinks = disallowedLinks.get(userAgent);
			if(dlinks != null)
				for(String dl:dlinks)
					System.out.println("Disallow: "+dl);
			ArrayList<String> alinks = allowedLinks.get(userAgent);
			if(alinks != null)
					for(String al:alinks)
						System.out.println("Allow: "+al);
			if(crawlDelays.containsKey(userAgent))
				System.out.println("Crawl-Delay: "+crawlDelays.get(userAgent));
			System.out.println();
		}
		if(sitemapLinks.size() > 0){
			System.out.println("# SiteMap Links");
			for(String sitemap:sitemapLinks)
				System.out.println(sitemap);
		}
	}
	
	public boolean crawlContainAgent(String key){
		return crawlDelays.containsKey(key);
	}

	/**
	 * Return true if the given path is disallowed by the robots.txt (for the given user agent)
	 */
	public boolean isDisallowed(String path, String userAgent) {

		path = (path.startsWith("/")) ? path : "/" + path;
		path = (path.endsWith("/")) ? path.substring(0, path.length() - 1) : path;
		ArrayList<String> disallowed = null;

		// If there are directives for this user agent, use them
		if (containsUserAgent(userAgent.toLowerCase())) {
			disallowed = getDisallowedLinks(userAgent.toLowerCase());
		}

		// Otherwise use the directives for "*"
		else if (containsUserAgent("*")){
			disallowed = getDisallowedLinks("*");
		}
		// Crawler is free to crawl anything
		if (disallowed == null) {
			return false;
		}

		// Don't need to support wildcards in path, so just compare char by char
		for (String d : disallowed) {
            if (d.length() > path.length()) {
                continue;
            }
            if (d.equals(path.substring(0, d.length()))) {
                return true;
            }
		}

		// If no matches are found, path is allowed
		return false;
    }
}
