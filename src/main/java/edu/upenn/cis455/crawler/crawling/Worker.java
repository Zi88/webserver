package edu.upenn.cis455.crawler.crawling;

import edu.upenn.cis455.crawler.Bookkeeper;
import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.frontier.WorkItem;
import edu.upenn.cis455.crawler.info.RobotsTxtInfo;
import edu.upenn.cis455.crawler.interfaces.FiltererInterface;
import edu.upenn.cis455.crawler.interfaces.ProtocolClient;
import edu.upenn.cis455.crawler.utils.Cache;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import edu.upenn.cis455.crawler.webtools.HTTPClient;
import edu.upenn.cis455.crawler.webtools.Response;
import edu.upenn.cis455.crawler.webtools.WebException;
import edu.upenn.cis455.storage.BerkeleyManager;
import edu.upenn.cis455.storage.ResourceEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.*;

/**
 * Created by Alex on 3/1/18
 */
public class Worker {

    private static final Logger LOG = LogManager.getLogger(Worker.class);

    private Cache<String, RobotsTxtInfo> robotsCache;                   // hostname -> robots.txt
    private FiltererInterface filter;
    private Bookkeeper bookkeeper;
    private MercatorFrontier<URL> urlFrontier;
    private DatagramSocket dgramSock;
    private InetAddress monitoringHost;

    public Worker(MercatorFrontier<URL> urlFrontier, Bookkeeper bookkeeper) throws SocketException, UnknownHostException {
        this.urlFrontier = urlFrontier;
        this.bookkeeper = bookkeeper;
        this.robotsCache = new Cache<>();
        this.filter = new Filterer();
        dgramSock = new DatagramSocket();
        monitoringHost = InetAddress.getByName(Config.MONITORING_HOSTNAME);
    }

    public Response waitThenCrawl(WorkItem<URL> entry) {

        URL url = entry.item;
        RobotsTxtInfo robo = null;
        try {

            // Do the waiting and timestamp update together I thing (with the robo)
            long now = System.currentTimeMillis();
            if (entry.timestamp > now) {
                Thread.sleep(entry.timestamp - now);
            }

            // Find the robots.txt for this host, and check if the given path is allowed. Continue if not
            robo = getRobotsTxt(url);
            if (robo != null && robo.isDisallowed(url.getPath(), Config.CRAWLER_NAME)) {
                long timestamp = updateTimestamp(robo);
                urlFrontier.reinsert(url.getHost(), timestamp);
                return null;
            }

            // If we've gotten this far, request the resource
            Response r = crawl(url);
            if (r != null) {
                r.setUrl(url);
            }

            // Update the timestamp here, so that the next worker will wait until crawl delay is satisfied
            long timestamp = updateTimestamp(robo);
            // Reinsert this host back into the heap
            urlFrontier.reinsert(url.getHost(), timestamp);

            return r;

        } catch (WebException | IOException | InterruptedException e) {
            LOG.error(url, e);
            return null;
        }
        // TODO this would be nice but causes a dreadful race condition
//        } finally {
//            System.out.println(url.toString() + ": Updating timestamp");
//            // Update the timestamp here, so that the next worker will wait until crawl delay is satisfied
//            long timestamp = updateTimestamp(robo);
//            // Reinsert this host back into the heap
//            urlFrontier.reinsert(url.getHost(), timestamp);
//        }
    }

    /**
     * Get a resource, and do the postprocessing
     * @param url The URL to be crawled
     */
    private Response crawl(URL url) throws IOException, WebException {
        ProtocolClient client = new HTTPClient();
        sendMonitoringPacket(url.toString());
        Response r = client.prefetch(url);

        // Update the number of documents crawled, and stop crawling if we've already hit the page limit
        if (!bookkeeper.continueCrawling()) {
            return null;
        }

        // Check if the HEAD is a redirect.  If so, return the response as is for processing
        if (r.isRedirect()) {
            LOG.info(url.toString() + ": Redirected to " + r.getHeader("Location"));
            return r;
        }

        // Given the HEAD response, discontinue the request if it isn't the correct content type, is too long, etc.
        if (filter.filterOut(url, r)) {

            // TODO place this somewhere less ad hoc, perhaps in the filter. Also, reinsert rather than sleeping
            // Respond specifically to a 429 rate limit by slowing down
            if (r.getStatusCode() == 429) {
                urlFrontier.enqueue(url);

                // Wait for how long they've asked, which could be in either milliseconds or date format
                long timestamp = r.getDateHeader("Retry-After");
                long delay = -1;
                if (timestamp <= 0 || timestamp < System.currentTimeMillis()) {
                    delay = r.getLongHeader("Retry-After") * 1000;
                    if (delay <= 0) {
                        delay = Config.COURTESY_DELAY * 5;
                    }
                    timestamp = System.currentTimeMillis() + delay;
                }
                //urlFrontier.reinsert(url.getHost(), timestamp);
                long now = System.currentTimeMillis();
                try {
                    if (timestamp - now > 0) {
                        LOG.info(String.format("Sleeping (%d)ms to accommodate 429", (timestamp - now)));
                        Thread.sleep(timestamp - now);
                    }
                } catch (InterruptedException ignored) {}

            }
            return null;
        }

        // If not filtered out, issue a GET
        LOG.info(url.toString() + ": Downloading");
        sendMonitoringPacket(url.toString());
        r = client.getResource(url);
        // Do not put any unsuccessful requests in the database
        if (r.getStatusCode() != 200) {
            LOG.error(String.format("%s: Request received a non-200 status code (%d)", url.toString(), r.getStatusCode()));
            return null;
        }

        // Return the response object
        return r;
    }


    private RobotsTxtInfo getRobotsTxt(URL url) throws IOException, WebException {
        RobotsTxtInfo robo = robotsCache.get(url.getHost());
        if (robo == null) {
            URL roboUrl = new URL(url.getProtocol(), url.getHost(), url.getPort(), "/robots.txt");
            Response r = new HTTPClient().getResource(roboUrl);
            if (r.getStatusCode() != 200) {
                return null;
            }
            robo = CrawlerUtils.buildRoboTxtInfo(r);
        }
        if (robo != null) {
            robotsCache.put(url.getHost(), robo);
        }
        return robo;
    }


    /**
     * Update the timestamp for when it's safe to next crawl based on robots.txt
     * @param robo
     */
    private long updateTimestamp(RobotsTxtInfo robo) {
        Integer crawlDelay = Config.COURTESY_DELAY;
        if (robo != null) {
            Integer parsedDelay = robo.getCrawlDelay(Config.CRAWLER_NAME);
            crawlDelay = (parsedDelay == null) ? Config.COURTESY_DELAY : parsedDelay * 1000;  // Crawl delay is in ms
        }
        return System.currentTimeMillis() + crawlDelay;
    }

    private void sendMonitoringPacket(String url) {
        try {
            byte[] data = ("thurston;" + url).getBytes();
            DatagramPacket packet = new DatagramPacket(data, data.length, monitoringHost, 10455);
            dgramSock.send(packet);
        } catch (IOException e) {
            LOG.error("Error with monitoring packet:", e);
        }
    }
}
