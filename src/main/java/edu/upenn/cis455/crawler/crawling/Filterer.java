package edu.upenn.cis455.crawler.crawling;

import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.interfaces.FiltererInterface;
import edu.upenn.cis455.crawler.webtools.Response;
import edu.upenn.cis455.storage.BerkeleyManager;
import edu.upenn.cis455.storage.ResourceEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by Alex on 3/2/18
 */
public class Filterer implements FiltererInterface {

    private static final Logger LOG = LogManager.getLogger(Filterer.class);
    private final static String[] acceptedContentTypes = {"text/html", "text/xml", "application/xml", "+xml"};

    // Most common TLDs that we won't filter out (basically, the English-speaking world)
    private final static String[] commonAcceptedTLDs = {".com", ".org", ".edu", ".gov", ".uk", ".net", ".ca", ".au", ".us", ".mil", ".eu", ".ie"};

    // Most common TLDs that we actively select against (basically, continental Europe + Japan)
    private final static String[] commonUnnacceptedTLDs = {".de", ".jp", ".fr", ".ru", ".ch", ".it", ".nl", ".se", ".no", ".es"};

    // Hostnames that show up a lot and are low quality, or forbidden by robots.txt anyway
    private final static String[] unacceptableHosts = {"t.co", "www.tripadvisor.com"};
    private final static HashSet<String> badHostsSet = new HashSet<>();
    static {
        Collections.addAll(badHostsSet, unacceptableHosts);
    }


    /**
     * @param url          the URL whose content we wish to fetch
     * @param prefetchResp a webserver response containing metadata (e.g. from a HEAD request)
     * @return true if we should not download this resource because (i.e. wrong content type, not modified, etc.)
     */
    @Override
    public boolean filterOut(URL url, Response prefetchResp) {

        // If we didn't get a clean 200 from the web server, stop right here
        int statusCode = prefetchResp.getStatusCode();
        if (statusCode != 200) {
            LOG.info(url.toString() + String.format(": Not downloading (%d)", statusCode));
            return true;
        }

        // Check that the resource is under the maximum.  If a useful Content-Length header wasn't sent, filter out
        long contentLength = prefetchResp.getLongHeader("Content-Length");
        if (contentLength < 0) {
            // TODO for now allow this, since many pages seem to do it
            //LOG.info(url.toString() + ": (Content-Length header malformed or nonexistent), but proceeding anyway");
            //return true;
        }

        if (contentLength > Config.MAX_CONTENT_LEN_MB * 1000000) {
            LOG.info(url.toString() + String.format(": Not downloading (Content-Length (%d) exceeds max size)", contentLength));
            return true;
        }

        // Check that the resource has the correct content type. If a useful Content-Type header wasn't sent, filter out
        String contentType = prefetchResp.getHeader("Content-Type");
        if (contentType == null) {
            LOG.info(url.toString() + ": Not downloading (Content-Type header not found)");
            return true;
        }
        boolean match = false;
        for (String c : acceptedContentTypes) {
            if (contentType.contains(c)) {
                match = true;
                break;
            }
        }
        if (!match) {
            LOG.info(url.toString() + String.format(": Not downloading (Content-Type (%s) not accepted)", contentType));
            return true;
        }

        // Otherwise, don't filter out
        return false;
    }

    /**
     * @param url the URL for the underlying resource
     * @param prefetchResp a Response (e.g. from a HEAD request)
     * @return The ResourceEntity in the database corresponding to the given URL, if the resource has not been modified
     * since the time it was last downloaded.  If the resource has been modified since then or it is not possible to
     * tell, this method returns null
     */
    @Override
    public ResourceEntity getUnmodifiedResource(URL url, Response prefetchResp) {
        // Check if this resource lives in the database yet. If not, don't filter it out
        ResourceEntity inDB = BerkeleyManager.getResource(url);

        if (inDB == null) {
            return null;
        }

        // If there is a preexisting resource in DB, check the timestamp against standard HTTP headers. Filter out if unmodified
        long lastFetched = inDB.getTimestamp();
        long lastModified = prefetchResp.getDateHeader("Last-Modified");
        if (lastModified > 0 && lastFetched > lastModified) {
            LOG.info(url.toString() + ": Not downloading (Not modified since last crawl)");
            return inDB;
        }
        return null;
    }

    @Override
    public boolean prefilter(URL url) {
        if (url == null) {
            LOG.error("Received a null URL");
            return true;
        }

        String[] split = url.getPath().split("/");
        if (split.length > Config.MAX_PAGE_DEPTH) {
            LOG.info(url + ": Prefiltering (Max page depth exceeded)");
            return true;
        }

        String host = url.getHost();

        // If URL happens to be in the set of bad hosts, don't do anything further
        if (badHostsSet.contains(host)) {
            return true;
        }

        // If a URL belongs to one of the most common English TLDs, we'll accept it
        int start = host.lastIndexOf(".");
        if (start == -1) {
            LOG.error(url + ": Prefiltering (Breaks TLD test)");
            return true;
        }
        String tld = url.getHost().substring(start, host.length());
        for (String preferred : commonAcceptedTLDs) {
            if (tld.equals(preferred)) {
                return false;
            }
        }

        // If a URL belongs to one of the most common non-English TLDs, we'll reject it
        for (String unaccepted: commonUnnacceptedTLDs) {
            if (tld.equals(unaccepted)) {
                return true;
            }
        }

        // For now, we'll let the oddballs slip through the cracks, just to see what happens
        return false;



    }


}
