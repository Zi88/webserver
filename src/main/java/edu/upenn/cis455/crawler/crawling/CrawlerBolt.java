package edu.upenn.cis455.crawler.crawling;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.crawler.Bookkeeper;
import edu.upenn.cis455.crawler.frontier.WorkItem;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import edu.upenn.cis455.crawler.webtools.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Alex on 3/21/18
 */
public class CrawlerBolt implements IRichBolt {

    private static Logger LOG = LogManager.getLogger(CrawlerBolt.class);

    private String executorId = "CrawlerBolt-" + UUID.randomUUID().toString();
    private Fields fields = new Fields("Response");
    private Worker worker;
    private OutputCollector collector;

    public CrawlerBolt() {
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }

    @Override
    public void cleanup() {

    }

    @Override
    public void execute(Tuple input) {
        WorkItem<URL> workItem = (WorkItem<URL>) input.getObjectByField("WorkItem");

        Response r = worker.waitThenCrawl(workItem);

        // Emit the URL and Response as long as it is non-null
        if (r != null) {
            collector.emit(new Values<>(r));
        }
    }

    @Override
    public void prepare(Map<String, Object> stormConf, TopologyContext context, OutputCollector collector) {
        // Pass in the Frontier and Bookkeeper from the Config map, use these to create a Worker
        try {
            MercatorFrontier f = (MercatorFrontier) stormConf.get("Frontier");
            Bookkeeper b = (Bookkeeper) stormConf.get("Bookkeeper");
            this.worker = new Worker(f, b);
            this.collector = collector;
        } catch (UnknownHostException  | SocketException e) {
            LOG.error(executorId + ": Error when instantiating worker", e);
        }
    }

    @Override
    public void setRouter(IStreamRouter router) {
        collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return fields;
    }
}
