package edu.upenn.cis455.crawler.pagetypes;

import com.amazonaws.services.dynamodbv2.datamodeling.*;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alex on 3/30/18
 */
@DynamoDBTable(tableName = "Pages")
public class DynamoPageItem implements Page {

    private String URL;
    private int statusCode;
    private String fingerprint;     // A hash that we may use for identifying duplicate content
    private long timestamp;
    private long contentLength;
    private HashMap<String, String> headers;
    private ByteBuffer bodyBuff;
    private int chunks;      // For chunking (0 chunks means whole body is here, > 0 means we need to assemble it

    @DynamoDBHashKey(attributeName = "URL")
    public String getURL() {
        return URL;
    }
    public void setURL(String URL) {
        this.URL = URL;
    }

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int sc) {
        statusCode = sc;
    }

    public String getFingerprint() {
        return fingerprint;
    }
    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public long getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getContentLength() {
        return contentLength;
    }
    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    @DynamoDBTypeConverted(converter = MapTypeConverter.class)
    public Map<String, String> getHeaders() {
        return headers;
    }
    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public ByteBuffer getBodyBuff() {
        return bodyBuff;
    }
    public void setBodyBuff(ByteBuffer body) {
        this.bodyBuff = body;
    }

    public int getChunks() {
        return chunks;
    }
    public void setChunks(int numChunks) {
        this.chunks = numChunks;
    }


    @DynamoDBIgnore
    public byte[] getBody() {
        return bodyBuff.array();
    }

    @DynamoDBIgnore
    public void setBody(byte[] body) {
        bodyBuff = ByteBuffer.wrap(body);
    }

    @DynamoDBIgnore
    public String getHeader(String key) {
        return headers.get(key);
    }

    @Override
    public List<String> getHeaders(String key) {
        // TODO: 4/17/18
        return null;
    }

    @DynamoDBIgnore
    public void setHeader(String key, String value) {
        //// TODO: 4/17/18  
    }

    @Override
    public void addHeader(String k, String v) {
        // TODO: 4/17/18  
    }

    @Override
    public void setLinks(List<String> links) {
        //// TODO: 4/16/18
    }

    @Override
    public List<String> getLinks() {
        // TODO: 4/17/18
        return null;
    }

}
