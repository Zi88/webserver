package edu.upenn.cis455.crawler.pagetypes;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alex on 3/31/18
 *
 * A class for serializing and deserializing a Map<String, String> into a Set<String>, for use in DynamoDB
 */
public class MapTypeConverter implements DynamoDBTypeConverter<Set<String>, Map<String, String>> {

    @Override
    public Set<String> convert(Map<String, String> stringStringMap) {
        HashSet<String> serialized = new HashSet<>();
        for (Map.Entry<String, String> e : stringStringMap.entrySet()) {
            String s = e.getKey() + "->" + e.getValue();
            serialized.add(s);
        }
        return serialized;
    }

    @Override
    public Map<String, String> unconvert(Set<String> strings) {
        Map<String, String> map = new HashMap<>();
        for (String s : strings) {
            String[] split = s.split("->");
            map.put(split[0], split[1]);
        }
        return map;
    }
}
