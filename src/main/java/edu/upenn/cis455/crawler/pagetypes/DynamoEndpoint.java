package edu.upenn.cis455.crawler.pagetypes;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Alex on 3/31/18
 */
public class DynamoEndpoint {

    private static final int CHUNK_THRESH = 350000;  // Threshold (in bytes) for chunking an item
    private static final Logger LOG = LogManager.getLogger(DynamoEndpoint.class);

    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    private static DynamoDBMapperConfig conf = DynamoDBMapperConfig.DEFAULT;
    private static  DynamoDBMapper mapper = new DynamoDBMapper(client);

    public static void putPageItem(DynamoPageItem page) {
        byte[] body = page.getBody();

        // Chunk up the page if the body is greater than 350 KB
        int numChunks = body.length / CHUNK_THRESH;
        for (int i = 0; i < numChunks; i++) {
            int start = i * CHUNK_THRESH;
            int len = Math.min(CHUNK_THRESH, body.length - start);
            ByteBuffer chunk = ByteBuffer.wrap(body, start, len);
            putChunkItem(chunk, i, page.getURL());
        }
        page.setChunks(numChunks);
        mapper.save(page, conf);
    }

    /**
     * Put a chunk of a page into the Dynamo "Chunks" table
     */
    private static void putChunkItem(ByteBuffer chunk, int i, String url) {
        DynamoChunkItem chunkItem = new DynamoChunkItem();
        chunkItem.setKey(url + "-" + i);
        chunkItem.setChunk(chunk);
        mapper.save(chunkItem, conf);
    }

    public static Page getPageItem(String url) {
        try {
            DynamoPageItem pageItem = mapper.load(DynamoPageItem.class, url, conf);

            // Reassemble the chunks, if there are any
            if (pageItem != null && pageItem.getChunks() > 0) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                for (int i = 0; i < pageItem.getChunks(); i++) {
                    DynamoChunkItem chunkItem = getChunkItem(pageItem.getURL(), i);
                    baos.write(chunkItem.getChunk().array());
                }
                pageItem.setBody(baos.toByteArray());
            }
            return pageItem;

        } catch (IOException e) {
            LOG.error("DynamoDB de-chunking failed", e);
            return null;
        }

    }


    private static DynamoChunkItem getChunkItem(String url, int i) {
        return mapper.load(DynamoChunkItem.class, url + "-" + i, conf);
    }
}
