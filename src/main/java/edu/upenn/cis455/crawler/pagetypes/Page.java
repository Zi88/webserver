package edu.upenn.cis455.crawler.pagetypes;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by Alex on 3/31/18
 */
public interface Page {

    int getStatusCode();

    void setStatusCode(int statusCode);

    String getURL();

    void setURL(String URL);

    String getFingerprint();

    void setFingerprint(String fingerprint);

    long getTimestamp();

    void setTimestamp(long timestamp);

    long getContentLength();

    void setContentLength(long contentLength);

    byte[] getBody();

    void setBody(byte[] body);

    String getHeader(String key);

    List<String> getHeaders(String key);

    void setHeader(String key, String value);

    void addHeader(String k, String v);

    void setLinks(List<String> links);

    List<String> getLinks();

}
