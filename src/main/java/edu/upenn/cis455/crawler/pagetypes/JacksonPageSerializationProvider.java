package edu.upenn.cis455.crawler.pagetypes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Created by Alex on 4/16/18
 */
public class JacksonPageSerializationProvider implements SerializationProvider<Page> {

    private static final Logger log = LogManager.getLogger(JacksonPageSerializationProvider.class);
    private ObjectMapper mapper = new ObjectMapper();

    // Use Jackson's serialization to do all the tough work.  Don't pretty print, because we want this to be one line.
    @Override
    public String writeObject(Page obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e);
            return null;
        }
    }

    // Use Jackon's deserialization to do all the tough work.  Return the JacksonPage, which adheres to the Page interface
    @Override
    public JacksonPage readObject(String serialized) {
        try {
            return mapper.readValue(serialized, JacksonPage.class);
        } catch (IOException e) {
            log.error(e);
            return null;
        }
    }

    @Override
    public String generateKey(Page obj) {
        return obj.getURL();
    }

    // Shouldn't ever call this...it's only here because it has to be
    @Deprecated
    @Override
    public String[] tokenize(Page obj) {
        return null;
    }
}
