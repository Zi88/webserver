package edu.upenn.cis455.crawler.pagetypes;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.nio.ByteBuffer;

/**
 * Created by Alex on 4/1/18
 */
@DynamoDBTable(tableName = "Chunks")
public class DynamoChunkItem {

    private String key;             // Key is "url-chunkId" e.g. http://foo.com/bar.html-0
    private ByteBuffer chunk;       // Underlying chunk is a ByteBuffer

    @DynamoDBHashKey(attributeName = "key")
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }


    public ByteBuffer getChunk() {
        return chunk;
    }
    public void setChunk(ByteBuffer chunk) {
        this.chunk = chunk;
    }
}
