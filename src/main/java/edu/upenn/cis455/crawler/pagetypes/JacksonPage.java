package edu.upenn.cis455.crawler.pagetypes;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Alex on 4/16/18
 */
public class JacksonPage implements Page {

    private int statusCode;
    private String url;
    private String fingerprint;
    private long timestamp;
    private long contentLength;
    private byte[] body;     // Jackson is supposedly smart enough to turn this to Base64 on its own
    private HashMap<String, List<String>> headers = new HashMap<>();
    private List<String> links = new ArrayList<>();

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public void setURL(String URL) {
        this.url = URL;
    }

    @Override
    public String getFingerprint() {
        return fingerprint;
    }

    @Override
    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public long getContentLength() {
        return contentLength;
    }

    @Override
    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }


    @Override
    public byte[] getBody() {
        return body;
    }


    @Override
    public void setBody(byte[] body) {
        this.body = body;
    }

    // Ignore these because they are meaningless for Jackson
    @JsonIgnore
    @Override
    public String getHeader(String key) {
        List<String> vals = headers.get(key.toLowerCase());
        if (vals == null || vals.size() == 0) {
            return null;
        }
        return vals.get(0);
    }

    @JsonIgnore
    @Override
    public List<String> getHeaders(String key) {
        return headers.get(key.toLowerCase());
    }

    @JsonIgnore
    @Override
    public void setHeader(String key, String value) {
        List<String> vals = new ArrayList<>();
        vals.add(value);
        headers.put(key.toLowerCase(), vals);
    }

    @JsonIgnore
    @Override
    public void addHeader(String k, String v) {
        List<String> vals = getHeaders(k);
        if (vals == null) {
            vals = new ArrayList<>();
            headers.put(k.toLowerCase(), vals);
        }
        vals.add(v);
    }

    @Override
    public void setLinks(List<String> links) {
        this.links = links;
    }

    @Override
    public List<String> getLinks() {
        return links;
    }

    // Used by Jackson
    public void setHeaders(HashMap<String, List<String>> _headers) {
        headers = _headers;
    }

    // Used by Jackson
    public HashMap<String, List<String>> getHeaders() {
        return headers;
    }
}
