package edu.upenn.cis455.crawler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.crawler.crawling.CrawlerBolt;
import edu.upenn.cis455.crawler.documentprocessing.DocumentProcessorBolt;
import edu.upenn.cis455.crawler.frontier.FrontierSpout;
import edu.upenn.cis455.crawler.hostsplitter.HostSplitterBolt;
import edu.upenn.cis455.crawler.urlfiltering.UrlFilterBolt;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import edu.upenn.cis455.crawler.utils.URLSerializationProvider;
import edu.upenn.cis455.crawler.xpathmatching.XPathEngineBolt;
import edu.upenn.cis455.extras.StaticPrioritizer;

import java.net.URL;

public class XPathCrawlerFactory {

    private static final String URL_SPOUT = "URL_SPOUT";
    private static final String CRAWL_BOLT = "CRAWL_BOLT";
    private static final String XPATH_BOLT = "XPATH_BOLT";
    private static final String PROCESS_BOLT = "PROCESS_BOLT";
    private static final String SPLIT_BOLT = "SPLIT_BOLT";
    private static final String FILTER_BOLT = "FILTER_BOLT";

    private MercatorFrontier<URL> f;
    private Bookkeeper bookkeeper;

    public LocalCluster getCrawler() throws Exception {

        edu.upenn.cis.stormlite.Config config = new edu.upenn.cis.stormlite.Config();

        if (f == null) {
            System.err.println("Frontier not initialized before building topology");
            System.exit(-1);
        }

        if (bookkeeper == null) {
            bookkeeper = new Bookkeeper(f);
        }
        config.put("Frontier", f);
        config.put("Bookkeeper", bookkeeper);

        // Instantiate all the spouts and bolts
        FrontierSpout frontierSpout = new FrontierSpout();
        CrawlerBolt crawlerBolt = new CrawlerBolt();
        XPathEngineBolt xpathBolt = new XPathEngineBolt();
        DocumentProcessorBolt processorBolt = new DocumentProcessorBolt();
        HostSplitterBolt splitterBolt = new HostSplitterBolt();
        UrlFilterBolt filterBolt = new UrlFilterBolt();

        // FrontierSpout -> CrawlerBolt -> XPathEngineBolt -> DocumentProcessorBolt -> UrlFilterBolt
        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(URL_SPOUT, frontierSpout, 1);

        // Potentially many crawlers/workers, which receive URLs with no particular grouping
        builder.setBolt(CRAWL_BOLT, crawlerBolt, Config.NUM_WORKERS).shuffleGrouping(URL_SPOUT);

        // Potentially many document processors, which receive Response objects with no particular grouping
        builder.setBolt(PROCESS_BOLT, processorBolt, 10).shuffleGrouping(CRAWL_BOLT);

        // TODO could potentially have many of these?
        // One splitter bolt, which sends and receives sharded hostnames with the Chord router
        builder.setBolt(SPLIT_BOLT, splitterBolt, 1).shuffleGrouping(PROCESS_BOLT);

        // TODO only one of these...use first grouping
        // Potentially many url filterers, which receive URLs by hostname
        builder.setBolt(FILTER_BOLT, filterBolt, 1).fieldsGrouping(SPLIT_BOLT, new Fields("Host"));


        LocalCluster cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
        try {
            String str = mapper.writeValueAsString(topo);
            System.out.println("The StormLite topology is:\n" + str);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        cluster.submitTopology("CrawlerTopo", config, builder.createTopology());
        return cluster;
	}

    public void setFrontier(MercatorFrontier frontier) {
        f = frontier;
    }

    public void setBookkeeper(Bookkeeper bookkeeper) {
        this.bookkeeper = bookkeeper;
    }
}
