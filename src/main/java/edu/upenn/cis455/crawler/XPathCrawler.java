package edu.upenn.cis455.crawler;


import com.amazonaws.services.glue.model.Crawler;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis455.crawler.hostsplitter.RcvDaemon;
import edu.upenn.cis455.crawler.hostsplitter.SendDaemon;
import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import edu.upenn.cis455.crawler.utils.ConservativeURLSerializationProvider;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import edu.upenn.cis455.crawler.utils.URLSerializationProvider;
import edu.upenn.cis455.extras.DynamicPrioritizer;
import edu.upenn.cis455.extras.StaticPrioritizer;
import edu.upenn.cis455.storage.BerkeleyManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class XPathCrawler {

    private static final Logger LOG;

    static {
        String log4jProps = "./resources/log4j.properties";
        PropertyConfigurator.configure(log4jProps);
        LOG = LogManager.getLogger(XPathCrawler.class);
        LOG.info("Initializing log");
    }
	
  public static void main(String args[]) throws Exception {

      // Parse args

      if (args.length < 4) {
          System.out.println("Author: Alex Thurston (thurston)");
          System.out.println("args: [seeds file] [priorities file] [finger file] [ip:port] [# to crawl] [working dir] [persistent dir]");
          System.out.println("See edu.upenn.cis455.crawler.Config to pass additional arguments");
          System.exit(0);
      }

      // Set the routing and directory related information (which is used bolts and frontier to initialize)
      Config.FINGERS_FILE = args[2];
      Config.SELF_ADDR = args[3];

      // Set # of docs to crawl, if provided
      if (args.length >= 5) {
          Config.MAX_PAGES_TO_CRAWL = Integer.parseInt(args[4]);
          LOG.info("Crawling " + Config.MAX_PAGES_TO_CRAWL + " pages, then shutting down");
      }

      // A temporary workaround to help us test in a single project
      if (args.length >= 6) {
          Config.WORKING_DIR = args[5];
          Config.PERSISTENT_DIR = args[6];
          Config.CHECKPOINT_DIR = Config.PERSISTENT_DIR + "/CheckpointHome";
          Config.DATABASE_ENV_DIR = Config.PERSISTENT_DIR + "/BerkleyHome";
          Config.PAGE_DIR = Config.PERSISTENT_DIR + "/PageHome";

      }

      // Clean the starting URLs (default URL provided should not need to be used)
      List<URL> seeds = CrawlerUtils.readSeedsFile(args[0]);

      // Initialize DB
      BerkeleyManager.initManager();

      // Instantiate shared data structures: the robots cache, url frontier, filterer, and processor
      SerializationProvider<URL> urlProvider = new ConservativeURLSerializationProvider();  // use the conservative one
      MercatorFrontier<URL> frontier = new MercatorFrontier<URL>(urlProvider, new DynamicPrioritizer<>(args[1], urlProvider));

      // Instantiate the Bookkeeper
      Bookkeeper bookkeeper = new Bookkeeper(frontier);

      // Restore the frontier from the checkpoint directory, if necessary
      bookkeeper.loadFrontier();

      // Enqueue the starting page(s) as the first URL in the frontier (besides any existing ones)
      frontier.initialize(seeds, Config.WORKING_DIR);
      
      // Pass the factory shared state, and let it build the topology and start the cluster
      XPathCrawlerFactory factory = new XPathCrawlerFactory();
      factory.setFrontier(frontier);
      factory.setBookkeeper(bookkeeper);
      LocalCluster cluster = factory.getCrawler();
      
      // Register a shutdown handler to save state
      Runtime.getRuntime().addShutdownHook(new ShutdownHandler(frontier, cluster));
      
      // Start the bookeeper
      bookkeeper.start();
      
      // Create and start the daemons.  Static fields should have already when the topology was built
      Thread sendDaemonT = new Thread(new SendDaemon());
      Thread rcvDaemonT = new Thread(new RcvDaemon(Config.SELF_ADDR));
      sendDaemonT.setDaemon(true);
      rcvDaemonT.setDaemon(true);
      sendDaemonT.start();
      rcvDaemonT.start();
  }
}
