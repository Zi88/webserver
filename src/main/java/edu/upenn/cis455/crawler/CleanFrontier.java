package edu.upenn.cis455.crawler;

import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import edu.upenn.cis455.crawler.utils.ConservativeURLSerializationProvider;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import edu.upenn.cis455.crawler.utils.URLCleaningCallback;
import edu.upenn.cis455.extras.StaticPrioritizer;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;

/**
 * Created by Alex on 4/28/18
 */
public class CleanFrontier {

    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            System.out.println("Usage: [frontier dir] [priorities file]");
            return;
        }
        String frontierDir = args[0];     //  The dir to clean
        String prioritiesFile = args[1];  //  The priorities.txt file for rearranging things
        SerializationProvider<URL> provider = new ConservativeURLSerializationProvider();
        MercatorFrontier<URL> frontier =
                new MercatorFrontier<>(provider, new StaticPrioritizer<>(prioritiesFile, provider));
        frontier.initialize(Collections.emptyList(), frontierDir);
        frontier.cleanQueues(new URLCleaningCallback());
    }
}
