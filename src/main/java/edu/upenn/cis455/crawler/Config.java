package edu.upenn.cis455.crawler;

import edu.upenn.cis455.crawler.hostsplitter.ChordRouter;

/**
 * Created by Alex on 3/1/18
 */
public class Config {

    // Maximum (default size) of a Cache, beyond which items are evicted
    public static final int DEFAULT_CACHE_SIZE = 32768; // 2^15

    // Maximum time in milliseconds to block on reading from sockets
    public static final int DEFAULT_SO_TIMEOUT = 15000;

    // Time in milliseconds that workers will wait between requests if a host has no robots.txt or crawl delay directive
    public static final Integer COURTESY_DELAY = 5000;

    // The depth beneath which the crawler will filter out pages
    public static final int MAX_PAGE_DEPTH = 10;

    // Maximum size in megabytes of a resource to download
    public static long MAX_CONTENT_LEN_MB = 2;

    // Number of pages to crawl before stopping (Will never stop if this equals Integer.MAX_VALUE)
    public static int MAX_PAGES_TO_CRAWL = 25000;//Integer.MAX_VALUE;

    // How often (in minutes) the crawler should checkpoint its state
    public static final int CHECKPOINT_INTERVAL_MINUTES = 5;

    // The crawler's name, sent along as a User-Agent header in HTTP requests
    public static final String CRAWLER_NAME = "cis455crawler";

    // Number of workers (CrawlerBolt executors) to use per JVM
    public static final int NUM_WORKERS = 50;

    // Number of threads in the thread pool used to run all executors
    public static final int THREAD_POOL_SIZE = 50;

    // A directory that we guarantee to be persistent across crawler runs
    public static String PERSISTENT_DIR = "./persistent";

    // Home directory for Berkeley DB (persistent)
    public static String DATABASE_ENV_DIR = PERSISTENT_DIR + "/BerkleyHome";

    // Home directory for checkpoint files.  This may be the same as the working directory
    public static String CHECKPOINT_DIR = PERSISTENT_DIR + "/CheckpointHome";

    // Home directory for the append-only Page files (JSON objects as Strings, one per line)
    public static String PAGE_DIR = PERSISTENT_DIR + "/PageHome";

    // Working directory for the crawler, where queue buffer files get saved temporarily (no guarantee of persistence)
    public static String WORKING_DIR = "./tmp";

    // Maximum character length of a scraped href, beyond which the crawler ignores it
    public static final int HREF_LEN_LIMIT = 256;

    // Buffer size (in bytes) used for the page queues when appending to their underlying files.
    // If writing pages to EBS (over network), we will want to tune this to get high throughput
    public static final int PAGES_FILE_BUFFER_SIZE = 32768;  //2^15, or 32 KiB

    // The hostname to which the crawler sends monitoring info
    public static String MONITORING_HOSTNAME = "cis455.cis.upenn.edu";

    // This node's IP:Port for receiving Chord messages, passed on the command line
    public static String SELF_ADDR = "127.0.0.1:8080";

    // The path to the finger pointers file, also passed on the command line
    public static String FINGERS_FILE = "./resources/fingers.txt";
}
