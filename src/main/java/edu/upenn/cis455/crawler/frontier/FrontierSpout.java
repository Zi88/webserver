package edu.upenn.cis455.crawler.frontier;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.spout.IRichSpout;
import edu.upenn.cis.stormlite.spout.SpoutOutputCollector;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.crawler.utils.HeapEntry;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Alex on 3/21/18
 */
public class FrontierSpout implements IRichSpout {

    private static final Logger log = LogManager.getLogger(FrontierSpout.class);

    private MercatorFrontier<URL> frontier;
    private Fields fields = new Fields("WorkItem");
    private SpoutOutputCollector collector;

    public FrontierSpout(MercatorFrontier<URL> frontier) {
        this.frontier = frontier;
    }

    public FrontierSpout() {
    }

    @Override
    public String getExecutorId() {
        return "FrontierSpout-" + UUID.randomUUID().toString();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }

    @Override
    public void open(Map<String, Object> config, TopologyContext topo, SpoutOutputCollector collector) {
        this.collector = collector;
        frontier = (MercatorFrontier<URL>) config.get("Frontier");
        // If the frontier were persistent on disk, we'd do some work here to restore some of it into memory
    }

    @Override
    public void close() {
        // If the frontier were persistent on disk, we'd maybe close our files here or cleanup or whatever
    }

    @Override
    public void nextTuple() {
        if (frontier.isEmpty()) {
            return;
        }

        // Dequeue a heap entry, take the first URL out of the heap entry's queue
        HeapEntry<URL> heapEntry = frontier.dequeue();

        // A rare issue that seems to come up. Probably a race condition that I can't find.
        if (heapEntry.queueHandle.isEmpty()) {
            log.error("Got an empty backend queue from the frontier! Host: " + heapEntry.hostname);
            return;
        }
        URL url = heapEntry.queueHandle.remove();

        // Bundle the URL into a WorkItem, emit this
        WorkItem<URL> workItem = new WorkItem<URL>(url, heapEntry.timestamp);
        collector.emit(new Values(workItem));
    }

    @Override
    public void setRouter(IStreamRouter router) {
        collector.setRouter(router);
    }
}
