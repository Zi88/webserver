package edu.upenn.cis455.crawler.frontier;

import java.io.Serializable;

/**
 * Created by Alex on 3/21/18
 */
public class WorkItem<T> implements Serializable {

    public T item;          // The item to work on (probably a URL).  Needs to implement Serializable whatever it is
    public long timestamp;  // The timestamp for when it is safe to contact this host

    public WorkItem(T item, long timestamp) {
        this.item = item;
        this.timestamp = timestamp;
    }
}
