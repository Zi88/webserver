package edu.upenn.cis455.crawler;

import edu.upenn.cis455.crawler.utils.MercatorFrontier;
import edu.upenn.cis455.storage.BerkeleyManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Created by Alex on 3/4/18
 */
public class Bookkeeper extends Thread {

    private static final Logger LOG = LogManager.getLogger(Bookkeeper.class);
    static int docsCrawled = 0;
    private MercatorFrontier urlFrontier;
    private int docsThisInterval;
    private File statFile;

    public Bookkeeper(MercatorFrontier urlFrontier) {
        this.urlFrontier = urlFrontier;
        this.statFile = new File(Config.PERSISTENT_DIR +
                String.format("/LogHome/crawlstats-%d.csv", System.currentTimeMillis()));
    }

    // Methods below run in Worker threads (through their references to the Bookkeeper)

    /**
     * Increment the number of documents crawled, and check if we should discontinue the crawl
     * @return true if the calling worker should continue crawling. False otherwise
     */
    public synchronized boolean continueCrawling() {
        if (docsCrawled + 1 > Config.MAX_PAGES_TO_CRAWL) {
            LOG.error(String.format("Crawler has hit page limit (%d pages requested) Requesting shutdown.", Config.MAX_PAGES_TO_CRAWL));
            // TODO would be ideal to notify workers and give them a chance to stop before calling System.exit, or have each crawler do its own local shutdown
            System.exit(0);
            return false;
        }
        docsCrawled++;
        docsThisInterval++;
        return true;
    }



    /**
     * Notify the worker threads that they should stop crawling after completing their current requests
     */
//    public void notifyWorkers() {
//        // All workers are interrupted (should be kicked out of any blocking calls, and should stop executing on their
//        // next pass through the main loop)
//        for (Thread t : workers) {
//            t.interrupt();
//        }
//        // The bookkeeper thread is itself interrupted
//        this.interrupt();
//    }



    // Methods below run in Bookkeeper Thread

    /**
     * The Bookkeeper thread sleeps inside a loop, periodically waking up to checkpoint the URL frontier and some other
     * server state.
     */
    @Override
    public void run() {
        dumpConf();
        while (!this.isInterrupted()) {
            try {
                docsThisInterval = 0;
                Thread.sleep(Config.CHECKPOINT_INTERVAL_MINUTES * 60 * 1000);
                checkpoint();
            } catch (InterruptedException e) {
                LOG.error("System shutting down. Docs crawled:" + docsCrawled);
                checkpoint();
                return;
            }
        }
    }

    private void dumpConf() {
        try {
            // # Threads, # Crawl Executors, Courtesy Delay, SO_Timeout
            String line = String.format("%d,%d,%d,%d,%d\n",
                    Config.THREAD_POOL_SIZE, Config.NUM_WORKERS, Config.COURTESY_DELAY,
                    Config.DEFAULT_SO_TIMEOUT, Config.MAX_CONTENT_LEN_MB);

            if (!statFile.exists()) {
                statFile.getParentFile().mkdirs();
                statFile.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(statFile, true);

            // Dump to CSV file
            // Conf header
            fos.write("Pool Size,Number Workers,Courtesy Delay,SO Timeout,Max CLen MB\n".getBytes());
            fos.write(line.getBytes());
            // Interval stats header
            fos.write("Time,Docs Total,DPM\n".getBytes());
            fos.close();
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    /**
     * Checkpoint crawler state
     */
    private void checkpoint() {
        try {
            LOG.info("Beginning checkpoint of crawler state");
            LOG.info("Docs crawled so far: " + docsCrawled);
            BerkeleyManager.sync();
            saveFrontier();
            dumpState();
            LOG.info("Checkpoint completed successfully");
        } catch (IOException e) {
            LOG.error("Checkpoint failed", e);
        }
    }

    private void dumpState() {
        LOG.info("Docs crawled since last checkpoint: " + docsThisInterval);
        int docsPerMinute = docsThisInterval / Config.CHECKPOINT_INTERVAL_MINUTES;

        // timestamp, total docs crawled, docs per minute
        String line = String.format("%d,%d,%d\n", System.currentTimeMillis(), docsCrawled, docsPerMinute);

        // Dump to CSV file
        try (FileOutputStream fos = new FileOutputStream(statFile, true)) {
            fos.write(line.getBytes());
        } catch (IOException e) {
            LOG.error(e);
        }
        LOG.info("Docs per minute since last checkpoint: " + docsPerMinute);
    }

    protected void saveFrontier() throws IOException {
        // Save the entire frontier's state in its working directory first
        urlFrontier.save();

        // Copy the working directory to the checkpoint directory, if they are different
        // This is useful if e.g. working dir is an epheremal instance store, checkpoint dir is an EBS volume
        if (!Config.CHECKPOINT_DIR.equals(Config.WORKING_DIR)) {
            File workDir = Paths.get(Config.WORKING_DIR).toFile();
            File[] workingFiles = workDir.listFiles();
            if (workingFiles == null) {
                throw new IOException("Unable to get crawler's working directory queue files");
            }

            File checkpointDir = Paths.get(Config.CHECKPOINT_DIR).toFile();
            if (!checkpointDir.exists() && checkpointDir.mkdirs()) {
                LOG.info("Initialized checkpoint directory: " + checkpointDir);
            }

            // Copy each queue file over to the permanent location
            for (File f : workingFiles) {
                if (f.isDirectory()) {
                    continue;
                }
                Path dst = Paths.get(Config.CHECKPOINT_DIR, f.getName());
                Files.copy(f.toPath(), dst, StandardCopyOption.REPLACE_EXISTING);
            }
        }

    }


    // Methods below run in the Main thread, at startup

    /**
     * Attempt to reload the URL frontier from a checkpoint directory to a working directory
     * @throws IOException
     */
    public void loadFrontier() throws IOException {

        // If the Checkpoint directory is in a different place than the working directory, copy it over
        // Useful if the checkpoint directory is remote and persistent (e.g. an EBS volume), but working dir is not
        if (!Config.CHECKPOINT_DIR.equals(Config.WORKING_DIR)) {
            LOG.info("Restoring frontier from the checkpoint directory: " + Config.CHECKPOINT_DIR);
            File checkpointDir = Paths.get(Config.CHECKPOINT_DIR).toFile();
            File[] checkpointFiles = checkpointDir.listFiles();

            // This should never happen, but let's force the operator to make sure everything is where it's supposed to be
            if (checkpointFiles == null) {
                LOG.fatal("Unable to get crawler's checkpoint directory");
                System.exit(-1);
            }

            // Create the working dir if necessary
            File workdir = Paths.get(Config.WORKING_DIR).toFile();
            if (!workdir.exists() && workdir.mkdirs()) {
                LOG.info("Initialized working directory: " + workdir);
            }

            // Give copied files the same name as they have in checkpoint dir.  Note working dir contents are overwritten
            for (File f : checkpointFiles) {
                Path dst = Paths.get(Config.WORKING_DIR, f.getName());
                Files.copy(f.toPath(), dst, StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
}
