package edu.upenn.cis455.crawler.hostsplitter;

import edu.upenn.cis455.crawler.utils.CrawlerUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by Alex on 4/26/18
 */
public class ChordRouter {

    // Finger table is iterated s.t. entries go from higher to lower address
    private SortedMap<Long, NodeAddr> fingersTable = new TreeMap<>((o1, o2) -> -o1.compareTo(o2));
    public NodeAddr self;
    private MessageDigest digest;

    public ChordRouter() {}

    public void init(String selfAddr, String fingersTxt) throws NoSuchAlgorithmException, IOException {
        // Fill in the basics
        digest = MessageDigest.getInstance("SHA-1");
        fingersTable = new TreeMap<>((o1, o2) -> -o1.compareTo(o2));
        self = new NodeAddr();
        self.addr = selfAddr;
        self.hash = getHash(self.addr);
        self.hex = getHexHash(self.addr);
        self.end = self.hash;

        // Read the routing file to create finger pointers
        SortedMap<Long, NodeAddr> naturallyOrdered = new TreeMap<>();
        List<String> fingers = CrawlerUtils.readStringsFile(fingersTxt);
        for (String line: fingers) {
            // can give everyone the same fingers file if we just make sure not to add self's entry to fingers table
            if (line.equals(selfAddr)) {
                continue;
            }
            NodeAddr f = new NodeAddr();
            f.addr = line;
            f.hash = getHash(line);
            f.end = f.hash;
            fingersTable.put(f.hash, f);
            naturallyOrdered.put(f.hash, f);
        }

        // Our own successor is the next highest entry in the finger table, or the lowest entry if there's nothing higher
        self.successor = fingersTable.get(fingersTable.lastKey());
        for (Map.Entry<Long, NodeAddr> e : fingersTable.entrySet()) {
            if (e.getKey() > self.hash) {
                self.successor = e.getValue();
            }
        }

        // Our own predecessor is the next lowest entry in the finger table, or the highest entry if there's nothing lower
        self.predecessor = fingersTable.get(fingersTable.firstKey());
        for (Map.Entry<Long, NodeAddr> e  : naturallyOrdered.entrySet()) {
            if (e.getKey() < self.hash) {
                self.predecessor = e.getValue();
            }
        }

        // Create the intervals for each node. Include self in the map so our values are taken into account too
        naturallyOrdered.put(self.hash, self);
        Long[] ids = naturallyOrdered.keySet().toArray(new Long[naturallyOrdered.size()]);
        boolean hasLowerPredecessor = false;
        for (int i = 0; i < ids.length; i++) {
            NodeAddr curr = naturallyOrdered.get(ids[i]);
            NodeAddr pred = null;
            if (i == 0) {
                pred = naturallyOrdered.get(naturallyOrdered.lastKey());
            } else {
                pred = naturallyOrdered.get(ids[i - 1]);
            }
            // Fix current node's starting hash
            curr.start = pred.end + 1;
//            // Fix self's starting hash (gradually walk it clockwise)
//            if (curr.end < self.hash) {
//                hasLowerPredecessor = true;
//                self.start = curr.end + 1;
//            }
        }
//        // If self is the lowest key, we need to fix it by wrapping the highest key
//        if (!hasLowerPredecessor) {
//            self.start = naturallyOrdered.get(naturallyOrdered.lastKey()).end + 1;
//        }
    }

    private String getHexHash(String addr) {
        byte[] rawBytes = digest.digest(addr.getBytes());
        return DatatypeConverter.printHexBinary(rawBytes);
    }

    public long getHash(String addr) {
        if (addr == null) {
            System.err.println("Received a null routing address!");
            return -1;
        }
        byte[] rawBytes = digest.digest(addr.getBytes());
        long hash = ByteBuffer.wrap(rawBytes).getLong();


        // Truncate 4 leftmost bits to always get a positive number
        hash = (hash & 0xfffffffffffffffL);
        digest.reset();
        return hash;
    }


    public String route(String key) {
        // Hash the hostname
        long hash = getHash(key);
        return route(hash);
    }

    public String route(long hash) {
        NodeAddr n = findSuccessor(hash);
        return n.addr;
    }

//    public String routeHex(String hexHash) {
//        NodeAddr n = findSuccessor(hexHash);
//        return  n.addr;
//    }

//    private NodeAddr findSuccessor(String id) {
        // The base condition: the id is between the start of our interval (inclusive) and our own hash (inclusive)
//        if (insideInterval(self.hexStart, incrHex(self.hexEnd, 1), id)) {
//            return self;
//        }
//        // The second base condition: the id is between self's id (exclusive) and successor's id (inclusive)
//        if (insideInterval(self.end + 1, self.successor.end + 1, id)) {
//            return self.successor;
//        }
//        NodeAddr n = findPredecessor(id);
//
//        // In recursive version, you only know the successor on the last hop
//        if (n.successor == null) {
//            return n;
//        }
//        return n.successor;
//    }

    // These three methods are straight from the Chord paper pseudocode
    private NodeAddr findSuccessor(long id) {
        // The base condition: the id is between the start of our interval (inclusive) and our own hash (inclusive)
        if (insideInterval(self.start, self.end + 1, id)) {
            return self;
        }
        // The second base condition: the id is between self's id (exclusive) and successor's id (inclusive)
        if (insideInterval(self.end + 1, self.successor.end + 1, id)) {
            return self.successor;
        }
        NodeAddr n = findPredecessor(id);

        // In recursive version, you only know the successor on the last hop
        if (n.successor == null) {
            return n;
        }
        return n.successor;
    }

    private NodeAddr findPredecessor(long id) {
        NodeAddr n = self;
        return closestPrecedingNode(n, id);

        // THe iterative version
        // While id is not between n's hash (exclusive) and successor (inclusive), keep jumping clockwise
//        while (!(id > n.hash && id <= n.successor.hash)) {
//            n = closestPreceedingNode(n, id);
//        }
//        return n;
    }

    // This is a local lookup in the finger table for the first hop, then an RPC on all subsequent hops
    private NodeAddr closestPrecedingNode(NodeAddr n, long id) {
        // If we are still routing from our self, look for the finger pointer immediately preceding the search id
        if (n.hash == self.hash) {
            // Table is ordered to iterate from higher to lower keys
            NodeAddr insideWrap = null;
            for (Map.Entry<Long, NodeAddr> e: fingersTable.entrySet()) {
                // Route to a pointer between n (exclusive) and id (exclusive)
                if (insideInterval(n.hash, id - 1, e.getValue().hash)) {
                    // TODO not sure this optimization is needed
                    // When interval wraps, we can't go with the first value that fits it.  Need the last one instead
                    if (n.hash > id - 1) {
                        insideWrap = e.getValue();
                    } else {
                        return e.getValue();
                    }
                }
            }
            if (insideWrap != null) {
                return insideWrap;
            }
            // If all the finger pointers overshoot, we use our self
            return self;
        }
        // We don't keep track of pointers of other nodes.  Route the message to them to find it.
        else {
            return null;
        }
    }

    // Open on left, closed on right
    private static boolean insideInterval(long start, long end, long i) {
        // Normal case
        if (start < end && start <= i && i < end) {
            return true;
        }
        // Wrap around case
        if (end < start) {
            if (i >= start) {
                return true;
            }
            if (i < end) {
                return true;
            }
        }
        return false;
    }

    public static class NodeAddr {
        public String addr;  // e.g. "12.34.56.78:80
        public long hash;    // fully qualified hash of the address
        public String hex;
        public NodeAddr successor;
        public NodeAddr predecessor;
        public long start;
        public long end;
        public String hexStart;
        public String hexEnd;
    }
    
    
    public static void main(String[] args) {
    		ChordRouter cr = new ChordRouter();
    		
    }
    
}
