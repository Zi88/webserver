package edu.upenn.cis455.crawler.hostsplitter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sleepycat.persist.EntityCursor;
import edu.upenn.cis455.crawler.webtools.HTTPClient;
import edu.upenn.cis455.crawler.webtools.Response;
import edu.upenn.cis455.crawler.webtools.WebException;
import edu.upenn.cis455.storage.BerkeleyManager;
import org.apache.commons.logging.Log;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Alex on 4/25/18
 *
 * An endpoint for RPCs, some synchronous others asynchronous
 */
public class SendDaemon implements Runnable {

    private static final int BATCH_THRESH = 2;
    private static final long BACKOFF = 5 * 60 * 1000;   // Milliseconds to wait once an RPC to some node has failed repeatedly

    private static final Logger LOG = LogManager.getLogger(SendDaemon.class);

    private static BlockingQueue<SendItemEntity> queue = new LinkedBlockingDeque<>();

    private static Map<String, Integer> unsentMessages = new HashMap<>();
    private static Map<String, Integer> failedSends = new HashMap<>();
    private static Map<String, Long> waitUntil = new HashMap<>();

    private static HTTPClient rpcClient = new HTTPClient();


    @Override
    public void run() {
        try {
            LOG.info("SendDaemon started");
            while (true) {
                // Block forever waiting for an item to send
                SendItemEntity i = queue.take();
                addToVirtualQueue(i);
            }
        } catch (InterruptedException ignored) {}
    }

    /**
     * Add the SendItem to a queue of waiting items that have not yet been sent
     */
    private void addToVirtualQueue(SendItemEntity i) {
        BerkeleyManager.putSendItemEntity(i);
        int unsent = unsentMessages.getOrDefault(i.addr, 0) + 1;
        unsentMessages.put(i.addr, unsent);
        long waitTime = waitUntil.getOrDefault(i.addr, 0L);
        long now = System.currentTimeMillis();

        // If the SendItem isn't an urgent routing message and we've backed off from the remote node, don't send
        if (now < waitTime && i.type != SendItemEntity.MsgType.ROUTING) {
            return;
        }

        // Otherwise, send all the buffered messages in a batch (or as many as there are if the message is urgent)
        if (unsent > BATCH_THRESH || i.type == SendItemEntity.MsgType.ROUTING) {
            Queue<SendItemEntity> q = fetchBufferedSendItems(i.addr);
            batchSend(i.addr, q);
        }
    }

    /**
     * Read the unsent SendItemEntities out of Berkeley and into a queue
     */
    private Queue<SendItemEntity> fetchBufferedSendItems(String addr) {
        Queue<SendItemEntity> bufferedMsgs = new LinkedList<>();
        try (EntityCursor<SendItemEntity> cursor = BerkeleyManager.getSendItemsByAddr(addr)) {
            SendItemEntity curr = null;
            while ((curr = cursor.next()) != null) {
                bufferedMsgs.add(curr);
            }
        }
        return bufferedMsgs;
    }

    /**
     * Clear the SendItemEntities with the given address out of Berkeley
     */
    private void clearBufferedMessages(String addr) {
        // try-with-resource: the cursor must always be closed
        try (EntityCursor<SendItemEntity> cursor = BerkeleyManager.getSendItemsByAddr(addr)) {
            while (cursor.next() != null) {
                cursor.delete();
            }
        }
    }


    private void batchSend(String addr, Queue<SendItemEntity> q) {
        try {
            // Turn the ip:port into a proper URL
            String[] split = addr.split(":", 2);
            String ip = split[0];
            int port = Integer.parseInt(split[1]);

            // All RPCs made to the root path
            URL url = new URL("http", ip, port, "");

            // Bundle the queue of SendItems into JSON
            ObjectMapper om = new ObjectMapper();
            String body = om.writer().writeValueAsString(q);

            Response r = rpcClient.postResource(url, body);

            if (r.getStatusCode() != 200) {
                LOG.error(String.format("Remote RPC server responded unusually (%d)", r.getStatusCode()));
            }
            // Empty the buffer of SendItems for this remote node only on success
            else {
                clearBufferedMessages(addr);
                unsentMessages.put(addr, 0);
                failedSends.put(addr, 0);
            }

            // TODO for synchronous RPCs, you'd somehow pass the response back to the caller here.
        }
        catch (IOException | WebException e) {
            LOG.error("Error when sending to " + addr, e);
            int numFailures = failedSends.getOrDefault(addr, 0) + 1;
            failedSends.put(addr, numFailures + 1);

            // If we repeatedly fail to reach a node (e.g. because it already exited), wait longer between sends
            if (numFailures > 3) {
                LOG.info("Backing off from " + addr);
                waitUntil.put(addr, System.currentTimeMillis() + BACKOFF);
            }
        }
    }



    public static void sendUrl(String addr, URL url) {
        SendItemEntity i = new SendItemEntity(SendItemEntity.MsgType.URL, addr, url.toString());
        queue.offer(i);
    }

}
