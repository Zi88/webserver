package edu.upenn.cis455.crawler.hostsplitter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.crawler.hostsplitter.SendItemEntity.MsgType;
import edu.upenn.cis455.crawler.webtools.HttpParseException;
import edu.upenn.cis455.crawler.webtools.HttpRequest;
import edu.upenn.cis455.crawler.webtools.HttpResponse;
import edu.upenn.cis455.crawler.webtools.HttpUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.Queue;

/**
 * Created by Alex on 4/26/18
 */
public class RcvDaemon implements Runnable {

    private static final Logger LOG = LogManager.getLogger(RcvDaemon.class);
    private static final int BACKLOG = 50;

    private ServerSocket sock;
    private String ip;
    private int port;

    // The output collector is shared across all bolts
    private static OutputCollector collector;
    private static boolean initialized = false;

    private ObjectMapper om = new ObjectMapper();

    public RcvDaemon(String ipPort) {
        String[] split = ipPort.split(":", 2);
        ip = split[0];
        port = Integer.parseInt(split[1]);
    }

    // Pass in the shared outputcollector.  Called by the ProcessorBolt(s)
    public static synchronized void init(OutputCollector collector_) {
        if (initialized) {
            return;
        }
        initialized = true;
        collector = collector_;
    }

    @Override
    public void run() {
        LOG.info("RcvDaemon started");
        // TODO could start some other threads here to act as workers and do the RPCs themselves
        Socket client = null;
        try {
            // Bind on all interfaces
            if (sock == null) {
                sock = new ServerSocket(port, BACKLOG);
            }
            // Accept connections forever, parsing them and doing RPCs
            while (true) {
                client = sock.accept();
                HttpRequest req = HttpUtil.decodeRequest(client.getInputStream());
                int status = doRequest(req);
                writeResponse(client, status);
            }
        } catch (IOException e) {
            LOG.error(e);
        } catch (HttpParseException e) {
            // Shouldn't really be getting any of these since we control both the client and the server
            LOG.error(e);
            writeResponse(client, e.getCode());
        }
    }

    /**
     * Parse out the body of the request and do the necessary RPCs
     */
    private int doRequest(HttpRequest req) throws HttpParseException, IOException {
        // Disallow everything but POST
        if  (!req.getMethod().equals("POST")) {
            throw new HttpParseException(405);
        }

        // Parse the request body, which should be a JSON list
        Queue<SendItemEntity> q = om.readValue(req.getBody(), new TypeReference<Queue<SendItemEntity>>() {});
        SendItemEntity item = null;

        // Examine each item in the queue of items, doing different things as the RPC dictates
        while ((item = q.poll()) != null) {
            // Unpack URLs, send them to the collector (get fed back through the sharding bolt)
            if (item.type == MsgType.URL) {
                URL url = new URL(item.value);
                handleUrl(url);
            }
            // Respond to a routing message
            else if (item.type == MsgType.ROUTING) {
                // TODO: 4/26/18
                //String addr = ChordRouter.route(item.value);
                return 500;
            }
        }
        return 200;
    }

    protected void handleUrl(URL url) {
        collector.emit(new Values<>(url.getHost(), url));
    }

    /**
     * A method for writing simple messages directly to the socket.  Meant for simple messages that don't require
     * a body (e.g. 400, 500, etc.)
     */
    private void writeResponse(Socket client, int code) {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(code);
        resp.setProto("HTTP/1.1");
        resp.addHeader("Connection", "close");
        resp.addHeader("Content-Length", "0");
        try {
            HttpUtil.encodeResponse(client.getOutputStream(), resp);
        } catch (IOException e) {
            // Give up if something goes badly
        }
    }
}
