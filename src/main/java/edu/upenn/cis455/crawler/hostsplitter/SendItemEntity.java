package edu.upenn.cis455.crawler.hostsplitter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

/**
 * Created by Alex on 4/26/18
 */
@Entity
public class SendItemEntity {


    public enum MsgType {URL, ROUTING}

    @JsonIgnore
    @PrimaryKey(sequence = "ID")
    long id;  // The PK, which is only used internally by Berkeley

    // Everything is public so that Jackson can figure out how to set/serialize fields
    @SecondaryKey(relate = Relationship.MANY_TO_ONE)
    public String addr;  // The intermediate key for MapReduce (many values map to one key)

    public MsgType type;  // The type of the message (e.g. URL, ROUTING, used for multiplexing RPCs to handlers)
    public String value;  // Whatever the actual thing being worked on is (e.g. a URL)


    // Default constructor needed for Jackson deserialization
    public SendItemEntity() {
    }

    SendItemEntity(MsgType type, String addr, String s) {
        this.type = type;
        this.addr = addr;
        this.value = s;
    }
}
