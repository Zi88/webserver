package edu.upenn.cis455.crawler.hostsplitter;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.documentprocessing.DocumentProcessorBolt;
import edu.upenn.cis455.crawler.documentprocessing.Processor;
import edu.upenn.cis455.crawler.pagetypes.Page;
import edu.upenn.cis455.crawler.utils.BufferedQueue;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Alex on 4/26/18
 */
public class HostSplitterBolt implements IRichBolt {

    private static final Logger LOG = LogManager.getLogger(DocumentProcessorBolt.class);
    private Fields fields = new Fields("Host", "URL");      // The names of the objects that this bolt emits
    private String executorId = "HostSplitterBolt-" + (UUID.randomUUID().toString());  // An ID for debugging
    private OutputCollector collector;
    private ChordRouter chordRouter;   // TODO all bolts should really share the same router with the same state.
                                        // Only reason it's not static is for the simulation
    
    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }

    @Override
    public void cleanup() {

    }

    /* Determine if a url belongs to this node.  If so, emit it to our filter.  If not, pass it to the sender. */
    @Override
    public void execute(Tuple input) {

        // Ask the router which node this URL belongs to
        URL url = (URL) input.getObjectByField("URL");
        String routeTo = chordRouter.route(url.getHost());

        // If the URL belongs to this node, emit it to our UrlFilterBolt
        if (routeTo.equals(Config.SELF_ADDR)) {
            collector.emit(new Values<>(url.getHost(), url));
        }
        // Otherwise, have the SendDaemon send the url to the appropriate node
        else {
            SendDaemon.sendUrl(routeTo, url);
        }
    }

    @Override
    public void prepare(Map<String, Object> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;

        try {
            chordRouter = new ChordRouter();
            chordRouter.init(Config.SELF_ADDR, Config.FINGERS_FILE);
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public void setRouter(IStreamRouter router) {
        collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return fields;
    }
}
