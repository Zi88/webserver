package edu.upenn.cis455.crawler.urlfiltering;

import edu.upenn.cis455.crawler.Config;
import edu.upenn.cis455.crawler.utils.Cache;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import edu.upenn.cis455.crawler.utils.DiskBackedCache;
import edu.upenn.cis455.storage.BerkeleyManager;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alex on 3/6/18
 */
public class DuplicateUrlEliminator {

    private static Cache<String, String> seenUrls = new DiskBackedCache<>(Config.DEFAULT_CACHE_SIZE);

    /**
     * @return a List of URLs that have not been placed in the set of seen URLs
     */
    public static synchronized List<URL> filter(List<URL> urls) {
        // Check if any of the URLs are in the cache.  If they are, filter them out
        List<URL> unseen = new LinkedList<>();
        for (URL url : urls) {
            if (!seenBefore(url)) {
                unseen.add(url);
            }
        }
        return unseen;
    }

    /**
     * @param url The URL to be tested
     * @return true if the given URL hasn't been placed in the set of seen URLs before.
     */
    public static synchronized boolean seenBefore(URL url) {
        String key = CrawlerUtils.generateUrlKey(url);
        // For eliminating duplicate URLs, we must count http://foo.com/ as distinct from http://foo.com because of redirects
        // Key generator removes trailing slashes for uniformity in the DB, so add it back in if necessary.
        key = (url.getPath().endsWith("/")) ? key + "/" : key;

        // This is really a hashset, but use a map because we implemented one

        if (seenUrls.get(key) == null) {
            seenUrls.put(key, key);
            return false;
        }
        return true;
    }
}
