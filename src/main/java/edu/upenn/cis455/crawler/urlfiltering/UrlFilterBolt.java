package edu.upenn.cis455.crawler.urlfiltering;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.crawler.crawling.Filterer;
import edu.upenn.cis455.crawler.interfaces.FiltererInterface;
import edu.upenn.cis455.crawler.utils.MercatorFrontier;

import java.net.URL;
import java.util.*;

/**
 * Created by Alex on 3/21/18
 */
public class UrlFilterBolt implements IRichBolt {

    private String executorId = "FilterBolt-" + UUID.randomUUID().toString();
    private Fields fields = new Fields("URL");  // Schema for the tuples that this bolt emits
    private OutputCollector collector;
    private MercatorFrontier<URL> frontier;
    private FiltererInterface filterer;

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }

    @Override
    public void cleanup() {}

    @Override
    public void execute(Tuple input) {
        // Receive a URL, pass it through the duplicate filter
        URL url = (URL) input.getObjectByField("URL");
        List<URL> unseenUrls = DuplicateUrlEliminator.filter(Collections.singletonList(url));


        // TODO when distributed, we would place this in some kind of collector that batches, then shuffles values
        // Enqueue all URLs that don't get prefiltered based on TLD, depth, etc.
        for (URL u : unseenUrls) {
            if (!filterer.prefilter(u)) {
                frontier.enqueue(u);
            }
        }

        // TODO this is how we would do it if we weren't using a shared frontier
        // Emit the URL to somewhere where it can be enqueued
//        for (URL unseen : unseenUrls) {
//            collector.emit(new Values<>(unseen));
//        }
    }

    @Override
    public void prepare(Map<String, Object> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        MercatorFrontier f = (MercatorFrontier) stormConf.get("Frontier");
        this.frontier = f;
        filterer = new Filterer();
    }

    @Override
    public void setRouter(IStreamRouter router) {
        this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return fields;
    }
}
