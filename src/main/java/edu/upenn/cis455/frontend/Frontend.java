package edu.upenn.cis455.frontend;

public class Frontend {
	
	public static String getMainPage() {
		
		String mainPgae = "<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">	\n" + 
				"<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n" + 
				"<!-- jQuery library -->\n" + 
				"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n" + 
				"<!-- Latest compiled JavaScript -->\n" + 
				"<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n" + 
				"<style>\n" + 
				".logo{\n" + 
				"	margin-top: 280px\n" + 
				"}\n" + 
				"#logo{\n" + 
				"	font-size: 60px;\n" + 
				"	color: #4682b4;\n" + 
				"}\n" + 
				"#searchBar{\n" + 
				"	margin-top: 20px;\n" + 
				"	height: 50px;\n" + 
				"	box-shadow: 1px 1px 1px #DCDCDC;\n" + 
				"}\n" + 
				"#searchPanel{\n" + 
				"	width: 730px\n" + 
				"}\n" + 
				"#searchButton{\n" + 
				"	margin-top: 20px;\n" + 
				"	height: 50px;\n" + 
				"	width: 80px;\n" + 
				"	box-shadow: 1px 1px 1px #DCDCDC;\n" + 
				"}\n" + 
				"#seachSpan{\n" + 
				"	z-index: 10;\n" + 
				"}\n" + 
				"#searchButton {\n" + 
				"	background: #4682b4;\n" + 
				"    color: white;\n" + 
				"    font-size: 23px;\n" + 
				"    border: 1px solid grey;\n" + 
				"    border-left: none;\n" + 
				"    cursor: pointer;\n" + 
				"}\n" + 
				"#searchButton:hover {\n" + 
				"    background: #2196F3;\n" + 
				"}\n" + 
				"</style>\n" + 
				"<script>\n" + 
				"$(document).ready(function() {\n" + 
				"    $(\"#searchForm\").submit(function(e){\n" + 
				"    	var query = document.getElementById('searchBar').value.trim()\n" + 
				"    	if (query === null || query === \"\"){\n" + 
				"    		e.preventDefault(e);\n" + 
				"    	}\n" + 
				"        document.getElementById('query').value = query\n" + 
				"    });\n" + 
				"});\n" + 
				"</script>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<div class=\"main\">\n" + 
				"		<div class=\"row logo\" style=\"text-align:center\">\n" + 
				"			<h1 id=\"logo\">\n" + 
				"				AskAndreas<span class=\"glyphicon glyphicon-thumbs-up\"></span>\n" + 
				"			</h1>\n" + 
				"		</div>\n" + 
				"		<div class=\"row\">\n" + 
				"		  <div class=\"col-lg-8 col-sm-offset-3\">\n" + 
				"		  	<form id=\"searchForm\" action=\"search\" method=\"GET\">\n" + 
				"		    <div class=\"input-group\" id=\"searchPanel\">\n" + 
				"			      <input id=\"searchBar\" type=\"text\" class=\"form-control\" placeholder=\"Search for...\" value=\"\">\n" + 
				"			      <input type=\"hidden\" id=\"query\" name=\"query\" value=\"\">\n" + 
				"			      <span id=\"seachSpan\" class=\"input-group-btn\">\n" + 
				"	<!-- 		        <button id=\"searchButton\" class=\"btn btn-default\" type=\"button\"></button> -->\n" + 
				"			        <button id=\"searchButton\" type=\"submit\"><i class=\"fa fa-search\"></i></button>\n" + 
				"			      </span>\n" + 
				"		    </div>\n" + 
				"		    </form>\n" + 
				"		  </div>\n" + 
				"		</div>\n" + 
				"</div>\n" + 
				"</body>\n" + 
				"</html>";
		return mainPgae;
	}
	
	
	public static String getTop() {
		String top = "<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">	\n" + 
				"<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n" + 
				"<!-- jQuery  -->\n" + 
				"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n" + 
				"<!-- JavaScript -->\n" + 
				"<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>";
		return top;
	}
	
	public static String getCss() {
		String css = "<style>\n" + 
				"#top{\n" + 
				"	background-color: #fafafa;\n" + 
				"	box-shadow: 1px 1px 1px #DCDCDC;\n" + 
				"}\n" + 
				"#topbar{\n" + 
				"	width:100%;\n" + 
				"	height:90px;\n" + 
				"/*	text-align: center;*/\n" + 
				"}\n" + 
				"#logoDiv{\n" + 
				"	float: right;\n" + 
				"/*	margin : 0;*/\n" + 
				"	position: relative;\n" + 
				"	margin-right: 50px\n" + 
				"}\n" + 
				"#searchFormDiv{\n" + 
				"	float: left;\n" + 
				"	margin-top : 20px;\n" + 
				"	margin-left : 130px;\n" + 
				"	width:40%;\n" + 
				"	position: absolute;\n" + 
				"}\n" + 
				"#logo{\n" + 
				"	font: arial;\n" + 
				"	font-size: 30px;\n" + 
				"	padding: 6px;\n" + 
				"	color: #4682b4\n" + 
				"}\n" + 
				"#searchBar{\n" + 
				"	height: 40px;\n" + 
				"}\n" + 
				"#seachSpan{\n" + 
				"	z-index: 10;\n" + 
				"}\n" + 
				"#searchButton{\n" + 
				"	height: 40px;\n" + 
				"	width: 80px;\n" + 
				"}\n" + 
				"#searchButton {\n" + 
				"    background: #4682b4;\n" + 
				"    color: white;\n" + 
				"    font-size: 23px;\n" + 
				"    border: 1px solid grey;\n" + 
				"    border-left: none;\n" + 
				"    cursor: pointer;\n" + 
				"}\n" + 
				"#searchButton:hover {\n" + 
				"    background: #0b7dda;\n" + 
				"}\n" + 
				".item{\n" + 
				"	margin-left: 140px;\n" + 
				"}\n" + 
				".content{\n" + 
				"	max-width:48em; \n" + 
				"	font-size: 13px\n" + 
				"}\n" + 
				".link{\n" + 
				"/*	font-size: 10px*/\n" + 
				"	margin-bottom: 3px\n" + 
				"}\n" + 
				".head{\n" + 
				"	max-width:48em; \n" + 
				"}\n" + 
				".linkText{\n" + 
				"	font-size:18px;\n" + 
				"	font-family: Arial;\n" + 
				"	color: #4682b4\n" + 
				"}\n" + 
				".url{\n" + 
				"	font-size:14px; \n" + 
				"	color: darkgreen;\n" + 
				"	max-width:40em; \n" + 
				"}\n" + 
				".stats{\n" + 
				"	margin-left:140px; \n" + 
				"	margin-top:15px;\n" + 
				"	color: #808080\n" + 
				"}\n" + 
				"#pages{\n" + 
				"	margin-left: 140px;\n" + 
				"}\n" + 
				"#bottom{\n" + 
				"	height:60px;\n" + 
				"	background-color: #fafafa;\n" + 
				"	box-shadow: -1px -1px -1px #DCDCDC;	\n" + 
				"}\n" + 
				".pagination .active a:hover,\n" + 
				".pagination .active a {\n" + 
				"    background-color: #DCDCDC;\n" + 
				"/*    color: #000000;*/\n" + 
				"}\n" + 
				".pagination li a {\n" + 
				"    margin-right: 15px;\n" + 
				"    color: #808080;\n" + 
				"}\n" + 
				"#weatherDiv{\n" + 
				"/*	display: none;*/\n" + 
				"	margin-top: 19px;\n" + 
				"	width: 50% !important; \n" + 
				"	margin-left: 140px;\n" + 
				"	max-width:40em; \n" + 
				"}\n" + 
				"#weatherDiv p{\n" + 
				"	color: #2F4F4F;\n" + 
				"	font-size: 18px\n" + 
				"}\n" + 
				".img-responsive{\n" + 
				"	width:18%;\n" + 
				"}\n" + 
				"#weatherTable td, #weatherTable th {\n" + 
				"    border: none;\n" + 
				"}\n" + 
				"#weatherTable thead th{\n" + 
				"	color: #008B8B;\n" + 
				"}\n" + 
				".btn{\n" + 
				"	background-color: white;\n" + 
				"	border: none;\n" + 
				"	color: #008B8B\n" + 
				"}\n" + 
				".dropdown-menu a:hover{\n" + 
				"	cursor: pointer;\n" + 
				"}\n" + 
				".liked{\n" + 
				"	pointer-events: none;\n" + 
				"	background-color: lightgrey;\n" + 
				"}\n" + 
				"</style>";
		return css;
	}
	
	public static String getJs() {
		String js = "<script>\n" + 
				"$(document).ready(function() {\n" + 
				"    if ($('#weatherDiv').length){\n" + 
				"        console.log('Found with Length');\n" + 
				"        console.log(document.getElementById('weatherCity').value)\n" + 
				"        getWeather()\n" + 
				"    }\n" + 
				"});\n" + 
				"$(document).ready(function() {\n" + 
				"    $(\"#searchForm\").submit(function(e){\n" + 
				"    	var query = document.getElementById('searchBar').value.trim()\n" + 
				"    	if (query === null || query === \"\"){\n" + 
				"    		e.preventDefault(e);\n" + 
				"    	}\n" + 
				"        document.getElementById('query').value = query\n" + 
				"    });\n" + 
				"});\n" + 
				"$(document).on(\"click\", \".like\", function() {\n" + 
				"    console.log($(this).text())\n" + 
				"    console.log($(this).text())\n" + 
				"    if ($(this).text() == \"Already Liked\") return\n" + 
				"    var vote = 1\n" + 
				"    var url = $(this).parents('div.item').find('.linkText').attr('href');\n" + 
				"    console.log(vote)\n" + 
				"    console.log(url)\n" + 
				"    var that = $(this)\n" + 
				"    var asyncRequest = \"ajax?url=\" + url + \"&vote=\" + vote\n" + 
				"    $.get(asyncRequest, function(responseText) {\n" + 
				"    	console.log($(this).text())\n" + 
				"	    $(that).text(\"Already Liked\")\n" + 
				"	    console.log($(this).text())\n" + 
				"	    $(that).addClass(\"liked\");    	\n" + 
				"        console.log(responseText)\n" + 
				"    });\n" + 
				"});	\n" + 
				"$(document).on(\"click\", \".dislike\", function() {\n" + 
				"    $(this).parents('div.item').fadeOut();  \n" + 
				"    var vote = 0\n" + 
				"    var url = $(this).parents('div.item').find('.linkText').attr('href');\n" + 
				"    console.log(vote)\n" + 
				"    console.log(url)\n" + 
				"    var asyncRequest = \"ajax?url=\" + url + \"&vote=\" + vote\n" + 
				"    $.get(asyncRequest, function(responseText) {\n" + 
				"        console.log(responseText)\n" + 
				"    });\n" + 
				"});	\n" + 
				"function getWeather(){\n" + 
				"	var city = document.getElementById('weatherCity').value\n" + 
				"    $.getJSON(\"http://api.openweathermap.org/data/2.5/forecast?q=\" + city + \"&APPID=f273695ada9aced5d020561399830400\",function(json){\n" + 
				"        document.querySelector(\"#weatherDiv p\").innerHTML = \"Weather forecast for \" + document.getElementById('weatherCity').value;\n" + 
				"        document.querySelector(\"#weatherDiv thead th:nth-child(2)\").innerHTML = \"Temperature\";\n" + 
				"        document.querySelector(\"#weatherDiv thead th:nth-child(3)\").innerHTML = \"Weather\";\n" + 
				"        // $(\"#icon\").html(\"<img src='http://openweathermap.org/img/w/\" + \"03n\" + \".png' alt=''>\");\n" + 
				"        var days = json['list'].length\n" + 
				"        console.log(days)\n" + 
				"        var dateInx = 0\n" + 
				"        var curDate = null\n" + 
				"        var curLow = null\n" + 
				"        var curHigh = null\n" + 
				"        var icon\n" + 
				"        for (var key in json['list']){\n" + 
				"        	item = json['list'][key]\n" + 
				"        	// console.log(item)\n" + 
				"        	testDate = item['dt_txt'].split(\" \")[0]\n" + 
				"        	temp = item['main']['temp']\n" + 
				"        	icon = item['weather'][0]['icon']\n" + 
				"        	if (curDate == null || curDate != testDate){\n" + 
				"        		var icon = item['weather'][0]['icon']\n" + 
				"        		if (curDate != null){\n" + 
				"        			document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(1)\").innerHTML = curDate;\n" + 
				"        			var tempRange = (curLow-273.15).toFixed(2) + \"°C\" + \" -> \" + (curHigh-273.15).toFixed(2) + \"°C\"\n" + 
				"        			document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(2)\").innerHTML = tempRange;\n" + 
				"        			$(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(3)\").html(\"<img src='http://openweathermap.org/img/w/\" + icon + \".png' alt=''>\")\n" + 
				"        		}\n" + 
				"        		dateInx += 1\n" + 
				"        		curDate = testDate\n" + 
				"        		curLow = temp\n" + 
				"        		curHigh = temp\n" + 
				"        	} else{\n" + 
				"        		if (temp < curLow){\n" + 
				"        			curLow = temp\n" + 
				"        		} \n" + 
				"        		if (temp > curHigh){\n" + 
				"        			curHigh = temp\n" + 
				"        		}\n" + 
				"        	}\n" + 
				"        }\n" + 
				"		document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(1)\").innerHTML = curDate;\n" + 
				"		var tempRange = (curLow-273.15).toFixed(2) + \"°C\" + \" -> \" + (curHigh-273.15).toFixed(2) + \"°C\"\n" + 
				"		document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(2)\").innerHTML = tempRange;\n" + 
				"		$(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(3)\").html(\"<img src='http://openweathermap.org/img/w/\" + \"03n\" + \".png' alt=''>\")        \n" + 
				"    });\n" + 
				"}\n" + 
				"</script>\n" + 
				"</head>\n" + 
				"<body>";
		return js;
	}
	
	
	public static String getSearchBar(String query) {
		String search = "<div id=\"top\" class=\"container-fluid\">\n" + 
				"        <div id=\"topbar\">\n" + 
				"        	<div id=\"logoDiv\">\n" + 
				"	        	<h1 id=\"logo\">\n" + 
				"				AskAndreas<span class=\"glyphicon glyphicon-thumbs-up\"></span>" + 
				"				</h1>\n" + 
				"			</div>\n" + 
				"			<div id=\"searchFormDiv\">\n" + 
				"			  	<form id=\"searchForm\" action=\"search\" method=\"GET\" onsubmit=sendQuery()>\n" + 
				"				    <div class=\"input-group\" id=\"searchPanel\">\n" + 
				"					      <input id=\"searchBar\" type=\"text\" class=\"form-control\" placeholder=\"\" value=\"" + query + "\">\n" + 
				"					      <input type=\"hidden\" id=\"query\" name=\"query\" value=\"\">\n" + 
				"					      <span class=\"input-group-btn\">\n" + 
				"					        <button id=\"searchButton\" type=\"submit\"><i class=\"fa fa-search\"></i></button>\n" + 
				"					      </span>\n" + 
				"				    </div>\n" + 
				"			    </form>\n" + 
				"			</div>\n" + 
				"        </div>\n" + 
				"</div>";
		return search;
	}
	
//	public static String getSearchPageTop(String query, int resultsNum, long timeLast) {
//		String page = "<!DOCTYPE html>\n" + 
//				"<html>\n" + 
//				"<head>\n" + 
//				"<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">	\n" + 
//				"<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n" + 
//				"<!-- jQuery  -->\n" + 
//				"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n" + 
//				"<!-- JavaScript -->\n" + 
//				"<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n" + 
//				"<style>\n" + 
//				"#top{\n" + 
//				"	background-color: #fafafa\n" + 
//				"}\n" + 
//				"#topbar{\n" + 
//				"	width:100%;\n" + 
//				"	height:90px;\n" + 
//				"/*	text-align: center;*/\n" + 
//				"}\n" + 
//				"#logoDiv{\n" + 
//				"	float: left;\n" + 
//				"/*	margin : 0;*/\n" + 
//				"	position: absolute;\n" + 
//				"/*	margin-left: 800px*/\n" + 
//				"}\n" + 
//				"#searchFormDiv{\n" + 
//				"	float: left;\n" + 
//				"	margin-top : 20px;\n" + 
//				"	margin-left : 130px;\n" + 
//				"	width:40%;\n" + 
//				"	position: absolute;\n" + 
//				"}\n" + 
//				"#logo{\n" + 
//				"	font: arial;\n" + 
//				"	font-size: 25px;\n" + 
//				"	padding: 6px;\n" + 
//				"}\n" + 
//				"#searchBar{\n" + 
//				"	height: 40px;\n" + 
//				"}\n" + 
//				"#searchButton{\n" + 
//				"	height: 40px;\n" + 
//				"}\n" + 
//				"#searchButton {\n" + 
//				"    background: #2196F3;\n" + 
//				"    color: white;\n" + 
//				"    font-size: 23px;\n" + 
//				"    border: 1px solid grey;\n" + 
//				"    border-left: none;\n" + 
//				"    cursor: pointer;\n" + 
//				"}\n" + 
//				"#searchButton:hover {\n" + 
//				"    background: #0b7dda;\n" + 
//				"}\n" + 
//				".item{\n" + 
//				"	margin-left: 140px;\n" + 
//				"}\n" + 
//				".content{\n" + 
//				"	max-width:48em; \n" + 
//				"	font-size: 13px\n" + 
//				"}\n" + 
//				".link{\n" + 
//				"/*	font-size: 10px*/\n" + 
//				"	margin-bottom: 3px\n" + 
//				"}\n" + 
//				".head{\n" + 
//				"	max-width:48em; \n" + 
//				"}\n" + 
//				".linkText{\n" + 
//				"	font-size: 19px\n" + 
//				"}\n" + 
//				".url{\n" + 
//				"	font-size:14px; \n" + 
//				"	color: darkgreen\n" + 
//				"}\n" + 
//				".stats{\n" + 
//				"	margin-left:140px; \n" + 
//				"	margin-top:10px;\n" + 
//				"	color: #808080\n" + 
//				"}\n" + 
//				"#pages{\n" + 
//				"	margin-left: 140px;\n" + 
//				"}\n" + 
//				"#bottom{\n" + 
//				"	height:60px;\n" + 
//				"	background-color: #fafafa	\n" + 
//				"}\n" + 
//				".pagination .active a:hover,\n" + 
//				".pagination .active a {\n" + 
//				"    background-color: #DCDCDC;\n" + 
//				"/*    color: #000000;*/\n" + 
//				"}\n" + 
//				".pagination li a {\n" + 
//				"    margin-right: 15px;\n" + 
//				"    color: #808080;\n" + 
//				"}\n" + 
//				"#weatherDiv{\n" + 
//				"/*	display: none;*/\n" + 
//				"	margin-top: 19px;\n" + 
//				"	width: 50% !important; \n" + 
//				"	margin-left: 140px;\n" + 
//				"	max-width:40em; \n" + 
//				"}\n" + 
//				"#weatherDiv p{\n" + 
//				"	color: #2F4F4F;\n" + 
//				"	font-size: 18px\n" + 
//				"}\n" + 
//				".img-responsive{\n" + 
//				"	width:18%;\n" + 
//				"}\n" + 
//				"#weatherTable td, #weatherTable th {\n" + 
//				"    border: none;\n" + 
//				"}\n" + 
//				"#weatherTable thead th{\n" + 
//				"	color: #008B8B;\n" + 
//				"}\n" + 
//				"</style>\n" + 
//				"<script>\n" + 
//				"$(document).ready(function() {\n" + 
//				"    if ($('#weatherDiv').length){\n" + 
//				"        console.log('Found with Length');\n" + 
//				"        console.log(document.getElementById('weatherCity').value)\n" + 
//				"        getWeather()\n" + 
//				"    }\n" + 
//				"});	\n" + 
//				
//				"$(document).on(\"click\", \"#ajaxBut\", function() { // When HTML DOM \"click\" event is invoked on element with ID \"somebutton\", execute the following function...\n" + 
//				"    $.get(\"ajax?ajsx=someajax\", function(responseText) {   // Execute Ajax GET request on URL of \"someservlet\" and execute the following function with Ajax response text...\n" + 
//				"        console.log(responseText)\n" + 
//				"        // $(\"#somediv\").text(responseText);           // Locate HTML DOM element with ID \"somediv\" and set its text content with the response text.\n" + 
//				"    });\n" + 
//				"});	" + 
//				
//				"function sendQuery() {\n" + 
//				"	document.getElementById('query').value = document.getElementById('searchBar').value;;\n" + 
//				"}\n" + 
//				"function getWeather(){\n" + 
//				"	var city = document.getElementById('weatherCity').value\n" + 
//				"    $.getJSON(\"http://api.openweathermap.org/data/2.5/forecast?q=\" + city + \"&APPID=f273695ada9aced5d020561399830400\",function(json){\n" + 
//				"        document.querySelector(\"#weatherDiv p\").innerHTML = \"Weather forecast for \" + document.getElementById('weatherCity').value;\n" + 
//				"        document.querySelector(\"#weatherDiv thead th:nth-child(2)\").innerHTML = \"Temperature\";\n" + 
//				"        document.querySelector(\"#weatherDiv thead th:nth-child(3)\").innerHTML = \"Weather\";\n" + 
//				"        // $(\"#icon\").html(\"<img src='http://openweathermap.org/img/w/\" + \"03n\" + \".png' alt=''>\");\n" + 
//				"        var days = json['list'].length\n" + 
//				"        console.log(days)\n" + 
//				"        var dateInx = 0\n" + 
//				"        var curDate = null\n" + 
//				"        var curLow = null\n" + 
//				"        var curHigh = null\n" + 
//				"        var icon\n" + 
//				"        for (var key in json['list']){\n" + 
//				"        	item = json['list'][key]\n" + 
//				"        	// console.log(item)\n" + 
//				"        	testDate = item['dt_txt'].split(\" \")[0]\n" + 
//				"        	temp = item['main']['temp']\n" + 
//				"        	icon = item['weather'][0]['icon']\n" + 
//				"        	if (curDate == null || curDate != testDate){\n" + 
//				"        		var icon = item['weather'][0]['icon']\n" + 
//				"        		if (curDate != null){\n" + 
//				"        			document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(1)\").innerHTML = curDate;\n" + 
//				"        			var tempRange = (curLow-273.15).toFixed(2) + \"°C\" + \" -> \" + (curHigh-273.15).toFixed(2) + \"°C\"\n" + 
//				"        			document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(2)\").innerHTML = tempRange;\n" + 
//				"        			$(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(3)\").html(\"<img src='http://openweathermap.org/img/w/\" + icon + \".png' alt=''>\")\n" + 
//				"        		}\n" + 
//				"        		dateInx += 1\n" + 
//				"        		curDate = testDate\n" + 
//				"        		curLow = temp\n" + 
//				"        		curHigh = temp\n" + 
//				"        	} else{\n" + 
//				"        		if (temp < curLow){\n" + 
//				"        			curLow = temp\n" + 
//				"        		} \n" + 
//				"        		if (temp > curHigh){\n" + 
//				"        			curHigh = temp\n" + 
//				"        		}\n" + 
//				"        	}\n" + 
//				"        }\n" + 
//				"		document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(1)\").innerHTML = curDate;\n" + 
//				"		var tempRange = (curLow-273.15).toFixed(2) + \"°C\" + \" -> \" + (curHigh-273.15).toFixed(2) + \"°C\"\n" + 
//				"		document.querySelector(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(2)\").innerHTML = tempRange;\n" + 
//				"		$(\"#weatherDiv tbody tr:nth-child(\" + dateInx + \") td:nth-child(3)\").html(\"<img src='http://openweathermap.org/img/w/\" + \"03n\" + \".png' alt=''>\")        \n" + 
//				"    });\n" + 
//				"}\n" + 
//				"</script>\n" + 
//				"</head>\n" + 
//				"<body>	\n" + 
//				"<div id=\"top\" class=\"container-fluid\">\n" + 
//				"        <div id=\"topbar\">\n" + 
//				"        	<div id=\"logoDiv\">\n" + 
//				"	        	<h1 id=\"logo\">\n" + 
//				"					AskAhea\n" + 
//				"				</h1>\n" + 
//				"			</div>\n" + 
//				"			<div id=\"searchFormDiv\">\n" + 
//				"			  	<form id=\"searchForm\" action=\"search\" method=\"GET\" onsubmit=sendQuery()>\n" + 
//				"				    <div class=\"input-group\" id=\"searchPanel\">\n" + 
//				"					      <input id=\"searchBar\" type=\"text\" class=\"form-control\" placeholder=\"\" value=\"" + query + "\">\n" + 
//				"					      <input type=\"hidden\" id=\"query\" name=\"query\" value=\"\">\n" + 
//				"					      <span class=\"input-group-btn\">\n" + 
//				"					        <button id=\"searchButton\" type=\"submit\"><i class=\"fa fa-search\"></i></button>\n" + 
//				"					      </span>\n" + 
//				"				    </div>\n" + 
//				"			    </form>\n" + 
//				"			</div>\n" + 
//				"        </div>\n" + 
//				"</div>\n" + 
//				"<div class=\"main\">\n" + 
//				"	<div class=\"search\">\n" + 
//				"		<div class=\"stats\">\n" + 
//				"			<p>9999999 results in 0.00001 seconds</p>\n" + 
//				"		</div>";
//		return page;
//	}
	
	public static String getStats(int reusltsNum, long sec) {
		String stats = "<div class=\"main\">\n" + 
				"	<div class=\"search\">\n" + 
				"		<div class=\"stats\">\n" + 
				"			<p>" + reusltsNum + " results in " + Long.toString(sec) + " seconds</p>\n" + 
				"		</div>";
		return stats;
	}

	public static String getWeatherDiv(String city) {
		String div = "		<div id=\"weatherDiv\">\n" + 
				"			<input id=\"weatherCity\" type=\"hidden\" value=\"" + city + "\">\n" + 
				"			<br><p></p>\n" + 
				"			<table class=\"table borderless\" id=\"weatherTable\">\n" + 
				"			  <thead>\n" + 
				"			    <tr>\n" + 
				"			      <th scope=\"col\"></th>\n" + 
				"			      <th scope=\"col\"></th>\n" + 
				"			      <th scope=\"col\"></th>\n" + 
				"			    </tr>\n" + 
				"			  </thead>\n" + 
				"			  <tbody>\n" + 
				"			    <tr>\n" + 
				"			      <td scope=\"row\"></th>\n" + 
				"			      <td ></td>\n" + 
				"			      <td ></td>\n" + 
				"			    </tr>\n" + 
				"			    <tr>\n" + 
				"			      <td scope=\"row\"></th>\n" + 
				"			      <td></td>\n" + 
				"			      <td></td>\n" + 
				"			    </tr>\n" + 
				"			    <tr>\n" + 
				"			      <td scope=\"row\"></th>\n" + 
				"			      <td></td>\n" + 
				"			      <td></td>\n" + 
				"			    </tr>\n" + 
				"			    <tr>\n" + 
				"			      <td scope=\"row\"></th>\n" + 
				"			      <td></td>\n" + 
				"			      <td></td>\n" + 
				"			    </tr>\n" + 
				"			    <tr>\n" + 
				"			      <td scope=\"row\"></th>\n" + 
				"			      <td></td>\n" + 
				"			      <td></td>\n" + 
				"			    </tr>			    			    \n" + 
				"			  </tbody>\n" + 
				"			</table>\n" + 
				"		</div>	";
		return div;
	}	
	
	public static String getSearchItem(String url, String linkTest, String description){
		linkTest = linkTest == null? url: linkTest;
		description = description == null? "sorry preview is not avaiable, please check the link " + url + " for more details" : description;
//		if (linkTest == null || description == null) {
//			String item = "		<div class=\"item\">\n" + 
//					"			<div class=\"head\">\n" + 
//					"			<h3 class=\"link\">\n" + 
//					"				<a class=\"linkText\" href=\"" + url + "\">" + url + "</a>\n" + 
//					"			</h3>\n" + 
//					"			</div>\n" + 
//					"			<div class=\"content\">\n" + 
//					"				<div>\n" + 
//					"					<div class=\"url\">" + url + "</div>\n" + 
//					"				</div>\n" + 
//					"			</div>\n" + 					
//					"		</div><br>	";
//			return item;
//		}
//		String item = "		<div class=\"item\">\n" + 
//				"			<div class=\"head\">\n" + 
//				"			<h3 class=\"link\">\n" + 
//				"				<a class=\"linkText\" href=\"" + url + "\">" + linkTest + "</a>\n" + 
//				"			</h3>\n" + 
//				"			</div>\n" + 				
//				"			<div class=\"content\">\n" + 
//				"				<div>\n" + 
//				"					<div class=\"url\">" + url + "</div>\n" + 
//				"					<span class=\"description\">" + description + "\n" + 
//				"					</span>\n" + 
//				"				</div>\n" + 
//				"			</div>\n" + 
//				"		</div><br>";
		
		String item = "		<div class=\"item\">\n" + 
		"			<div class=\"head\">\n" + 
		"			<h3 class=\"link\">\n" + 
		"				<a class=\"linkText\" href=\"" + url + "\">" + linkTest + "</a>\n" + 
		"			</h3>\n" + 
		"			</div>\n" + 
		"			<div class=\"content\">\n" + 
		"					<div class=\"url\">" + url + "\n" + 
		"						<div class=\"btn-group\" class=\"preference\">\n" + 
		"						  <button type=\"button\" class=\"btn btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" + 
		"						    <span class=\"glyphicon glyphicon-arrow-down\"></span>\n" + 
		"						  </button>\n" + 
		"						 <ul class=\"dropdown-menu\">\n" + 
		"					      <li><a class=\"like\">Like</a></li>\n" + 
		"					      <li><a class=\"dislike\">Dislike</a></li>\n" + 
		"					    </ul>\n" + 
		"						</div>\n" + 
		"					</div>\n" + 
		"					<span class=\"description\">" + description + "\n" +  
		"					</span>\n" + 
		"			</div>\n" + 
		"		</div><br>";
		return item;
	}	
	
	public static String getAfterResults() {
		String page = "	</div>			\n" + 
				"</div>\n" + 
				"<div id=\"pages\">\n" + 
				"	<nav aria-label=\"Page navigation example\">\n" + 
				"	<input type=\"hidden\" id=\"pageId\" name=\"pageNum\" value=\"\">\n" + 
				"	  <ul class=\"pagination\">";
		return page;
	}
	
	public static String getSearchPageBottom() {
		String page = "	  </ul>\n" + 
				"	</nav>\n" + 
				"</div>\n" + 
				"<div class=\"container-fluid\" id=\"bottom\">\n" + 
				"</div>\n" + 
				"</body>\n" + 
				"</html>";
		return page;
	}
	

	
	public static String getPagination(String query, int pagenum, int curPage) {
		String item;
		// active
		if (curPage == pagenum) {
			item = "<li class=\"active\" class=\"page-item\"><a class=\"page-link\">" + pagenum + "</a></li>";
		}
		// not active
		else {
			item = "<li class=\"page-item\"><a class=\"page-link\" href=\"search?query=" + query + "&page=" + pagenum + "\">" + pagenum + "</a></li>";
		}
		return item;
	}
	
	
}
