package edu.upenn.cis455.frontend;

import java.util.List;

public class Page{
	public String page;
	public List<Result> results;
	public int pageNum;
	public Page(int pageNum, List<Result> results, String query, int resultsNum, int totalPageNum, long timeLast) {
		this.results = results;
		this.pageNum = pageNum;
		StringBuffer sb = new StringBuffer();
		sb.append(Frontend.getTop());
		sb.append(Frontend.getCss());
		sb.append(Frontend.getJs());
		sb.append(Frontend.getSearchBar(query));
		sb.append(Frontend.getStats(resultsNum, timeLast));
		for (int i = 0; i < results.size(); i++) {
			Result curRes = results.get(i);
			sb.append(Frontend.getSearchItem(curRes.url, curRes.link, curRes.description));
		}
		sb.append(Frontend.getAfterResults());
		for (int i = 0; i < totalPageNum; i++) {
			sb.append(Frontend.getPagination(query, i+1, pageNum));
		}
		sb.append(Frontend.getSearchPageBottom());
		this.page = sb.toString();
	}
	
	public Page(int pageNum, List<Result> results, String query, int resultsNum, int totalPageNum, long timeLast, String city) {
		this.results = results;
		this.pageNum = pageNum;
		StringBuffer sb = new StringBuffer();
		sb.append(Frontend.getTop());
		sb.append(Frontend.getCss());
		sb.append(Frontend.getJs());
		sb.append(Frontend.getSearchBar(query));
		sb.append(Frontend.getStats(resultsNum, timeLast));
		sb.append(Frontend.getWeatherDiv(city));
		// assume add 10 of the above search result
		for (int i = 0; i < results.size(); i++) {
			Result curRes = results.get(i);
			sb.append(Frontend.getSearchItem(curRes.url, curRes.link, curRes.description));
		}
		
		sb.append(Frontend.getAfterResults());
		for (int i = 0; i < totalPageNum; i++) {
			sb.append(Frontend.getPagination(query, i+1, pageNum));
		}
		sb.append(Frontend.getSearchPageBottom());
		this.page = sb.toString();
	}
	
	
}