package edu.upenn.cis455.frontend;
import Database.TFIDFScoreObject;
import PageRankGetter.PageRankAccesser;
import SearchEngine.SearchEngineIndexerHelpers;
import SearchEngine.SearchEngineServer;
import org.eclipse.jdt.core.search.SearchEngine;

import java.io.IOException;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class Test {

    public static void main(String args[]) throws IOException {
    	if (args.length < 2) {
    		System.out.println("please input the dir for indexer and pagerank");
    		return;
    	}
    	System.out.println("here");
        String query = "Coffee maker";
        SearchEngineServer.directory = "/Users/MertZ/Desktop/temp/dir2/";
        PriorityQueue<TFIDFScoreObject> pq = new PriorityQueue<TFIDFScoreObject>();
        PageRankAccesser accesser = new PageRankAccesser();
        accesser.init(args[1]);
        
        
        pq = SearchEngineIndexerHelpers.tokenizer(query, pq, args[0], accesser);

        while (pq.size() > 0) {
            TFIDFScoreObject obj = pq.poll();
            System.out.println(obj.url + "  " + obj.score);
        }

    }
}
