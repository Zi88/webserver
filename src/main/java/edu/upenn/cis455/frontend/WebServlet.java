package edu.upenn.cis455.frontend;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Database.TFIDFScoreObject;
import PageRankGetter.PageRankAccesser;
import SearchEngine.SearchEngineIndexerHelpers;
import cis555.SearchEngine.SearchEngine;
import cis555.SearchEngine.SearchEngineSingle;
import cis555.SearchEngine.UrlFinalScoreObject;


public class WebServlet extends HttpServlet {
	
	private Map<Integer, Page> pageMap = null;
	public SearchEngine engine = new SearchEngine();
	public SearchEngineSingle engineTest = new SearchEngineSingle();

	@Override
	public void init() {
		
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	}
	
    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	
		String URI = request.getRequestURI();
		System.out.println("test request: " + URI);
		PrintWriter pw = response.getWriter();
		String user = request.getRemoteAddr();
		
    		switch (URI) {
    			case "/":
    				System.out.println("main page");
    				renderMainPage(pw);
    				break;
    			case "/ajax":
    				System.out.println("ajax");
    				String url = request.getParameter("url");
    				String vote = request.getParameter("vote");
    				System.out.println("user:"+user);
    				System.out.println("test ajax||" + url + "||" + vote);
    				pw.print("user:" + user +"'s preference for " + url + "updated");
    				engineTest.addVote(user, url, vote);
    				break;
    			case "/search":
    				String query = request.getParameter("query");
    				String pageNum = request.getParameter("page");
    				String city = getWeatherCity(query);
    				System.out.println("test WeatherCity " + city);
    				System.out.println("test param query: " + query);
    				System.out.println("test param page: " + pageNum);
    				if (query == null || query.equals("")) break;
    				// new search request
    				
    				if (pageNum == null) {
    					pageMap = new HashMap<Integer, Page>();
    					PriorityQueue<UrlFinalScoreObject> pq = new PriorityQueue<>();
    					long start = System.currentTimeMillis();
    					pq = engineTest.run(query, user);
    					long end = System.currentTimeMillis();
    					long timeLast = (end - start);
    					System.out.println("timeLast: " + timeLast);
    					long secs = TimeUnit.MILLISECONDS.toSeconds(timeLast);
    					System.out.println("secs " + secs);
    					List<Result> results = getResults(pq);
    					
    					int pageIndex = 0;
    					int totalResults = results.size();
    					List<Result> cur = new LinkedList<>();
    					int totalPageNum = totalResults%10 == 0? totalResults/10:totalResults/10 + 1;
    					System.out.println("totalPageNum " + totalPageNum);
    					for (int i = 0; i < totalResults; i++) {
    						System.out.println(results.get(i).url);
    						cur.add(results.get(i));
    						if ((i+1)%10 == 0) {
    							pageIndex++;
    							if (pageIndex == 1 && city != null) {
    								pageMap.put(pageIndex, new Page(pageIndex, new LinkedList(cur), query, totalResults, totalPageNum, secs, city));
    							}
    							else{
    								pageMap.put(pageIndex, new Page(pageIndex, new LinkedList(cur), query, totalResults, totalPageNum, secs));
    							}
    							cur = new LinkedList<>();
    						}
    					}
    					if (cur.size() > 0) {
    						pageIndex++;
						if (pageIndex == 1 && city != null) {
							pageMap.put(pageIndex, new Page(pageIndex, new LinkedList(cur), query, totalResults, totalPageNum, secs, city));
						}else {
							pageMap.put(pageIndex, new Page(pageIndex, new LinkedList(cur), query, totalResults, totalPageNum, secs));
						}
    					}
    					System.out.println("test size " + pageMap.size() + " " + totalResults);
    					for (Map.Entry<Integer, Page> entry: pageMap.entrySet()) {
    						System.out.println(entry.getKey());
    						System.out.println(pageMap.get(entry.getKey()).results.size());
    					}
//    					search(pw, query, pageNum)
    					pw.print(pageMap.get(1).page);
    				} 
    				// jump page
    				else {
//    					search(pageNum);
    					pw.print(pageMap.get(Integer.parseInt(pageNum)).page);
    				}   				    				
//    				search(pw, query, pageNum);
    				break;
    		}
	}
    
    public String getWeatherCity(String query) {
    		query = query.toLowerCase();
    		if (!query.contains("weather")) return null;
    		String[] pieces = query.split(" ");
    		String res = "";
    		for (int i = 0; i < pieces.length; i++) {
    			String piece = pieces[i];
    			if (piece.equals("weather")) continue;
    			res = res + piece + " ";
    		}
    		return res.trim();
    }
    
    public void search(PrintWriter pw, String query, String pageNum) throws IOException {
//		PriorityQueue<TFIDFScoreObject> pq = new PriorityQueue<TFIDFScoreObject>();
//		pq = SearchEngineIndexerHelpers.tokenizer(query, pq, "resources/dir2", accesser);
//		List<Result> results = getResults(pq);
//		renderSearchPage(pw, query, pageNum, results);
    }
    
    
	// dynamically render page based on message
	public void renderMainPage(PrintWriter pw) {
		String page = Frontend.getMainPage();
		pw.print(page);
	}
	
	public void renderSearchPage(PrintWriter pw, String query, String pageNum, List<Result> results) throws IOException {
//		String page = Frontend.getMainPage();
		StringBuffer sb = new StringBuffer();
		// TO DO, grab back all the results according to query
		// now use some dummy data
		// assume we already have the url, text of the url and description
//		String url = "https://en.wikipedia.org/wiki/University_of_Pennsylvania";
//		String linkText = "University of Pennsylvania - Wikipedia";
//		String description = "The <em>University of Pennsylvania</em> (commonly known as <em>Penn</em> or <em>UPenn</em>) is a private Ivy League research <em>university</em> located in the <em>University</em> City section of Philadelphia.";
//		PriorityQueue<TFIDFScoreObject> pq = new PriorityQueue<TFIDFScoreObject>();
//		pq = SearchEngineIndexerHelpers.tokenizer(query, pq, "resources/dir2", accesser);
//		List<Result> results = getResults(pq);
//		List<Result> results = dummy();
//		Map<Integer, Page> pageMap = new HashMap<>();
//		int pageIndex = 0;
//		int totalResults = results.size();
//		List<Result> cur = new LinkedList<>();
//		for (int i = 0; i < totalResults; i++) {
//			cur.add(results.get(i));
//			if ((i+1)%10 == 0) {
//				pageIndex++;
//				pageMap.put(pageIndex, new Page(pageIndex, new LinkedList(cur), query));
//			}
//			cur = new LinkedList<>();
//		}
//		if (cur.size() > 0) pageMap.put(pageIndex++, new Page(pageIndex++, new LinkedList(cur), query));
		
//		sb.append(Frontend.getSearchPageTop(query, results.size()));
//		// assume add 10 of the above search result
//		for (int i = 0; i < results.size(); i++) {
//			Result curRes = results.get(i);
//			sb.append(Frontend.getSearchItem(curRes.url, curRes.link, curRes.description));
//		}
//		sb.append(Frontend.getPageBot(query));
//		pw.print(sb.toString());
	}
	
	public List<Result> getResults(PriorityQueue<UrlFinalScoreObject> pq){
		List<Result> res = new LinkedList<>();
        while (pq.size() > 0) {
        	UrlFinalScoreObject obj = pq.poll();
        		String url = obj.url;
        		double score = obj.score;
            res.add(0, new Result(url, null, null));
            System.out.println(url + "  " + score);
        }
        return res;
	}	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
