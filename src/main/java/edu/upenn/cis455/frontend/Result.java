package edu.upenn.cis455.frontend;

public class Result {

	public String url;
	public String link;
	public String description;
	public Result(String url, String link, String description) {
		this.url = url;
		this.link = link;
		this.description = description;
	}
	
}
