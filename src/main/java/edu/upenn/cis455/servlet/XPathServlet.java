package edu.upenn.cis455.servlet;

import edu.upenn.cis455.storage.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;

@SuppressWarnings("serial")
public class XPathServlet extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger(XPathServlet.class);
    private static final DateTimeFormatter ISO_FORMATTER = DateTimeFormatter.ofPattern("YYYY-MM-dd'T'HH:mm:ss");

    @Override
    public void init() throws ServletException {
        // Make sure Berkeley DB is initialized (may have already been done if crawler is running)
        String berkleyHome = getServletContext().getInitParameter("BDBstore");
        DBWrapper.initialize(berkleyHome);
        super.init();
    }

    @Override
    public void destroy() {
        DBWrapper.sync();
        DBWrapper.close();
    }

    /* You may want to override one or both of the following methods */

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
		switch (request.getRequestURI()) {
            case "/register":
                registerAccount(request, response);
                return;
            case "/login":
                attemptLogin(request, response);
                return;
            case "/logout":
                attemptLogout(request, response);
                return;
        }
        // Not found for a POST to any other page
        response.sendError(404);
	}

    private void attemptLogout(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Invalidate user session and redirect user to main page
        request.getSession().invalidate();
        response.sendRedirect("/");

    }

    private void attemptLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {

        // Get parameters
        String user = request.getParameter("username");
        String pass = request.getParameter("password");

        // Get UserEntity from DB (if it exists)
        UserEntity ue = DBWrapper.getUser(user);
        if (ue == null)
        {
            response.sendError(401);
            return;
        }

        // Compare the hashed passwords
        String providedHash;
        try
        {
            providedHash = new String(MessageDigest.getInstance("SHA-256").digest(pass.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            response.sendError(500);
            return;
        }

        // If successful, create a new session, add the UserEntity object to it, and redirect the user to the main page
        if (providedHash.equals(ue.getHashedPass()))
        {
            HttpSession sesh = request.getSession();
            sesh.setAttribute("userInfo", ue);
            sesh.setMaxInactiveInterval(5 * 60);  // 5 minute timeout
            response.sendRedirect("/");
            return;
        }
        response.sendError(403);
    }

    private void registerAccount(HttpServletRequest request, HttpServletResponse response) throws IOException
    {

        // Get the parameters
        String user = request.getParameter("username");
        String pass = request.getParameter("password");

        // Put the user info into the DB, or send a 403 if this user already exists
        UserEntity ue = DBWrapper.getUser(user);
        if (ue != null)
        {
            response.sendError(403);
            return;
        }

        // If successful (return the newly put UserEntity), redirect to the main page
        ue = DBWrapper.createUser(user, pass);
        if (ue != null)
        {
            response.sendRedirect("/");
            return;
        }

        // Unsuccessful due to DB error
        response.sendError(500);

    }

    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        switch (request.getRequestURI()) {
            // If at the main page, try to get the requestor's session.
            case "/":
                HttpSession sesh = request.getSession(false);
                if (sesh == null)
                {
                    // Let the user log in if no session exists
                    displayLoginForm(request, response);
                    return;
                }
                else
                {
                    // Display the user info if a session does exist
                    displayUserInfo(request, response, sesh);
                    return;
                }
            case "/lookup":
                lookupResource(request, response);
                return;
            case "/create":
                createChannel(request, response);
                return;
            case "/delete":
                deleteChannel(request, response);
                return;
            case "/subscribe":
                subscribe(request, response);
                return;
            case "/unsubscribe":
                unsubscribe(request, response);
                return;
            case "/show":
                showChannel(request, response);
                return;
            case "/logout":
                attemptLogout(request, response);
                return;
        }
        // Not found for a GET to any other path
        response.sendError(404);
	}

    private void unsubscribe(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession sesh = request.getSession(false);
        if (sesh == null) {
            response.sendError(401, "No user session exists");
            return;
        }

        // Get the channel name
        String cname = request.getParameter("name");
        if (cname == null) {
            response.sendError(422, "name param not provided");
            return;
        }

        // Get the UserEntity object for this session, check if user is subscribed to channel
        UserEntity ue = (UserEntity) sesh.getAttribute("userInfo");
        if (!ue.getSubscriptions().contains(cname)) {
            response.sendError(404, "User is not subscribed to channel: " + cname);
            return;
        }

        // Update UserEntity and replace it in the database
        ue.removeSubscription(cname);
        DBWrapper.putUser(ue);
        sesh.setAttribute("userInfo", ue);  // Shouldn't need this, but add it just in case Jetty does some cloning

        // Redirect to front page
        response.sendRedirect("/");

    }

    private void subscribe(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession sesh = request.getSession(false);
        if (sesh == null) {
            response.sendError(401, "No user session exists");
            return;
        }

        // Get the channel name
        String cname = request.getParameter("name");
        if (cname == null) {
            response.sendError(422, "name param not provided");
            return;
        }

        // Make sure the channel exists
        if (!channelExists(cname)) {
            response.sendError(404, "No channel: " + cname);
            return;
        }

        // Get the UserEntity object for this session
        UserEntity ue = (UserEntity) sesh.getAttribute("userInfo");
        // Make sure the user isn't already subscribed
        if (ue.getSubscriptions().contains(cname)) {
            response.sendError(409, "User is already subscribed to channel: " + cname);
            return;
        }

        // Update UserEntity and replace it in the database
        ue.addSubscription(cname);
        DBWrapper.putUser(ue);
        sesh.setAttribute("userInfo", ue);  // Shouldn't need this, but add it just in case Jetty does some cloning

        // Redirect to front page
        response.sendRedirect("/");
    }

    private void deleteChannel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession sesh = request.getSession(false);
        if (sesh == null) {
            response.sendError(401, "No user session exists");
            return;
        }

        // Get the channel name and xpath
        String cname = request.getParameter("name");
        if (cname == null) {
            response.sendError(422, "name param missing");
            return;
        }

        // Make sure the channel exists
        ChannelEntity ce = DBWrapper.getChannel(cname);
        if (ce == null) {
            response.sendError(404, "No such channel: " + cname);
            return;
        }

        // Make sure the current user is the creator of the channel
        UserEntity ue = (UserEntity) sesh.getAttribute("userInfo");
        if (!ue.getUsername().equals(ce.getCreator())) {
            response.sendError(403, "Current user is not owner for channel: " + cname);
            return;
        }

        // Delete the channel, unsubscribe creator, and update DB
        ue.removeSubscription(cname);
        DBWrapper.deleteChannel(cname);
        DBWrapper.putUser(ue);

        // Redirect to front page
        response.sendRedirect("/");
    }

    private void createChannel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession sesh = request.getSession(false);
        if (sesh == null) {
            response.sendError(401, "No user session exists");
            return;
        }

        // Get the channel name and xpath
        String cname = request.getParameter("name");
        String xpath = request.getParameter("xpath");
        if (cname == null || xpath == null) {
            response.sendError(422, "param(s) missing");
            return;
        }

        // Make sure a channel with the given name doesn't already exist
        if (channelExists(cname)) {
            response.sendError(409, "Channel already exists with name: " + cname);
            return;
        }

        // Create the channel and update the UserEntity to be subscribed to it
        UserEntity ue = (UserEntity) sesh.getAttribute("userInfo");
        ue.addSubscription(cname);
        DBWrapper.createChannel(cname, xpath, ue.getUsername());
        DBWrapper.putUser(ue);

        // Redirect to the front page
        response.sendRedirect("/");
    }

    private void showChannel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession sesh = request.getSession(false);
        if (sesh == null) {
            response.sendError(401, "No user session exists");
            return;
        }

        // Get the channel name
        String cname = request.getParameter("name");
        if (cname == null) {
            response.sendError(422, "param missing");
            return;
        }

        // Make sure this user is subscribed to this channel and that the channel still exists (may have been deleted)
        UserEntity ue = (UserEntity) sesh.getAttribute("userInfo");
        if (!ue.getSubscriptions().contains(cname) || !channelExists(cname)) {
            response.sendError(404, "Channel either doesn't exist, or user is not subscribed to it: " + cname);
            return;
        }

        // Print the Channel if all has gone well
        ChannelEntity ce = DBWrapper.getChannel(cname);
        printChannel(ce, response);
    }

    private void printChannel(ChannelEntity ce, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        // Collect all the resources that have been put in this channel
        HashSet<String> urlStrs = ce.getUrls();
        List<ResourceEntity> resources = new LinkedList<>();
        for (String urlStr : urlStrs) {
            // We get resources by URL objects, so need to do a quick conversion
            try {
                URL url = URI.create(urlStr).toURL();
                ResourceEntity re = BerkeleyManager.getResource(url);
                if (re != null) {
                    resources.add(re);
                } else {
                    LOG.error("Failed to find ResourceEntity in database for URL: " + url.toString());
                }
            } catch (MalformedURLException e) {
                LOG.error("Failed to generate a URL from a ChannelEntity's key: " + urlStr);
            }
        }

        // Print the document according to the picky (yet simultaneously underspecified) format in the instructions
        PrintWriter pw = response.getWriter();
        pw.write("<html><body>");  // Opening tags
        printChannelHeader(ce, pw); // Channel header
        for (ResourceEntity re : resources) {
            printDocumentHeader(re, pw);  // Doc header
            printDocumentContents(re, pw); // Doc contents
        }
        pw.write("</body></html>"); // Closing tags

    }

    private void printChannelHeader(ChannelEntity ce, PrintWriter pw) {
        pw.write("<div class=\'channelheader\'>");
        pw.write("Channel name: " + ce.getName() + ", created by: " + ce.getCreator());
        pw.write("</div>");
    }

    private void printDocumentHeader(ResourceEntity re, PrintWriter pw) {
        pw.write("<div class=\'documentheader\'>");
        String timestamp = formatTimestamp(re.getTimestamp());
        pw.write("Crawled on: " + timestamp + "</br>");
        pw.write("Location: " + re.getUrl() + "</br>");
        pw.write("</div>");
    }

    private void printDocumentContents(ResourceEntity re, PrintWriter pw) {
        pw.write("<div class=\'document\'");
        pw.write(re.getBody());
        pw.write("</div>");
    }

    private String formatTimestamp(long timestamp) {
        return ISO_FORMATTER.format(Instant.ofEpochMilli(timestamp).atOffset(ZoneOffset.UTC));
    }

    /**
     * @param cname the name of a Channel
     * @return True if a ChannelEntity exists in the database with the given namme
     */
    private boolean channelExists(String cname) {
        return !(DBWrapper.getChannel(cname) == null);
    }

    private void lookupResource(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Get the query parameter.  Send 422 (Unprocessable Entity) if one is not provided
        String urlStr = request.getParameter("url");
        if (urlStr == null)
        {
            response.sendError(422);
            return;
        }

        // Try to build a URL, then look it up in the database
        ResourceEntity re;
        try {
            URL url = new URL(urlStr);
            re = BerkeleyManager.getResource(url);
        } catch (MalformedURLException e) {
            response.sendError(400);
            return;
        }

        // 404 if no resource found for the given URL
        if (re == null)
        {
            response.sendError(404);
            return;
        }

        // Print the resource's body, send everything as UTF-8 for now
        String ctype = (re.getContentType() != null && re.getContentType().contains("utf-8")) ? re.getContentType() : re.getContentType() + "; charset=utf-8";
        response.setContentType(ctype);
        OutputStream os  = response.getOutputStream();
        os.write(re.getBody().getBytes());
    }

    private void displayUserInfo(HttpServletRequest request, HttpServletResponse response, HttpSession sesh) throws IOException
    {
        UserEntity ue = (UserEntity) sesh.getAttribute("userInfo");
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.write("<p>" + ue.getUsername() + " logged in </p>");

        printHomepageChannelList(ue, pw);
        printCreateChannelForm(ue, pw);

        pw.write(formatLink("Logout", "/logout"));
    }

    private void printCreateChannelForm(UserEntity ue, PrintWriter pw) {
        String form = "<form action=\"/create\" method=\"GET\" >" +
                "Channel Name: <input type=\"text\" name=\"name\"><br>" +
                "Channel XPath: <input type=\"text\" name=\"xpath\"><br>" +
                "  <input type=\"submit\" value=\"Submit\">" +
                "</form>";
        pw.write(form);
    }

    private void printHomepageChannelList(UserEntity ue, PrintWriter pw) {

        pw.write("<ul>");
        // Iterate over the list of channels in the system, printing info for each
        SortedMap<String, ChannelEntity> allChannels = DBWrapper.getAllChannels();
        for(ChannelEntity ce : allChannels.values()) {
            pw.write("<li>");
            String cname = ce.getName();

            // If user is subscribed to this channel, print a show link and an unsubscribe link
            if (ue.getSubscriptions().contains(cname)) {
                pw.write(formatLink(cname, "/show?name=" + cname));
                pw.write(formatLink("[Unsubscribe]", "/unsubscribe?name=" + cname));
            }

            // If user is not subscribed, print the channel with a subscribe l
            else {
                pw.write(cname);
                pw.write(formatLink("[Subscribe]", "/subscribe?name=" + cname));
            }

            // If user is the creator, also print the delete link
            if (ce.getCreator().equals(ue.getUsername())) {
                pw.write(formatLink("[Delete]", "/delete?name=" + cname));
            }
            pw.write("</li>");
        }
        pw.write("</ul>");
    }

    private String formatLink(String content, String href) {
        return String.format("<a href='%s'> %s </a>", href, content);
    }

    private void displayLoginForm(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.write("Login:");
        pw.write("<form action=\"/login\" method=\"POST\" >" +
                 "Username: <input type=\"text\" name=\"username\"><br>" +
                 "Password: <input type=\"text\" name=\"password\"><br>" +
                 "  <input type=\"submit\" value=\"Submit\">" +
                 "</form>");
        pw.write("Register:");
        pw.write("<form action=\"/register\" method=\"POST\" >" +
                "Username: <input type=\"text\" name=\"username\"><br>" +
                "Password: <input type=\"text\" name=\"password\"><br>" +
                "  <input type=\"submit\" value=\"Submit\">" +
                "</form>");
        pw.flush();
    }

}









