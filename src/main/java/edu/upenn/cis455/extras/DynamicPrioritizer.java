package edu.upenn.cis455.extras;

import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import edu.upenn.cis455.crawler.utils.Cache;

/**
 * Created by Alex on 4/30/18
 */
public class DynamicPrioritizer<T> extends StaticPrioritizer<T> {

    private Cache<String, Integer> hits;

    public DynamicPrioritizer(String prioritiesTxt, SerializationProvider<T> provider) {
        super(prioritiesTxt, provider);
        hits = new Cache<>();
    }

    @Override
    public int prioritize(T item) {
        // Look for this item's key (hostname) in a cache of hits.
        String hostname = provider.generateKey(item);
        Integer seen = hits.get(hostname);

        // Never before seen URLs (on this crawl) get maximum priority
        if (seen == null) {
            hits.put(hostname, 1);
            int priority = queues.size() - 1;
            super.queues.get(priority).add(item);
            return priority;
        }

        // Otherwise, record another hit and prioritize the item statically (with the hostname trie)
        hits.put(hostname, seen + 1);
        return super.prioritize(item);
    }
}
