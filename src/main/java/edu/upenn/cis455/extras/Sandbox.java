package edu.upenn.cis455.extras;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis455.crawler.hostsplitter.RcvDaemon;
import edu.upenn.cis455.crawler.hostsplitter.SendDaemon;
import edu.upenn.cis455.crawler.pagetypes.JacksonPage;
import edu.upenn.cis455.crawler.pagetypes.JacksonPageSerializationProvider;
import edu.upenn.cis455.crawler.pagetypes.Page;

import java.io.IOException;
import java.net.URI;
import java.util.*;

/**
 * Created by Alex on 3/28/18
 */
public class Sandbox {

    public static void main(String[] args) throws IOException, InterruptedException {

//        // 4 queues w/ 10 numbers 1..4
//        ArrayList<Queue<Integer>> queues = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            queues.add(new LinkedList<>());
//            for (int j = 0; j < 10; j++) {
//                queues.get(i).add(i + 1);
//            }
//        }
//
//        QueueScheduler<Integer> drr = new DeficitRoundRobin<>();
//        drr.initialize(queues, null);  // Takes no extra params
//
//        // see how things get printed out
//        Integer retval = 0;
//        while (retval != null) {
//            retval = drr.schedule();
//            System.out.println(retval);
//        }

        // See if we can write a Page object, when it's underlying type is set up for Jackson
//        JacksonPage p = new JacksonPage();
//        byte[] body = "Life is a tale told by an idiot, full of sound and fury, but signifying nothing!\r\n".getBytes();
//        p.setBody(body);
//        p.setHeader("Thane of Fife", "Lord MacDuff");
//        p.addHeader("Thane of Fife", "Slayer of MacBeth");
//        List<String> links = new LinkedList<>();
//        links.add("http://gutenberg.org/");
//        links.add("http://shakepeare.com/");
//        p.setLinks(links);
//
//        JacksonPageSerializationProvider provider = new JacksonPageSerializationProvider();
//        String json = provider.writeObject(p);
//        System.out.println(json);
//
//        // Now read it back and see if it doesn't look stupid
//        Page p2 = provider.readObject(json);
//        System.out.println(new String(p2.getBody()));
//        System.out.println(p2.getHeader("Thane of Fife"));
//        System.out.println(p2.getHeaders("Thane of Fife"));
//        System.out.println(p2.getLinks());


        Thread snd = new Thread(new SendDaemon());
        snd.start();
        SendDaemon.sendUrl("127.0.0.1:8080", URI.create("http://foo.com/a/b/c").toURL());
        SendDaemon.sendUrl("127.0.0.1:8080", URI.create("http://bar.com/a/b/c").toURL());

        Thread rcv = new Thread(new RcvDaemon("127.0.0.1:8080"));
        rcv.start();

//
//        // There should be one item in this IP address's virtual queue, once the daemon has had enough time to take it
//        Thread.sleep(1000);
//        Queue<SendDaemon.SendItem> q = SendDaemon.virtualQueues.get("127.0.0.1:8080");
//
//        ObjectMapper om = new ObjectMapper();
//        String qJson = om.writer().writeValueAsString(q);
//        System.out.println(qJson);
//
//        // The magic line: take a JSON list and turn it into a queue of JSON objects...just works somehow
//        q = om.readValue(qJson, new TypeReference<Queue<SendDaemon.SendItem>> () {});
//
//        SendDaemon.SendItem s1 = q.remove();
//        System.out.println(s1.type == SendDaemon.MsgType.ROUTING);
//        System.out.println(s1.type == SendDaemon.MsgType.URL);
//        System.out.println(s1.value);
//        System.out.println(s1.addr);
//
//
//        SendDaemon.SendItem s2 = q.remove();
    }
}
