package edu.upenn.cis455.extras;

import edu.upenn.cis455.crawler.interfaces.Prioritizer;
import edu.upenn.cis455.crawler.interfaces.SerializationProvider;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Alex on 3/28/18
 */
public class StaticPrioritizer<T> implements Prioritizer<T> {

    private static final Logger LOG = LogManager.getLogger(StaticPrioritizer.class);

    protected List<Queue<T>> queues;
    private Trie<String, Integer> prioritiesTrie;
    protected SerializationProvider<T> provider;

    public StaticPrioritizer(String prioritiesTxt, SerializationProvider<T> provider) {

        this.provider = provider;

        // queues indices range from 0 to ~9. Root priority is 0
        prioritiesTrie = new Trie<>(0);

        // Build the prefix tree of prioritiesTrie from a file
        try {
            for (String line : CrawlerUtils.readStringsFile(prioritiesTxt)) {

                // Simple format: edu.cis.upenn,5
                String[] split = line.split(",");
                if (split.length != 2) {
                    continue;
                }
                String[] toks = split[0].split("\\.");
                if (toks.length < 1) {
                    continue;
                }
                Integer priority = Integer.parseInt(split[1]);

                // Note that the file order kind of matters for defaults in prioritiesTrie:
                // e.g. adding "edu.upenn,5", then "edu.princeton,1" means "edu.dartmouth" gets priority 5
                prioritiesTrie.add(toks, priority);
            }
        } catch (IOException e) {
            LOG.fatal("Initialization failed for file " + prioritiesTxt, e);
            System.exit(-1);  
        } 

    }

    @Override
    public void initialize(List<? extends Queue<T>> queues) {
        this.queues = (List<Queue<T>>) queues;
    }

    @Override
    public int prioritize(T item) {
        String[] toks = provider.tokenize(item);
        Integer priority = prioritiesTrie.get(toks);

        // Shouldn't be null, but check anyway
        if (priority == null) {
            priority = 0;
        }

        // Some bounds checking, since priorities.txt doesn't have to align with the actual number of frontend queues
        priority = Math.min(queues.size() - 1, priority);

        // Negative priority indicates it's something we don't want to enqueue at all
        if (priority < 0) {
            return priority;
        }

        queues.get(priority).add(item);
        return priority;
    }
}
