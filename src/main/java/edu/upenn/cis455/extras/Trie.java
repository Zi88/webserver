package edu.upenn.cis455.extras;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alex on 4/19/18
 */
public class Trie<K, V> {

    private class Node {
        K key;
        V val;
        List<Node> children;
    }

    Node root;

    public Trie(V defaultVal) {
        root = new Node();
        root.val = defaultVal; // Have to provide a default
    }

    // Given a list of tokens, add an entry in the trie
    public void add(K[] tokens, V val) {
        Node curr = root;
        for (int i = 0; i < tokens.length; i++) {
            curr = addNextHop(curr, tokens, i);
            // Don't overwrite values of intermediate nodes, but add them if they were newly created
            if (curr.val == null || curr.key == null) {
                curr.key = tokens[i];
                curr.val = val;
            }
        }
    }

    /**
     * Get the value of the deepest node that matches the prefixes provided by tokens
     * @param tokens array of tokens in the order we would like to walk the tree e.g. ["edu", "upenn", "cis"]
     * @return the value of the node deepest in the tree, whose parents and self match the most prefixes
     */
    public V get(K[] tokens) {
        Node curr = root;
        for (int i = 0; i < tokens.length; i++) {
            boolean keepGoing = false;
            if (curr.children == null) {
                break;
            }
            for (Node next: curr.children) {
                if (next.key.equals(tokens[i])) {
                    curr = next;
                    keepGoing = true;
                    break;
                }
            }
            if (!keepGoing) {
                break;
            }
        }
        return curr.val;
    }

    private Node addNextHop(Node curr, K[] tokens, int i) {
        // Should be unreachable if called from for loop
        if (i >= tokens.length) {
            return curr;
        }

        // If no child list, add one and create child with the given val
        if (curr.children == null) {
            curr.children = new LinkedList<>();
            Node next = new Node();
            curr.children.add(next);
            return next;
        }

        // Otherwise, examine the next pointers for a good one
        for (Node next : curr.children) {
            if (next.key.equals(tokens[i])) {
                return next;
            }
        }

        // Add a new child to a preexisting list
        Node next = new Node();
        curr.children.add(next);
        return next;
    }
}
