package edu.upenn.cis455.extras;

import edu.upenn.cis455.crawler.interfaces.QueueScheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Alex on 3/28/18
 */
public class DeficitRoundRobin<T> implements QueueScheduler<T> {

    private int[] quantums;  // Each queue's budget per round (min number of items it gets to send per round)
    private int[] deficits;  // The number of items that a queue is "owed" (max number of items it can send per round)
    private int pos = 0;     // Our current position in a given round
    private List<Queue<T>> queues;  // The actual queues
    private int k;

    @Override
    public void initialize(List<? extends Queue<T>> queues, int[] params) {
        k = queues.size();
        quantums = new int[k];
        deficits = new int[k];
        this.queues = (List<Queue<T>>) queues;
        for (int i = 0; i < quantums.length; i++) {
            quantums[i] = i + 1;       // 9th queue gets a quantum of 10, 8th gets 9, etc.
            deficits[i] = 1;           // Everybody starts with just 1 item
        }
    }

    @Override
    public T schedule() {
        int startingPt = pos;
        do {
            Queue<T> curr = queues.get(pos);
            // If current queue is empty (because it was drained in previous call), it loses its accumulated deficit
            if (curr.isEmpty()) {
                deficits[pos] = 0;
                updatePos();
                continue;
            }

            // Remove element, move to next position, decrement deficit counter
            T retval = curr.remove();
            deficits[pos] -= 1;

            // Move to this queue has consumed its budget
            if (deficits[pos] == 0) {
                updatePos();
            }

            // Return the value
            return retval;
        } while (pos != startingPt);

        // return null if nothing is available in any queue
        return null;
    }

    private void updatePos() {
        pos++;
        // Check bounds
        if (pos >= k) {
            pos = 0;
        }

        // When moving to a new queue, increment this queue's deficit by its quantum
        deficits[pos] += quantums[pos];
    }
}
