package test.edu.upenn.storage;

import edu.upenn.cis455.crawler.webtools.Response;
import edu.upenn.cis455.storage.BerkeleyManager;
import edu.upenn.cis455.storage.ResourceEntity;
import junit.framework.TestCase;

import java.net.URI;

/**
 * Created by Alex on 3/3/18
 */
public class DBWrapperTest extends TestCase {


    public void testPutThenGetResponse() throws Exception {
        // Put a resource and see if we can get it back
        BerkeleyManager.initManager();
        Response resp = new Response();
        resp.setBody("Some body text is here".getBytes());
        resp.setTimestamp(123456789);
        resp.setStatusCode(200);
        BerkeleyManager.putResponse(URI.create("http://foo.com/scraped/from/here.txt").toURL(), resp);

        ResourceEntity gotResponse = BerkeleyManager.getResource(URI.create("http://foo.com/scraped/from/here.txt").toURL());
        assertEquals(resp.getTimestamp(), gotResponse.getTimestamp());
        assertEquals(resp.getBodyStr(), gotResponse.getBody());
    }


    public void testGetNonexistent() throws Exception {
        // Get a resource that doesn't exist.  Should return null
        BerkeleyManager.initManager();
        ResourceEntity re =BerkeleyManager.getResource(URI.create("http://nonexistent-website.com/some/resource/that/doesnt/exist/anywhere.html").toURL());
        assertNull(re);
    }

}