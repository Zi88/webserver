package test.edu.upenn.xpathengine;

import edu.upenn.cis455.xpathengine.XPathEngineImpl;
import junit.framework.TestCase;
import org.junit.Before;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Alex on 3/26/18
 */
public class XPathEngineImplTest extends TestCase {

    private static XPathEngineImpl engine;
    private static Document africa;


    @Before
    public void setUp() throws Exception {
        engine = new XPathEngineImpl();
        africa = fileToDoc("resources/NYTAfrica.xml");
    }

    public void testBasicParses() throws Exception {
        // Basic XPaths with no tests, only steps
        String [] xpath = {"/rss/channel/link", "/rss/channel/image/title", "/notRSS/channel/link", "/rss/channel/Notlink"};
        boolean[] expected = {true, true, false, false};
        engine.setXPaths(xpath);
        boolean[] actual = engine.evaluate(africa);
        assertArrayEquals(expected, actual);
    }

    public void testBasicParsesWithTests() throws Exception {
        // Basic XPaths with simple (terminal) tests
        String [] xpath = {"/rss[@version= \"2.0\"]/channel/link",
                "/rss/channel/title[contains( text (), \"Africa\"]",
                "/rss[@version=\"2.0\"]/channel/item/title[text()=\"World Briefing: Africa, Americas, Europe and Asia\"]",
                "/rss/channel[@nosuchattr=\"whatever\"]/title"};
        boolean[] expected = {true, true, true, false};

        engine.setXPaths(xpath);
        boolean[] actual = engine.evaluate(africa);
        assertArrayEquals(expected, actual);
    }

    public void testStepTests() throws Exception {
        // Some tougher XPaths that include (non-terminal) step tests
        String [] xpath = {
                "/rss/channel[link]",
                "/rss[channel[link][description[contains(text(), \"world news\")]]]",  // Tricky nested step tests + terminal test
                "/rss/channel[item/title[contains(text(), \"Khartoum\"]][item/title[contains(text(), \"World Briefings\"]]"
        };
        boolean[] expected = {true, true, true};

        engine.setXPaths(xpath);
        boolean[] actual = engine.evaluate(africa);
        assertArrayEquals(expected, actual);
    }

    public void testUltraNestedSteps() throws Exception {
        String[] xpath = {
                "/rss/channel[title[text()=\"NYT > Africa\"]][link][description][item[guid]][item[link]]"  // Very broad test
        };
        boolean[] expected = {true};

        engine.setXPaths(xpath);
        boolean[] actual = engine.evaluate(africa);
        assertArrayEquals(expected, actual);
    }

    public void testIsValid() throws Exception {
        String[] xpaths = {
                "/rss/easily/valid",
                "/rss[a/little][subtler][but[@attname=\"val\"][still[contains(text(), \"valid\")]]]",
                "no/root",
                "/some/extra/suff//",
                "/something/weird][contains(text(), \"Text\")]",
                "/rss/[emptynodename]",
                "/rss/this/looks/okay[but]][its/got/an/extra/parens]]"
        };
        engine.setXPaths(xpaths);
        boolean[] expected = {true, true, false, false, false, false, false};
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], engine.isValid(i));
        }
    }

    // Helper method to take a file and turn it into a Document object
    private static Document fileToDoc(String filepath) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(Paths.get(filepath).toFile());
    }
}