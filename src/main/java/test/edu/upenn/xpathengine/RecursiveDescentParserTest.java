package test.edu.upenn.xpathengine;

import edu.upenn.cis455.xpathengine.RecursiveDescentParser;
import edu.upenn.cis455.xpathengine.Step;
import edu.upenn.cis455.xpathengine.XPath;
import junit.framework.TestCase;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Alex on 3/25/18
 */
public class RecursiveDescentParserTest extends TestCase {

    private static RecursiveDescentParser parser;

    @Override
    public void setUp() throws Exception {
        parser = new RecursiveDescentParser();

    }

    public void testBasicNoTests() throws Exception {
        String exp = "/foo/bar/baz";
        XPath xpath = parser.compile(exp);

        // See if the parser correctly finds the first three nodes
        Step curr = xpath.first;
        assertEquals("foo", xpath.first.nodename);
        curr = getFirstChild(curr);
        assertEquals("bar", curr.nodename);
        curr = getFirstChild(curr);
        assertEquals("baz", curr.nodename);

        // There shouldn't be any extra, dangling refs
        curr = getFirstChild(curr);
        assertNull(curr);
    }

    public void testOneStepWithTextTest() throws Exception {
        String exp = "/firstGuy[text()=\"Something\"]";
        XPath xPath = parser.compile(exp);

        // Should be just one node, with a test attached to it
        Step curr = xPath.first;
        assertEquals("firstGuy", curr.nodename);
        assertNotNull(curr.tests);
        assertNotNull(curr.tests.get(0));

        // No dangling refs
        curr = getFirstChild(curr);
        assertNull(curr);
    }

    public void testOneStepWithManyTests() throws Exception {
        String exp = "/first[text () =\"Text Test\"][contains(text(), \"Contains Test\")][@attr = \"attval\"]";
        XPath xPath = parser.compile(exp);

        // Should be just one node, with 3 tests attached to it
        Step curr = xPath.first;
        assertEquals("first", curr.nodename);
        assertNotNull(curr.tests);
        assertEquals(3, curr.tests.size());

        // No dangling refs
        curr = getFirstChild(curr);
        assertNull(curr);
    }

    public void testSeveralStepsWithTests() throws Exception {
        String exp = "/first[text () =\"Text Test\"][contains(text(), \"Contains Test\")]" +
                "/second[@attr = \"attval\"]" +
                "/third[contains (text () , \"Another Contains Test\"]";
        XPath xPath = parser.compile(exp);

        // Should be just one node, with 3 tests attached to it
        Step curr = xPath.first;
        assertEquals("first", curr.nodename);
        assertNotNull(curr.tests);
        assertEquals(2, curr.tests.size());

        curr = getFirstChild(curr);
        assertEquals("second", curr.nodename);
        assertNotNull(curr.tests);
        assertEquals(1, curr.tests.size());

        curr = getFirstChild(curr);
        assertEquals("third", curr.nodename);
        assertNotNull(curr.tests);
        assertEquals(1, curr.tests.size());

        // No dangling refs
        curr = getFirstChild(curr);
        assertNull(curr);
    }

    public void testNestedSteps() throws Exception {
        String exp = "/first[second/third[fourth/fifth]]";
        XPath xPath = parser.compile(exp);

        Step curr = xPath.first;
        String[] expected = {"first", "second", "third", "fourth", "fifth"};
        for (int i = 0; i < 5; i++) {
            assertNotNull(curr);
            assertEquals(expected[i], curr.nodename);
            curr = getFirstChild(curr);
        }
    }

    public void testDoubleStepTest() throws Exception {
        String exp = "/d/e/f[foo[text()=\"something\"]][bar]";
        XPath xPath = parser.compile(exp);

        Step curr = xPath.first;
        String[] expected = {"d", "e", "f"};
        for (int i = 0; i < 3; i++) {
            assertNotNull(curr);
            assertEquals(expected[i], curr.nodename);
            if (i != 2) {
                curr = getFirstChild(curr);
            }
        }

        // Make sure that the third step has two children (each with correct nodenames)
        assertEquals(2, curr.next.size());
        assertEquals("foo", curr.next.get(0).nodename);
        assertEquals("bar", curr.next.get(1).nodename);
    }

    public void testUltraNestedDoubleStep() throws Exception {
        String exp = "/d/e/f[foo[text()=\"something\"][boo]][bar[hoo[woo]]]";
        XPath xPath = parser.compile(exp);

        Step curr = xPath.first;
        String[] expected = {"d", "e", "f"};
        for (int i = 0; i < 3; i++) {
            assertNotNull(curr);
            assertEquals(expected[i], curr.nodename);
            if (i != 2) {
                curr = getFirstChild(curr);
            }
        }

        // Make sure that the third step has two children (each with correct nodenames)
        assertEquals(2, curr.next.size());
        Step foo = curr.next.get(0);
        Step bar = curr.next.get(1);
        assertEquals("foo", foo.nodename);
        assertEquals("bar", bar.nodename);

        // foo and bar themselves have children
        Step boo = getFirstChild(foo);
        assertEquals("boo", boo.nodename);
        Step hoo = getFirstChild(bar);
        assertEquals("hoo", hoo.nodename);
        Step woo = getFirstChild(hoo);
        assertEquals("woo", woo.nodename);

    }

    public void testInvalidParses() throws Exception {
        // Unclosed bracket
        String exp = "/a/b/c[text()=\"Hello\"][foo/[]";
        assertNull(parser.compile(exp));

        // Extra closed bracket
        exp = "/a/b/c[text()=\"Hello\"]][foo/]";
        assertNull(parser.compile(exp));

        // Extra chars that don't go with a step
        exp = "/a/b/c[text()=\"Hello\"]xyz";
        assertNull(parser.compile(exp));

        // A tricker extra closed bracket
        exp = "/a/b/c[text()=\"Hello\"]]";
        assertNull(parser.compile(exp));

        // An invalid test
        exp = "/a[]";
        assertNull(parser.compile(exp));

        // An invalid step (no name)
        exp = "/a/";
        assertNull(parser.compile(exp));
    }

    public void testHandoutExamples() throws Exception {
        // One of the more complicated examples from the handout
        String exp = "/d/e/f[foo[text()=\"something\"]][bar]";

        XPath xPath = parser.compile(exp);
        Step d = xPath.first;
        Step e = d.next.get(0);
        Step f = e.next.get(0);
        Step foo = f.next.get(0);
        Step bar = f.next.get(1);

        // Make sure everybody has the correct nodename
        String[] expected  = {"d", "e", "f", "foo", "bar"};
        String[] actual = {d.nodename, e.nodename, f.nodename, foo.nodename, bar.nodename};
        assertArrayEquals(expected, actual);

        // foo and bar should be terminal
        assertNull(foo.next);
        assertNull(bar.next);

        // foo should have one test
        assertEquals(1, foo.tests.size());
    }

    private Step getFirstChild(Step step) {
        if (step.next == null) {
            return null;
        }
        return step.next.get(0);
    }
}