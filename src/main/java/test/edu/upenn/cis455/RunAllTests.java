package test.edu.upenn.cis455;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RunAllTests extends TestCase 
{
  public static Test suite() 
  {
    try {
      Class[]  testClasses = {
              Class.forName("test.edu.upenn.crawler.CrawlerUtilsTest"),
              Class.forName("test.edu.upenn.crawler.RobotsTxtInfoTest"),
              Class.forName("test.edu.upenn.crawler.HTTPClientTest"),
              Class.forName("test.edu.upenn.crawler.BufferedQueueTest"),
              Class.forName("test.edu.upenn.crawler.TrieTest"),
              Class.forName("test.edu.upenn.crawler.ChordRouterTest"),
              Class.forName("test.edu.upenn.storage.DBWrapperTest"),
              Class.forName("test.edu.upenn.xpathengine.RecursiveDescentParserTest"),
              Class.forName("test.edu.upenn.xpathengine.XPathEngineImplTest")

      };   
      
      return new TestSuite(testClasses);
    } catch(Exception e){
      e.printStackTrace();
    } 
    
    return null;
  }
}
