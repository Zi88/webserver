package test.edu.upenn.crawler;

import edu.upenn.cis455.crawler.utils.BufferedQueue;
import edu.upenn.cis455.crawler.utils.URLSerializationProvider;
import junit.framework.TestCase;

import java.io.File;
import java.net.URL;

/**
 * Created by Alex on 4/16/18
 */
public class BufferedQueueTest extends TestCase {

    private static final File f = new File("./resources/test-queue.txt");
    private static BufferedQueue<URL> q;

    @Override
    public void setUp() throws Exception {
        if (f.exists()) {
            f.delete();
            f.createNewFile();
        }
        // Default buffer size, flush on every write
        q = new BufferedQueue<>(f, new URLSerializationProvider(), -1);
    }

    @Override
    public void tearDown() throws Exception {
        f.delete();
    }

    public void testBasicAddRemove() throws Exception {

        // Create a few simple URLs and put them in the queue
        URL url1 = new URL("https://localhost:80/something");
        URL url2 = new URL("https://localhost:80/somethingElse");
        URL url3 = new URL("https://nashvillehotchicken.com:80/secretrecipes");

        q.add(url1);
        q.add(url2);
        q.add(url3);

        // Check basic properties (queue length, etc.)
        assertEquals(3, q.size());

        URL removed = q.remove();
        assertEquals(url1.toString(), removed.toString());
        assertEquals(2, q.size());

        removed = q.remove();
        assertEquals(url2.toString(), removed.toString());
        assertEquals(1, q.size());

        removed = q.remove();
        assertEquals(url3.toString(), removed.toString());
        assertEquals(0, q.size());
        assertTrue(q.isEmpty());

        // Shouldn't throw an exception if calling poll on an empty queue
        removed = q.poll();
        assertNull(removed);
    }

    public void testSavingQueue() throws Exception {
        // Create a few simple URLs and put them in the queue
        URL url1 = new URL("https://localhost:80/something");
        URL url2 = new URL("https://localhost:80/somethingElse");
        URL url3 = new URL("https://nashvillehotchicken.com:80/secretrecipes");

        q.add(url1);
        q.add(url2);
        q.add(url3);

        // Remove a couple entries, save the queue
        q.remove();
        q.remove();
        q.save();

        // Create a fresh queue from the underlying file
        q = new BufferedQueue<>(f, new URLSerializationProvider(), -1);

        // New queue should pick up where the old one left off, not reread anything
        assertEquals(1, q.size());
        URL removed  = q.remove();
        assertEquals(url3.toString(), removed.toString());
        assertEquals(0, q.size());
        assertTrue(q.isEmpty());
    }
}
