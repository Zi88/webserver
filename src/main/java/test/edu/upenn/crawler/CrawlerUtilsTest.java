package test.edu.upenn.crawler;

import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import junit.framework.TestCase;

import java.net.URI;
import java.net.URL;

/**
 * Created by Alex on 3/5/18
 */
public class CrawlerUtilsTest extends TestCase {


    public void testBuildURLBasic() throws Exception {

        // Try a simple URL
        URL defaultUrl = URI.create("http://crawltest.cis.upenn.edu:80").toURL();
        URL cleaned = CrawlerUtils.buildURL(defaultUrl, "bbc/");

        assertEquals("http", cleaned.getProtocol());
        assertEquals("crawltest.cis.upenn.edu", cleaned.getHost());
        assertEquals("/bbc", cleaned.getPath());
        assertEquals(80, cleaned.getPort());

        // A subtle variation of the same URL should be exactly the same
        cleaned = CrawlerUtils.buildURL(defaultUrl, "bbc");
        assertEquals("http", cleaned.getProtocol());
        assertEquals("crawltest.cis.upenn.edu", cleaned.getHost());
        assertEquals("/bbc", cleaned.getPath());
        assertEquals(80, cleaned.getPort());

        // An absolute URL should also be the same
        cleaned = CrawlerUtils.buildURL(defaultUrl, "http://crawltest.cis.upenn.edu/bbc/");
        assertEquals("http", cleaned.getProtocol());
        assertEquals("crawltest.cis.upenn.edu", cleaned.getHost());
        assertEquals("/bbc/", cleaned.getPath());
        assertEquals(80, cleaned.getPort());
    }


    public void testPathRelativeUrlVsRootRelative() throws Exception {

        // Try a path-relative url
        URL defaultUrl = URI.create("http://crawltest.cis.upenn.edu:80/root/to/curr").toURL();
        URL cleaned = CrawlerUtils.buildURL(defaultUrl, "sibling");

        assertEquals("/root/to/sibling", cleaned.getPath());
        assertEquals(80, cleaned.getPort());

        defaultUrl = URI.create("http://crawltest.cis.upenn.edu:80/root/to/curr/").toURL();
        cleaned = CrawlerUtils.buildURL(defaultUrl, "next");

        assertEquals("/root/to/curr/next", cleaned.getPath());

        // Try a root-relative url
        cleaned = CrawlerUtils.buildURL(defaultUrl, "/off/the/root");
        assertEquals("/off/the/root", cleaned.getPath());
    }


    public void testAbsoluteUrlMissingHost() throws Exception {

        // Try a URL that's missing the host but has the scheme.  weird but might as well try to parse it
        URL defaultUrl = URI.create("http://crawltest.cis.upenn.edu:80").toURL();
        URL cleaned = CrawlerUtils.buildURL(defaultUrl, "http:///hello/world.html");

        assertEquals("http", cleaned.getProtocol());
        assertEquals("crawltest.cis.upenn.edu", cleaned.getHost());
        assertEquals("/hello/world.html", cleaned.getPath());
        assertEquals(80, cleaned.getPort());

    }


    public void testAbsoluteUrlDifferentHostAndPort() throws Exception {

        // Try a URL that has different (and valid) scheme, hostname, and port
        URL defaultUrl = URI.create("http://crawltest.cis.upenn.edu:80").toURL();
        URL cleaned = CrawlerUtils.buildURL(defaultUrl, "https://foo.com:443/hello/world.html");

        assertEquals("https", cleaned.getProtocol());
        assertEquals("foo.com", cleaned.getHost());
        assertEquals("/hello/world.html", cleaned.getPath());
        assertEquals(443, cleaned.getPort());
    }


    public void testUnusualInputs() throws Exception {

        // Try passing a blank link.  Should get back the default URL
        URL defaultUrl = URI.create("http://crawltest.cis.upenn.edu:80").toURL();
        URL cleaned = CrawlerUtils.buildURL(defaultUrl, "");

        assertEquals("http", cleaned.getProtocol());
        assertEquals("crawltest.cis.upenn.edu", cleaned.getHost());
        assertEquals("/", cleaned.getPath());
        assertEquals(80, cleaned.getPort());

        // Passing one slash should also give you the default URL
        cleaned = CrawlerUtils.buildURL(defaultUrl, "/");

        assertEquals("http", cleaned.getProtocol());
        assertEquals("crawltest.cis.upenn.edu", cleaned.getHost());
        assertEquals("/", cleaned.getPath());
        assertEquals(80, cleaned.getPort());
    }


    public void testPathRelativeToFile() throws Exception {

        // Try two URLs to files, and see if the parser figures out that they should be siblings
        URL defaultUrl = URI.create("http://crawltest.cis.upenn.edu:80/dir/file1.html").toURL();
        URL cleaned = CrawlerUtils.buildURL(defaultUrl, "file2.xml/");

        assertEquals("crawltest.cis.upenn.edu", cleaned.getHost());
        assertEquals("/dir/file2.xml", cleaned.getPath());
    }
}