package test.edu.upenn.crawler;

import edu.upenn.cis455.crawler.hostsplitter.ChordRouter;
import junit.framework.TestCase;

/**
 * Created by Alex on 4/27/18
 */
public class ChordRouterTest extends TestCase {

    /* Test file has the following addrs, hashing to:
        Self:
        127.0.0.1:8080 ->    469828277852287134

        Fingers: largest to smallest
        36.92.46.255:8080 -> 1073622785889815328
        self              -> 469828277852287134
        32.12.43.12:8080 ->  281488866321855605
        12.34.56.78:8080 ->  41516692860413399
     */

    private static ChordRouter router;

    @Override
    public void setUp() throws Exception {
        router = new ChordRouter();
        router.init("127.0.0.1:8080", "./resources/testFingers.txt");
    }

    public void testBasicProperties() throws Exception {
        // We should have a self with successors and predecessors
        assertNotNull(router.self);
        assertNotNull(router.self.predecessor);
        assertNotNull(router.self.successor);

        // Can't say much about predecessor or successor
    }

    public void testBasicRouting() throws Exception {
        long to36xx = 1073622785889815300L;
        long toSelf = 469828277852287000L;
        long to32xx = 281488866321855604L;
        long to12xx = 41516692860413399L;

        // See if all the normal cases get routed correctly
        String addr = router.route(to36xx);
        // The successor of self gets the value routed to it on the first hop
        assertEquals("36.92.46.255:8080", addr);

        addr = router.route(toSelf);
        // Keys belonging to self also get routed to on first hop
        assertEquals("127.0.0.1:8080", addr);

        // For other keys, we don't know the successor for sure.  So we route for the closest preceding node that we know of
        addr = router.route(to32xx);
        assertEquals("12.34.56.78:8080", addr);

        // Even on an exact match, we route to the node that we know immediately precedes the key (could optimize this)
        addr = router.route(to12xx);
        assertEquals("36.92.46.255:8080", addr);

        // Reinit as 36.xx, and see if the next hop routes to 12.xx correctly
        router.init("36.92.46.255:8080", "./resources/testFingers.txt");
        addr = router.route(to12xx);
        assertEquals("12.34.56.78:8080", addr);

        // Now reinit as 12.xx, and see if it knows to stop routing
        router.init("12.34.56.78:8080", "./resources/testFingers.txt");
        addr = router.route(to12xx);
        assertEquals("12.34.56.78:8080", addr);

        // Double tap
        router.route(to12xx);
        assertEquals("12.34.56.78:8080", addr);

    }

    public void testWrapAroundRouting() throws Exception {

        // One larger than 36.xx. See if we wrap around in a couple hops
        long to12xx = 1073622785889815329L;

        // We don't know 36.xx's successor, so self routes to 36.xx first
        String addr = router.route(to12xx);
        assertEquals("36.92.46.255:8080", addr);

        // 36.xx knows its successor is the first to overshoot, so it routes there
        router.init("36.92.46.255:8080", "./resources/testFingers.txt");
        addr = router.route(to12xx);
        assertEquals("12.34.56.78:8080", addr);

        // 12.xx knows to stop
        router.init("12.34.56.78:8080", "./resources/testFingers.txt");
        addr = router.route(to12xx);
        assertEquals("12.34.56.78:8080", addr);


        // Another case: hash is the same as 12.xx
        to12xx = 41516692860413399L;

        // Again, self only knows enough to get to 36.xx
        router.init("127.0.0.1:8080", "./resources/testFingers.txt");
        addr = router.route(to12xx);
        assertEquals("36.92.46.255:8080", addr);

        // 36.xx handles the boundary condition when routing to neighbor
        router.init("36.92.46.255:8080", "./resources/testFingers.txt");
        addr = router.route(to12xx);
        assertEquals("12.34.56.78:8080", addr);

        // 12.xx knows to stop
        router.init("12.34.56.78:8080", "./resources/testFingers.txt");
        addr = router.route(to12xx);
        assertEquals("12.34.56.78:8080", addr);
    }
}