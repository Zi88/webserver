package test.edu.upenn.crawler;

import edu.upenn.cis455.crawler.webtools.HTTPClient;
import edu.upenn.cis455.crawler.webtools.Response;
import junit.framework.TestCase;
import org.junit.Before;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Alex on 3/20/18
 */
public class HTTPClientTest extends TestCase {

    private static HTTPClient client;

    private String normalGETResponse =  "HTTP/1.1 200 OK\r\n" +
                                        "Server: Apache\r\n" +
                                        "Pointless-Header: Whatever\r\n" +
                                        "No-Lists: value1, value2, value3  \r\n " +
                                        "Content-Length: 20\r\n\r\n" +
                                        "01234567890123456789";

    private String linefeedsOnlyGETResponse =   "HTTP/1.1 200 OK\n" +
                                                "Server: Apache\r\n" +
                                                "Pointless-Header: Whatever\n" +
                                                "Content-Length: 20\n\n" +
                                                "01234567890123456789";

    private String tooShortGETResponse =    "HTTP/1.1 200 OK\n" +
                                            "Server: Apache\r\n" +
                                            "Pointless-Header: Whatever\n" +
                                            "Content-Length: 10000\n\n" +
                                            "This response is way shorter than expected\r\n Will the parser know?";

    private String chunkedGETResponse =
            "HTTP/1.1 200 OK\n" +
            "Server: Apache\r\n" +
            "Pointless-Header: Whatever\n" +
            "Transfer-Encoding: chunked\r\n" +
            "Transfer-Encoding: gzip, deflate\r\n\r\n" +
            "4\r\nabcd\r\nA\r\nefghijklmn\r\n2\r\n\r\n\r\n5\r\n12345\r\n0\r\n";

    private String trickyChunkedGETResponse = "HTTP/1.1 200 OK\n" +
            "Server: Apache\r\n" +
            "Pointless-Header: Whatever\n" +
            "Transfer-Encoding: chunked\r\n\r\n" +
            "4\nAnd \r\n3\r\nall\n4\r\n our\nb\n yesterdays\r\n0\r\n";


    @Before
    public void setUp() {
        client = new HTTPClient();
    }


    public void testReadNormalGETResponse() throws Exception {
        // See if we read the body correctly for a vanilla GET request
        ByteArrayInputStream dummySock = new ByteArrayInputStream(normalGETResponse.getBytes());
        Response resp = client.readResponse(dummySock, HTTPClient.RequestType.DOWNLOAD);
        assertEquals("01234567890123456789", resp.getBodyStr());

        // Even if the parser doesn't recognize that a header accepts comma lists, it should handle it gracefully
        assertNotNull(resp.getHeaders("No-Lists"));
        assertEquals(1, resp.getHeaders("No-Lists").size());
        assertEquals("value1, value2, value3", resp.getHeaders("No-Lists").get(0));
    }


    public void testLinefeedsGETResponse() throws Exception {
        // This get request has a combo of crlf and just lf separating lines
        ByteArrayInputStream dummySock = new ByteArrayInputStream(linefeedsOnlyGETResponse.getBytes());
        Response resp = client.readResponse(dummySock, HTTPClient.RequestType.DOWNLOAD);
        assertEquals("Apache", resp.getHeader("Server"));
        assertEquals("01234567890123456789", resp.getBodyStr());
    }


    public void testTooShortGETResponse() throws Exception {
        // This get request has a combo of crlf and just lf separating lines
        ByteArrayInputStream dummySock = new ByteArrayInputStream(tooShortGETResponse.getBytes());
        Response resp = client.readResponse(dummySock, HTTPClient.RequestType.DOWNLOAD);
        assertEquals("This response is way shorter than expected\r\n Will the parser know?", resp.getBodyStr());
    }

    public void testChunkedGETResponse() throws Exception {
        // This is a fairly simple get GET response that involves a chunked body with no Content-Length header
        ByteArrayInputStream dummySock = new ByteArrayInputStream(chunkedGETResponse.getBytes());
        Response resp = client.readResponse(dummySock, HTTPClient.RequestType.DOWNLOAD);
        assertEquals("abcdefghijklmn\r\n12345", resp.getBodyStr());

        // All three Transfer-Encoding header values should be stored separately in a list (no comma separated String)
        assertEquals(3, resp.getHeaders("Transfer-Encoding").size());
    }

    public void testTrickyChunkedResponse() throws Exception {
        // A chunked response with missing carriage returns, lowercase hex, etc.
        ByteArrayInputStream dummySock = new ByteArrayInputStream(trickyChunkedGETResponse.getBytes());
        Response resp = client.readResponse(dummySock, HTTPClient.RequestType.DOWNLOAD);
        assertEquals("And all our yesterdays", resp.getBodyStr());
    }

}