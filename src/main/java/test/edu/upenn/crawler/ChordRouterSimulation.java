package test.edu.upenn.crawler;

import edu.upenn.cis455.crawler.hostsplitter.ChordRouter;
import edu.upenn.cis455.crawler.hostsplitter.SendDaemon;
import edu.upenn.cis455.crawler.utils.CrawlerUtils;
import edu.upenn.cis455.storage.BerkeleyManager;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Created by Alex on 4/30/18
 */
public class ChordRouterSimulation {

    private static final String testFingersTxt = "./resources/localTestFingers.txt";
    private static ArrayList<URL> emissions = new ArrayList<>();
    private static Map<String, String> knownCorrect = new HashMap<>();

    /*
     *  // The simulated server addresses:
        127.0.0.1:8001 -> 850827335640890750
        127.0.0.1:8003 -> 219943086909887294
        127.0.0.1:8002 -> 92227951634063093
        127.0.0.1:8000 -> 8622987826127333




        // The test URL hashes, and the servers that they correspond to
        en.wikipedia.org  -> 778355670399239292  -> 127.0.0.1:8001
        www.pitchfork.com -> 181092887548065419  -> 127.0.0.1:8003
        gutenberg.org     -> 1025938075758004752 -> 127.0.0.1:8000
        hypem.com         -> 1091989067908600761 -> 127.0.0.1:8000
        www.publin.ie     -> 377892757940534170  -> 127.0.0.1:8001
        www.savingcountrymusic.com -> 1111311241307235684 -> 127.0.0.1:8000
        docs.oracle.com            -> 35800511502906986   -> 127.0.0.1:8002
        grpc.io                    -> 836671249921302930  -> 127.0.0.1:8001
        stackoverflow.com          -> 141410390372173429  -> 127.0.0.1:8003
        www.cnn.com                -> 813592871954333936  -> 127.0.0.1:8001
     */

    // Map the hostnames to the known corect servers
    static {
        knownCorrect.put("en.wikipedia.org", "127.0.0.1:8001");
        knownCorrect.put("www.pitchfork.com", "127.0.0.1:8003");
        knownCorrect.put("gutenberg.org", "127.0.0.1:8000");
        knownCorrect.put("hypem.com", "127.0.0.1:8000");
        knownCorrect.put("www.publin.ie", "127.0.0.1:8001");
        knownCorrect.put("www.savingcountrymusic.com", "127.0.0.1:8000");
        knownCorrect.put("docs.oracle.com", "127.0.0.1:8002");
        knownCorrect.put("grpc.io", "127.0.0.1:8001");
        knownCorrect.put("stackoverflow.com", "127.0.0.1:8003");
        knownCorrect.put("www.cnn.com", "127.0.0.1:8001");
    }


    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {

        BerkeleyManager.initManager();

        // Take some URLs to emit
        emissions.add(new URL("http://en.wikipedia.org/whatever"));
        emissions.add(new URL("https://www.pitchfork.com/"));
        emissions.add(new URL("http://gutenberg.org/macbeth"));
        emissions.add(new URL("http://hypem.com/"));
        emissions.add(new URL("https://www.publin.ie"));
        emissions.add(new URL("https://www.savingcountrymusic.com/"));
        emissions.add(new URL("https://docs.oracle.com"));
        emissions.add(new URL("http://grpc.io/"));
        emissions.add(new URL("http://stackoverflow.com/big-numbers"));
        emissions.add(new URL("https://www.cnn.com/"));

        ChordRouter r = new ChordRouter();
        r.init("", testFingersTxt);
        for (URL u : emissions) {
            System.out.println("Hash of " + u.getHost() + " -> " + r.getHash(u.getHost()));
        }


        // All our virtual server can share a SendDaemon (it's just an HTTP Client)
        new Thread(new SendDaemon()).start();

        List<String> addrs = CrawlerUtils.readStringsFile(testFingersTxt);
        for (String addr: addrs) {
            System.out.println("Hash of " + addr + " -> " + r.getHash(addr));
            new Thread(new SimulatedServer(addr, testFingersTxt)).start();
        }
    }

    public static class SimulatedServer implements Runnable {
        ChordRouter router = new ChordRouter();
        String self;
        String fingersTxt;

        SimulatedServer(String addr, String fingersTxt) {
            this.self = addr;
            this.fingersTxt = fingersTxt;
        }

        @Override
        public void run() {
            new Thread(new SimulatedRcvDaemon(this.self, this)).start();
            try {
//                HostSplitterBolt bolt = new HostSplitterBolt();
//                bolt.prepare(null, null, null);
                router.init(self, fingersTxt);
                Random r = new Random();
                while (true) {

                    // Randomly emit a URL, routing it to wherever we think it's supposed to go
                    Thread.sleep(500);
                    URL randomUrl = emissions.get(r.nextInt(emissions.size()));
                    simulateHostSplitting(randomUrl);
                                    }
            } catch (NoSuchAlgorithmException | IOException e) {
                e.printStackTrace();
            } catch (InterruptedException ignored) {
            }

        }

        public void simulateHostSplitting(URL url) {

            // See if we are the terminal node
            String routedAddr = router.route(url.getHost());
            String actualAddr = knownCorrect.get(url.getHost());

            // Send URL if it doesn't belong to you (print error if it actually does belong to you
            if (!self.equals(routedAddr)) {
                if (self.equals(actualAddr)) {
                    System.err.println("Incorrectly routing " + url + " away from its dest " + self);
                }
                SendDaemon.sendUrl(self, url);
            }
            // If not sending URL away, verify that it does indeed belong to this node
            else {
                if (!self.equals(actualAddr)) {
                    System.err.println("Incorrectly routed " + url + " to " + self);
                } else {
                    System.out.println("Correctly routed " + url + " to " + self);
                }
            }
        }
    }
}
