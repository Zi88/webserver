package test.edu.upenn.crawler;

import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis455.crawler.hostsplitter.RcvDaemon;

import java.net.URL;
import java.util.Map;

/**
 * Created by Alex on 4/30/18
 *
 * This is a class solely for testing. It works just like the real RcvDaemon, but instead of emitting received URLs
 * to a collector, it counts the received ones
 */
public class SimulatedRcvDaemon extends RcvDaemon {

    private ChordRouterSimulation.SimulatedServer server;

    public SimulatedRcvDaemon(String ipPort, ChordRouterSimulation.SimulatedServer simulatedServer) {
        super(ipPort);
        this.server = simulatedServer;
    }

    @Override
    protected void handleUrl(URL url) {
        // This is analogous to the outputcollector of stormlite: pass the url back via collector to the splitting bolt
        server.simulateHostSplitting(url);
    }
}
