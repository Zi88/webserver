package test.edu.upenn.crawler;

import edu.upenn.cis455.extras.Trie;
import junit.framework.TestCase;

/**
 * Created by Alex on 4/19/18
 */
public class TrieTest extends TestCase {

    public void testBasicAddGet() throws Exception {
        Trie<String, Integer> prefixTree = new Trie<>(1);

        // Default priority
        assert prefixTree.get(new String[] {"edu", "upenn", "cis"}) == 1;

        // A .edu priority
        prefixTree.add(new String[] {"edu"}, 5);
        assert prefixTree.get(new String[] {"edu", "upenn", "cis"}) == 5;

        // A cis.upenn.edu priority
        prefixTree.add(new String[] {"edu", "upenn", "cis"}, 10);
        assert prefixTree.get(new String[] {"edu", "upenn", "cis"}) == 10;

        // One just above remains unaffected
        assert prefixTree.get(new String[] {"edu", "princeton"}) == 5;

        // A sibling doesn't overwrite the priority of parent
        prefixTree.add(new String[] {"edu", "upenn", "ese"}, 4);
        assert prefixTree.get(new String[] {"edu", "upenn"}) == 10;
        assert prefixTree.get(new String[] {"edu", "upenn", "ese"}) == 4;

    }
}