package test.edu.upenn.crawler;

import edu.upenn.cis455.crawler.info.RobotsTxtInfo;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Alex on 3/6/18
 */
public class RobotsTxtInfoTest extends TestCase {

    private static RobotsTxtInfo robo;

    @Before
    public void setUp() throws Exception {
        robo = new RobotsTxtInfo();

    }


    public void testIsDisallowedBasic() throws Exception {
        robo.addDisallowedLink("cis455crawler", "/something/private");

        // A parent directory shouldn't be disallowed
        boolean retval = robo.isDisallowed("/something/", "cis455crawler");
        assertFalse(retval);

        // The exact path should be disallowed, even if the slashes are different
        retval = robo.isDisallowed("something/private/", "cis455crawler");
        assertTrue(retval);

        // A subpath should also be disallowed
        retval = robo.isDisallowed("/something/private/to/some/subdir", "cis455crawler");
        assertTrue(retval);
    }


    public void testIsDisallowedWildcard() throws Exception {
        robo.addDisallowedLink("*", "/path/to/the/launch-codes");
        robo.addDisallowedLink("badbot", "/path/to/wherever");

        // If no directives exist specifically for this user agent, the wildcard directives should apply
        boolean retval = robo.isDisallowed("/path/to/the/launch-codes", "anotherbot");
        assertTrue(retval);

        // If specific directives do exist for some user agent, wildcard directives should be ignored
        retval = robo.isDisallowed("/path/to/the/launch-codes", "badbot");
        assertFalse(retval);
    }
}