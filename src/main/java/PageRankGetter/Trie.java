package PageRankGetter;

import java.util.HashMap;
import java.util.Map;

public class Trie {
    class TrieNode{
        public char value;
        public Map<Character,TrieNode> childrenMap;
        public double rank;
        //public boolean isWord;
        public TrieNode(char value){
          this.value = value;
          childrenMap = new HashMap<> ();
          //isWord = false;
          rank = -1.0;
        }
    }
    TrieNode root;
    public Trie(){
      root = new TrieNode(' ');
    }
    public void insert(String word, double rank){
      char[] wordArray = word.toCharArray();
      insert(wordArray,0,root, rank);
    }
    private void insert(char[] wordArray, int index, TrieNode root, double rank){
      if (index >= wordArray.length){
        //root.isWord = true;
    	  root.rank = rank;
        return;
      }
      if (!root.childrenMap.containsKey(wordArray[index])){
        root.childrenMap.put(wordArray[index], new TrieNode(wordArray[index]));
      }
      insert(wordArray,index+1,root.childrenMap.get(wordArray[index]), rank);
    }
    
    public double search(String word){
      char[] wordArray = word.toCharArray();
      return search(wordArray,0,root);
    }
    
    private double search(char[] wordArray, int index, TrieNode root){
      if (index >= wordArray.length){
        return root.rank;
      }
      if (root.childrenMap.containsKey(wordArray[index]))
        return search(wordArray,index+1,root.childrenMap.get(wordArray[index]));
      else
        return -1.0;
    }
    
//    /** Returns if there is any word in the trie that starts with the given prefix. */
//    public boolean startsWith(String prefix) {
//        return startsWith(prefix.toCharArray(), 0, root);
//    }
//    
//     private boolean startsWith(char[] wordArray, int index, TrieNode root){
//      if (index >= wordArray.length){
//        return true;
//      }
//      if (root.childrenMap.containsKey(wordArray[index]))
//        return startsWith(wordArray,index+1,root.childrenMap.get(wordArray[index]));
//      else
//        return false;
//    }
}
