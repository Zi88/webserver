package PageRankGetter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class PageRankAccesser {

	Trie trie;

	public PageRankAccesser() {
		trie = new Trie();
	}
	
	public void init(String folderPath){
		File folder = new File(folderPath);
		if (folder.exists() && folder.isDirectory()) {
			File[] listOfFiles = folder.listFiles();
			for (File file : listOfFiles) {
			    if (file.isFile()) {
			        //System.out.println(file.getName());
					insertDataFromFile(file);
			    }
			}
		} else {
			System.out.println("have problem opening the page rank folder: " + folderPath);
		}
	}

	public void insertDataFromFile(File file) {

		//File file = new File("resources/part-00000");
        //String fileName = "temp.txt";
        String line = null;
        try {
            FileReader fileReader = 
                new FileReader(file);
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) {
            	//(weareboth.dreamwidth.org,3.348265192993585)
                //System.out.println(line);
            	try {
            		String urlAndRank = line.substring(1,line.length() - 1);
                    String[] parts = urlAndRank.split(",");
                    trie.insert(parts[0], Double.parseDouble(parts[1]));
            	} catch (Exception e) {
            		System.out.println("error format: " + line);
            	}
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                file.getName() + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + file.getName() + "'");                  
        }
	}
	
	/**
	 * 
	 * @param url
	 * @return -2 if cannot convert url to host, -1 if not found
	 */
	public double getRank(String url) {
		String hostName;
		try {
			hostName = new URL(url).getHost();
			return trie.search(hostName);
		} catch (MalformedURLException e) {
			System.out.println("cannot convert url to host: " + url);
			return -2.0;
		}
	}
	
	//public static void main(String[] args) {
		

//		trie.insert("hello", 3);
//		System.out.println(trie.search("hello"));
//		trie.insert("helloo", 4);
//		trie.insert("peach", 5);
//		System.out.println(trie.search(" "));
//		System.out.println(trie.search("helloo"));
//		System.out.println(trie.search("peach"));
//		System.out.println(trie.search("peac"));
	//}

}
