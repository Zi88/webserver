package SearchEngine;

import Database.TFIDFScoreObject;

import javax.servlet.ServletException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.CharBuffer;
import java.util.PriorityQueue;

public class SearchEngineServer {
    public static final int BUFFER_SIZE = 200;
    public static String directory = "";

    public static void readSocket(Socket socket) throws IOException {
        //Prepare the Socket
        InputStreamReader reader = new InputStreamReader(socket.getInputStream());
        BufferedReader in = new BufferedReader(reader);
        OutputStream outStream = socket.getOutputStream();
        PrintWriter out = new PrintWriter(outStream, true);

        //Communication variables
        int numRead = 0;
        String body = "";
        CharBuffer buffer = CharBuffer.allocate(BUFFER_SIZE);

        //Read into buffer
        while (true) {
            try {
                System.out.println(numRead);
                numRead = in.read(buffer);
            }
            catch (Throwable t) {
                t.printStackTrace();
                out.close();
                break;
            }

            buffer.flip();
            String current = buffer.toString();
            buffer.clear();
            body = body + current;
            if (numRead < BUFFER_SIZE) break;
        }

        //Print the message
        System.out.println("Message: " + body);

        //Get the top objects
        PriorityQueue<TFIDFScoreObject> pq = new PriorityQueue<TFIDFScoreObject>();



    }


    public static void main(String args[]) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, ServletException {
        directory = args[0]; if (!directory.endsWith("/")) directory = directory + "/";
        
        //Command line argument inputs
        int port = 8001;

        //Open socket
        System.out.println(port);
        ServerSocket serverSocket = new ServerSocket(port);


        while (true) {
            //Accepting a connection
            System.out.println("Looking for connection");
            Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
                clientSocket.setSoTimeout(20000);
                readSocket(clientSocket);
            } catch (Throwable t) {}
        }

    }


}
