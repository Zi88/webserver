package SearchEngine;

import Database.TFIDFScoreObject;
import PageRankGetter.PageRankAccesser;

import java.io.*;
import java.util.PriorityQueue;

public class SearchEngineIndexerHelpers {
    public static final int NUM_BUCKETS = 20000;
    public static final int LIMIT = 50;

    
    public static PriorityQueue<TFIDFScoreObject> tokenizer(String query,  PriorityQueue<TFIDFScoreObject> pq, String dir, PageRankAccesser accesser) throws IOException {
        //Normalize
        query = query.toLowerCase().trim();
        
        //First Use Stemmer
        String[] splitted = query.split(" ");
        for (int i = 0; i < splitted.length; i++) {
            Stemmer stemmer = new Stemmer();
            String current = splitted[i];
            stemmer.add(current.toCharArray(), current.length());
            stemmer.stem();
            String temp = stemmer.toString();
            splitted[i] = temp;


            //Check for file
            dir = dir + "/";
            String f = dir  + temp + ".txt";
            File file = new File(f);
            System.out.println(dir);
            if (file.exists()) {
                System.out.println("exist");

                //Read the file
                FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr);
                String currentLine = "";

                while ((currentLine = br.readLine()) != null) {
                    if (currentLine.length() < 3) continue;

                    //Parse the line
                    int index = currentLine.indexOf("@@");
                    String word = currentLine.substring(0, index);
                    int index2 = currentLine.indexOf(" ", index);
                    String url = currentLine.substring(index + 2, index2);
                    String scores = currentLine.substring(index2, currentLine.length());
                    scores = scores.trim();
                    double score = Double.parseDouble(scores);

                    //TODO: Multiply the score with PR SCORE
                    double rank = accesser.getRank(url);
                    if (rank < 0) {
                        if (rank == -1.0) {
                        	System.out.println("url doesn't exist: " + url);
                        } else if (rank == -2.0){
                        	System.out.println("url format is incorrect: " + url);
                        } else if (rank == 0.0){
                        	System.out.println("zero rank: " + url);
                        }
                    }

                    score = score * rank;

                    TFIDFScoreObject obj = new TFIDFScoreObject(word, url, score);
                    pq.add(obj);
                    if (pq.size() > LIMIT) pq.poll();
                }

                //Implement the double indexing
                if (i == 0) continue;
                f = dir + splitted[i - 1] + " " + temp + ".txt";

                file = new File(f);
                if (file.exists()) {

                    //Read the file
                    fr = new FileReader(f);
                    br = new BufferedReader(fr);
                    currentLine = "";

                    while ((currentLine = br.readLine()) != null) {
                        if (currentLine.length() < 3) continue;

                        //Parse the line
                        int index = currentLine.indexOf("@@");
                        String word = currentLine.substring(0, index);
                        int index2 = currentLine.indexOf(" ", index);
                        String url = currentLine.substring(index + 2, index2);
                        String scores = currentLine.substring(index2, currentLine.length());
                        scores = scores.trim();
                        double score = Double.parseDouble(scores);

                        //TODO: Multiply the score with PR SCORE
                        double rank = accesser.getRank(url);
                        System.out.println("negative url: " + rank);
                        score = score * rank;
                        

                        TFIDFScoreObject obj = new TFIDFScoreObject(word, url, score);
                        pq.add(obj);
                        if (pq.size() > LIMIT) pq.poll();
                    }
                }
            }
        }

        return pq;
    }
}
