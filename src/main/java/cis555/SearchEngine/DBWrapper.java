package cis555.SearchEngine;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class DBWrapper {

	private String envDirectory = null;
	private File envDir = null;
	private Environment myEnv;
	private EntityStore store;
	
	// entity
	private PrimaryIndex<String, DBUser> userVotes = null;
	

	public DBWrapper(String dir) {
		this.envDirectory = dir;
		this.setupDir();
		this.setupEnv();
		this.setupIndex();
	}
	
	private void setupDir() {
		envDir = new File(envDirectory);
		if(!envDir.exists()) {
			envDir.mkdir();
			envDir.setWritable(true, false);
			envDir.setReadable(true);
		}
	}
	
	public boolean dbIsClosed() {
		return this.myEnv == null || !myEnv.isValid();
	}
	
	private void setupEnv() {
		// Open the environment. Allow it to be created if it does not already exist.
		// config
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);
	
		StoreConfig storeConfig = new StoreConfig();
		storeConfig.setAllowCreate(true);
//		storeConfig.setMutations(mutations);

		try {
			myEnv = new Environment(envDir, envConfig);
			store = new EntityStore(myEnv, "EntityStore", storeConfig);
		} catch (DatabaseException dbe) {
			dbe.printStackTrace();
		} 
	}
	
	// setup persistent mapping from username to user's DB storage
	private void setupIndex() {
//		this.bolts = store.getPrimaryIndex(String.class, DBReduce.class);
		this.userVotes = store.getPrimaryIndex(String.class, DBUser.class);
	}
	
	// close db entity
	public void close() {
		if (store != null) store.close();
		if (myEnv != null) myEnv.close();
	}
	
	//synch DB
	public void sync() {
		if(myEnv != null) myEnv.sync();
		if(store != null) store.sync();
	}
	
//	public boolean containsBolt(String id){
//		return this.bolts.contains(id);
//	}
//	
//	public DBReduce getBolt(String id) {
//		if (!bolts.contains(id)) return null;
//		return this.bolts.get(id);
//	}
//	
//	public synchronized void putBolt(DBReduce bolt) {
//		this.bolts.put(bolt);
//	}
//	
//	// add a document url to a channel
//	public void addVal(String id, String key, String value) {
//		DBReduce bolt = getBolt(id);
//		bolt.addVal(key, value);
//		// update channel
//		putBolt(bolt);
//	}
	
	public boolean containsUser(String userIp) {
		return this.userVotes.contains(userIp);
	}
	
	public DBUser getUser(String userIp) {
		if (!userVotes.contains(userIp)) return null;
		return this.userVotes.get(userIp);
	}
	
	public synchronized void addNewUser(DBUser user) {
		this.userVotes.put(user);
	}	
	
	public void addNewVote(String userIp, String url, int vote) {
		DBUser user = getUser(userIp);
		if (user == null) {
			System.out.println("adding new user " + userIp);
			addNewUser(new DBUser(userIp));
		}
		user = getUser(userIp);
		user.addVote(url, vote);
		addNewUser(user);
	}
	
//	public List<DBKeyVal> getAllEntity(){
//		EntityCursor<DBKeyVal> cursor = keyVals.entities();
//		List<DBKeyVal> allKeyVals = new LinkedList<>();
//		for (DBKeyVal curKeyVals = cursor.first(); curKeyVals != null; curKeyVals = cursor.next()) {
//			allKeyVals.add(curKeyVals);
//		}
//		cursor.close();
//		return allKeyVals;
//	}
	
//	public void resetEntity() {
//		deleteEntity();
////		this.bolts = store.getPrimaryIndex(String.class, DBReduce.class);
////		this.keyVals = store.getPrimaryIndex(String.class, DBKeyVal.class);
//		setupIndex();
//	}
//	
//	public void deleteEntity() {
////		store.truncateClass(DBReduce.class);
//		store.truncateClass(DBUser.class);
//	}
	
}
