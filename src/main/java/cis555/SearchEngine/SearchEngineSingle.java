package cis555.SearchEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;

import Database.TFIDFScoreObject;
import SearchEngine.SearchEngineIndexerHelpers;
import PageRankGetter.PageRankAccesser;
import edu.upenn.cis455.frontend.Page;
import edu.upenn.cis455.frontend.Result;

public class SearchEngineSingle {
	
	private PageRankAccesser accesser;
	private DBWrapper db = null;
	public String[] barrelServers = {"http://localhost:8011/barrel", "http://localhost:8012/barrel", "http://localhost:8013/barrel"};
	public Map<String, Double> indexerRanks;
	public String pageRankDir = "resources/engineTest";
	
	public SearchEngineSingle() {
		accesser = new PageRankAccesser();
		accesser.init("resources/engineTest");
		db = new DBWrapper("resources/user");
		indexerRanks = new ConcurrentHashMap<> ();
	}
	
	public PriorityQueue<UrlFinalScoreObject> run(String query, String userIp) throws IOException {
		PriorityQueue<TFIDFScoreObject> pq = new PriorityQueue<TFIDFScoreObject>();
		pq = SearchEngineIndexerHelpers.tokenizer(query, pq, "resources/dir2", accesser);
		DBUser user = db.getUser(userIp);
		
		PriorityQueue<UrlFinalScoreObject> res = new PriorityQueue<>();
        while (pq.size() > 0) {
            TFIDFScoreObject obj = pq.poll();
            String url = obj.url;
            double score = obj.score;
			if (user != null) {
				if (user.hasVoted(url)) {
					System.out.println("user scores!!: " + user.getScore(url));
					score = score * user.getScore(url);
				}
			}
//			System.out.println(url + " " + score);
			res.offer(new UrlFinalScoreObject(url, score));
//            System.out.println(obj.url + "  " + obj.score);
        }
        return res;
	}
	
	public void addVote(String userIp, String url, String vote) {
		int userScore = Integer.parseInt(vote) == 0? 0 : 100;
		db.addNewVote(userIp, url, userScore);
	}
	
	

	public static void main(String[] args) throws IOException {

		String query = "best cars";
		String user = "127.0.0.1";
		
		SearchEngineSingle engine = new SearchEngineSingle();
		PriorityQueue<UrlFinalScoreObject> pq = engine.run(query, user);
        while (pq.size() > 0) {
        		UrlFinalScoreObject obj = pq.poll();
	    		String url = obj.url;
	    		double score = obj.score;
//	        res.add(0, new Result(url, null, null));
	        System.out.println(url + "@@" + score);
        }		
		
		//create thread for pagerank
//        PageRankAccesser accesser = new PageRankAccesser();

		//create threads for http requests
		


		//merge response from pagerank and indexers
//		PriorityQueue<Map.Entry<String, Double>> pq = mergeIndexerAndPageRank(indexerRanks, accesser);
//		while (!pq.isEmpty()) {
//			Map.Entry<String, Double> entry = pq.poll();
//			System.out.println(entry.getKey() + " " + entry.getValue());
//		}	
	}
	
	public List<Result> getResults(PriorityQueue<UrlFinalScoreObject> pq){
		List<Result> res = new LinkedList<>();
        while (pq.size() > 0) {
        	UrlFinalScoreObject obj = pq.poll();
        		String url = obj.url;
        		double score = obj.score;
            res.add(0, new Result(url, null, null));
            System.out.println(url + "@@" + score);
        }
        return res;
	}	
	
	public PriorityQueue<Map.Entry<String, Double>> mergeIndexerAndPageRank(Map<String, Double> indexerRanks, PageRankAccesser accesser, String userIp) {
		DBUser user = db.getUser(userIp);
		PriorityQueue<Map.Entry<String, Double>> pq = new PriorityQueue<> ();
		for (String url: indexerRanks.keySet()) {
			double pagerank = accesser.getRank(url);
			double score = pagerank * indexerRanks.get(url);
			if (user != null && user.hasVoted(url)) {
				score = score * user.getScore(url);
			}
			pq.offer(new AbstractMap.SimpleEntry<> (url, score));
		}
		return pq;
	}
	

}
