package cis555.SearchEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;

import cis555.SearchEngine.PageRankAccesser;
import edu.upenn.cis455.frontend.Page;

public class SearchEngine {
	
	private PageRankAccesser accesser;
//	private DBWrapper db = null;
	public String[] barrelServers = {"http://localhost:8011/score", "http://localhost:8012/score", "http://localhost:8013/score"};
	public Map<String, Double> indexerRanks;
	public String pageRankDir = "resources/engineTest";
	
	public SearchEngine() {
		accesser = new PageRankAccesser();
//		accesser.init("resources/engineTest");
//		db = new DBWrapper("resources/user");
		indexerRanks = new ConcurrentHashMap<> ();
	}
	
	public Map<String, Double> run(String query, String userIp) {
		String[] splitted = tokenizer(query);
		int numThreads = (splitted.length)*2;
		Thread[] threads = new Thread[numThreads];
		
		threads[numThreads - 1] = new Thread(new PageRankRunnable(accesser, pageRankDir)); //TODO
		threads[numThreads - 1].start();
		
		int roundRobin = 0;
		String nextServer = barrelServers[0];
		for (int i = 0; i < splitted.length; i++) {
			nextServer = barrelServers[roundRobin++%barrelServers.length];
			System.out.println("sending query " + splitted[i] + " to " + nextServer);
			threads[i * 2] = new Thread(new QuerySenderRunnable(splitted[i], indexerRanks, nextServer));
			threads[i * 2].start();
			if (i != splitted.length - 1) {
				nextServer = barrelServers[roundRobin++%barrelServers.length];
				System.out.println("sending query " + splitted[i] + " " + splitted[i + 1] + " to " + nextServer);
				threads[i * 2 + 1] = new Thread(new QuerySenderRunnable(splitted[i] + " " + splitted[i + 1], indexerRanks, nextServer));
				threads[i * 2 + 1].start();
			}
		}
		
		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("all thread done");
		return indexerRanks;
		
	}
	
	public void addVote(String userIp, String url, String vote) {
		int userScore = Integer.parseInt(vote) == 0? 0 : 100;
//		db.addNewVote(userIp, url, userScore);
	}
	
	

	public static void main(String[] args) {
		
		String userIp = "user";
		String query = "best cars";
		SearchEngine engine = new SearchEngine();
		Map<String, Double> res = engine.run(query, userIp);
		
		
		//create thread for pagerank
//        PageRankAccesser accesser = new PageRankAccesser();

		//create threads for http requests
		


		//merge response from pagerank and indexers
//		PriorityQueue<Map.Entry<String, Double>> pq = mergeIndexerAndPageRank(indexerRanks, accesser);
//		while (!pq.isEmpty()) {
//			Map.Entry<String, Double> entry = pq.poll();
//			System.out.println(entry.getKey() + " " + entry.getValue());
//		}	
	}
	
	public PriorityQueue<Map.Entry<String, Double>> mergeIndexerAndPageRank(Map<String, Double> indexerRanks, PageRankAccesser accesser, String userIp) {
//		DBUser user = db.getUser(userIp);
		PriorityQueue<Map.Entry<String, Double>> pq = new PriorityQueue<> ();
		for (String url: indexerRanks.keySet()) {
			double pagerank = accesser.getRank(url);
			double score = pagerank * indexerRanks.get(url);
//			if (user != null && user.hasVoted(url)) {
//				score = score * user.getScore(url);
//			}
			pq.offer(new AbstractMap.SimpleEntry<> (url, score));
		}
		return pq;
	}
	
	public String[] tokenizer(String query) {
		query = query.toLowerCase().trim();
        String[] splitted = query.split(" ");
        for (int i = 0; i < splitted.length; i++) {
            Stemmer stemmer = new Stemmer();
            String current = splitted[i];
            stemmer.add(current.toCharArray(), current.length());
            stemmer.stem();
            String temp = stemmer.toString();
            splitted[i] = temp;
        }
        return splitted;
	}

}
