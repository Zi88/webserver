package cis555.SearchEngine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.HttpsURLConnection;

public class QuerySenderRunnable implements Runnable {

	private String query;
	private Map<String, Double> indexerRanks;
	private String serverURL;

	public QuerySenderRunnable(String query, Map<String, Double> indexerRanks, String serverURL) {
		this.query = query;
		this.indexerRanks = indexerRanks;
		this.serverURL = serverURL;
	}

	
	/**
	 * send http request to barrelserver to get the response
	 * add header as Query:<queryContent>
	 */
	@Override
	public void run() {
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(serverURL).openConnection();
		} catch (MalformedURLException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			connection.setRequestMethod("GET");
		} catch (ProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		connection.addRequestProperty("query", query);
		int statusCode = 0;
		try {
			statusCode = connection.getResponseCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(statusCode);
		if (statusCode != 200) {
			System.out.println("failed to get statuscode from server: " + serverURL);
			return;
		}
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String inputLine;
		try {
			while ((inputLine = in.readLine()) != null) {
				putResponseLineToMap(indexerRanks, inputLine);
				System.out.println("here");
				System.out.println(inputLine);
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(serverURL + " done");
	}

	public void unpack() {
		
	}
	
	
	/**
	 * format of the response body <rank> <url> <rank> <url> ...
	 * 
	 * @param map
	 * @param responseBody
	 */
	public void putResponseLineToMap(Map<String, Double> map, String line) {
		String[] parts = line.split("@@");
		String url = parts[1];
		double rank = Double.parseDouble(parts[0]);
		int weight = 1;
		if (query.contains(" ")) {
			weight = 2;
		}
		if (map.containsKey(url)) {
			map.put(url, map.get(url) * weight + rank);
		} else {
			map.put(url, rank);
		}
	}

}
