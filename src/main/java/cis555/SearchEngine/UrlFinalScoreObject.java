package cis555.SearchEngine;



public class UrlFinalScoreObject implements Comparable<UrlFinalScoreObject> {

    public String url;
    public double score;	
    
    public UrlFinalScoreObject(String url, double score) {
        this.score = score;
        this.url = url;
    }
    
    @Override
    public int compareTo(UrlFinalScoreObject other) {
        if (this.score >= other.score) return 1;
        else return -1;
    }    
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}


//public class TFIDFScoreObject implements Comparable<TFIDFScoreObject> {
//    public String word;
//    public String url;
//    public double score;
//
//    public TFIDFScoreObject(String word, String url, double score) {
//        this.score = score;
//        this.url = url;
//        this.word = word;
//    }
//
//    @Override
//    public int compareTo(TFIDFScoreObject other) {
//        if (this.score >= other.score) return 1;
//        else return -1;
//    }
//}