package cis555.SearchEngine;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import cis555.SearchEngine.PageRankAccesser;

public class PageRankRunnable implements Runnable{
	
	private PageRankAccesser accesser;
	private String pageRankDir;
	public PageRankRunnable(PageRankAccesser accesser, String pageRankDir) {
        this.accesser = accesser;
        this.pageRankDir = pageRankDir;
    }

    @Override
    public void run() {
      accesser.init(pageRankDir);	
    }
    
}
