package cis555.SearchEngine;

public class AccesserTester {
	public static void main(String[] args) {
		PageRankAccesser accesser = new PageRankAccesser();

		//accesser.init("resources/engineTest/_SUCCESS");
		accesser.init("resources/engineTest/");
		System.out.println(accesser.getRank("valyrians.dreamwidth.org"));
		System.out.println(accesser.getRank("www.car-pass.be"));
		System.out.println(accesser.getRank("www.tribridge.com"));
		System.out.println(accesser.getRank("http://www.tribridge.com/"));
		System.out.println(accesser.getRank("http://www.tribridge.com/efd"));
		System.out.println(accesser.getRank("https://www.tribridge.com/efd/xx"));
		System.out.println(accesser.getRank("e"));
		System.out.println(accesser.getRank("https://travel.state.gov/tt"));
		System.out.println(accesser.getRank("https://www.theguardian.com:443/nespress"));
//		System.out.println(accesser.getRank("https://twitter.com/sdf"));
		System.out.println(accesser.getRank("https://adactio.com"));
		System.out.println(accesser.getRank("https://itunes.com"));
		
	}
	
//	(valyrians.dreamwidth.org,6.370796842752728)
//	(www.car-pass.be,4.216215934872119)
//	(www.tribridge.com,1.846741211899922)
//	(cwexchange.dreamwidth.org,3.1113662694235926)
}
