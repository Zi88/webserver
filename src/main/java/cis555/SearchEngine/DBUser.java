package cis555.SearchEngine;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class DBUser {
	
	@PrimaryKey
	private String ip;
	
	// 1 represents user like the page, 0 means dislike
	private Map<String, Integer> preferences;
	
	public DBUser() {}
	
	public DBUser(String ip){
		this.ip = ip;
		preferences = new HashMap<>();
	}
	
	public String getID() {
		return ip;
	}
	
	public void addVote(String url, int preference) {
		preferences.put(url, preference);
	}
	
	public boolean hasVoted(String url) {
		return preferences.containsKey(url);
	}
	
	public int getScore(String url) {
		return preferences.get(url);
	}
	
	public Map<String, Integer> getPreferences(){
		return this.preferences;
	}
	
}
